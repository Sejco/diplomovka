var orientation = 1;
var scale 		= 1.1;
var cx	  		= 200;
var cy   		= 200;
var upCy		= 260;
var legendColor = '#CC00EE';
var baseColor   = '#444444';

var colors = [];
colors['Head']			= '#33DDEE';
colors['Neck']          = '#00BB00';
colors['Spine1']        = '#cc66dd';
colors['Spine']         = '#99ffff';
colors['ToSpine']       = '#99ff77';
colors['Hips']          = '#FFBB00';
colors['LeftShoulder']  = '#FF7700';
colors['RightShoulder'] = '#66DD00';
colors['LeftArm']       = '#FF7777';
colors['RightArm']      = '#66DD77';
colors['LeftForeArm']   = '#FF77EE';
colors['RightForeArm']  = '#66DDEE';
colors['LeftHand']      = '#CC77AA';
colors['RightHand']     = '#99DDAA';
colors['LeftUpLeg']     = '#99CCEE';
colors['RightUpLeg']    = '#FF5500';
colors['LeftLeg']       = '#9999EE';
colors['RightLeg']      = '#CC5500';
colors['LeftFoot']      = '#9944EE';
colors['RightFoot']     = '#CCAA00';
colors['LeftToeBase']   = '#33FFAA';
colors['RightToeBase']  = '#FF9900';


function getColor(part) {
	return colors[part];
}

function getColorUp(part) {
	if (part == 'LeftFoot') {
		return '#FF0000';
	} else if (part == 'RightFoot') {
		return '#FF0000';
	} else if (part == 'LeftToeBase') {
		return '#00FF00';
	} else if (part == 'RightToeBase') {
		return '#00FF00';
	} else if (part == 'LeftHand') {
		return '#00FF00';
	} else if (part == 'RightHand') {
		return '#00FF00';
	} else {
		return '#00FF00';
	}
}

function setOpacityForBodyPart(part, bodyParts) {
	if (bodyParts !== undefined) {
		if (bodyParts.indexOf(part) == -1) {
			ctx.globalAlpha = 0.03;
		} else {
			ctx.globalAlpha = 1;
		}
	}
}