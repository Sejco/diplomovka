function translatePoint(p, x, y, z) {
	/*	
		input  	=> point, traslation (x, y, z)
		output 	=> translated point
	*/
	
	return [x + p[0], y + p[1], z + p[2], 1];
}

function rotatePointX(p, angle) {
	angle = degrees2radians(angle);
	let s = parseFloat(Math.sin(angle).toFixed(6));
	let c = parseFloat(Math.cos(angle).toFixed(6));
	
	return [p[0], c * p[1] -s * p[2], s * p[1] + c * p[2], 1];
}

function rotatePointY(p, angle) {
	angle = degrees2radians(angle);
	let s = parseFloat(Math.sin(angle).toFixed(6));
	let c = parseFloat(Math.cos(angle).toFixed(6));
	
	return [c * p[0] + s * p[2], p[1] , -s * p[0] + c * p[2], 1];
}

function rotatePointZ(p, angle) {
	angle = degrees2radians(angle);
	let s = parseFloat(Math.sin(angle).toFixed(6));
	let c = parseFloat(Math.cos(angle).toFixed(6));
	
	return [c * p[0] - s * p[1], s * p[0] + c * p[1], p[2], 1];
}

function rotZXYaroundPoint(xA, yA, zA, rP, originalPoint) {
	originalPoint = translatePoint(originalPoint, -rP[0], -rP[1], -rP[2]);
	
	originalPoint = rotatePointY(originalPoint, yA);
	originalPoint = rotatePointX(originalPoint, xA);
	originalPoint = rotatePointZ(originalPoint, zA);
	
	originalPoint = translatePoint(originalPoint, rP[0], rP[1], rP[2]);
	
	return originalPoint;
}

function quatermRot3axis(xAngle, yAngle, zAngle, rotPoint, endPoint, rotOrder) {	
	/*	
		input  	=> angles of rotation, rotation point, point to rotate
		output 	=> rotated point
	*/
	
	let rv = quatermFromPoints(rotPoint, endPoint);
	
	let qx = new Quaterm('x', xAngle);
	let qy = new Quaterm('y', yAngle);
	let qz = new Quaterm('z', zAngle);

	let iqx = new Quaterm('x', xAngle, 'inverse');
	let iqy = new Quaterm('y', yAngle, 'inverse');
	let iqz = new Quaterm('z', zAngle, 'inverse');
	
	switch (rotOrder) {
		case 'ZYX':
			rv = quatermConjugation(qx, rv, iqx);
			rv = quatermConjugation(qy, rv, iqy);
			rv = quatermConjugation(qz, rv, iqz);
			break;
			
		case 'YZX':
			rv = quatermConjugation(qx, rv, iqx);
			rv = quatermConjugation(qz, rv, iqz);
			rv = quatermConjugation(qy, rv, iqy);
			break;
			
		case 'ZXY':
			rv = quatermConjugation(qy, rv, iqy);
			rv = quatermConjugation(qx, rv, iqx);
			rv = quatermConjugation(qz, rv, iqz);
			break;
			
		case 'XZY':		
			rv = quatermConjugation(qy, rv, iqy);
			rv = quatermConjugation(qz, rv, iqz);
			rv = quatermConjugation(qx, rv, iqx);
			break;
		
		case 'XYZ':
			rv = quatermConjugation(qz, rv, iqz);
			rv = quatermConjugation(qy, rv, iqy);
			rv = quatermConjugation(qx, rv, iqx);
			break;
		
		case 'YXZ':
			rv = quatermConjugation(qz, rv, iqz);
			rv = quatermConjugation(qx, rv, iqx);
			rv = quatermConjugation(qy, rv, iqy);
			break;
	}

	let vector = quatermToVector(rv);
	
	return translatePoint(vector, rotPoint[0], rotPoint[1], rotPoint[2]);
}

function quatermConjugation(q, p, iq) {
	/*
		eq>> quatermion * vector * inverese quatermion
	
		input  	=> quatermion
				=> vector 
				=> inverse quatermion
		output 	=> quatermion calculated by given equation
	*/
	
	let result;

	result 	 = quatermMultiply(q,p);
	result 	 = quatermMultiply(result, iq);
	result.w = 0;
	
	return result;
}

function quatermLength(q) {
	/*
		input  => quatermion
		output => lenght of quatermion 
	*/
	
	return Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
}

function quatermFromPoints(rotPoint, endPoint) {
	/*
		input  => rotation point rotation, point to rotate
		output => quatermion 
	*/
	
	let q = {x:0, y:0, z:0, w:0};

	q.x = endPoint[0] - rotPoint[0];
	q.y = endPoint[1] - rotPoint[1];
	q.z = endPoint[2] - rotPoint[2];
	q.w = 0;
	
	return q;
}

function quatermMultiply(q1, q2) {
	/*
		input  => two quatermions
		output => (quatermion) product of multiplication
	*/
	
	let q = {x:0, y:0, z:0, w:0};
	
	q.w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;
	q.x	= q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y;
	q.y	= q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x;
	q.z	= q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w;
	
	return q;
}

function quatermToVector(q) {
	/*
		input  => quatermion
		output => array
	*/
	
	return [q.x, q.y, q.z, q.w];
}

function createVector(p1, p2) {
	/*
		input  => two points
		output => (array) vector
	*/
	
	return [p2.x - p1.x, p2.y - p1.y, p2.z - p1.z];
}

function createVectorArr(arr1, arr2) {
	/*
		Y coordinate is set to zero - XZ plane projection
		
		input  => two arrays
		output => (array) vector
	*/
	
	return [arr2[0] - arr1[0], 0, arr2[2] - arr1[2]];
}

function vectorLength(vector) {
	/*
		input  => vector (array of numbers)
		output => vector length (number)
	*/
	
	let pomSum = 0;
	
	for (let i in vector) {
		pomSum += vector[i] * vector[i];
	}
	
	return Math.sqrt(pomSum);
}

function createStraightFromTwoPoints(p1, p2) {
	/*	
		input  	=> two points (x,y,z)
		output	=> parametric equation of line		
	*/
	
	let v 		 = createVector(p1, p2);
	let straight = {x:{num:0, t:0},
					y:{num:0, t:0},
					z:{num:0, t:0}};
	
	straight.x.num = p1.x;
	straight.y.num = p1.y;
	straight.z.num = p1.z;
	straight.x.t = v[0];
	straight.y.t = v[1];
	straight.z.t = v[2];
		
	return straight;
}

function computeDistancePointStraight(p, s) {
	/*	
		input  	=> point, line
		output	=> distance beetween point and line	
	*/
	
	let vector 		= $.extend(true, {}, s );			// clone of variable insead of reference
	let originalVec = $.extend(true, {}, s );			// clone of variable insead of reference
	
	vector.x.num -= p.x;
	vector.y.num -= p.y;
	vector.z.num -= p.z;
	
	let dirVec = {x : vector.x.t, 
				  y : vector.y.t, 
				  z : vector.z.t};
	
	// finding t parameter
	let pomVector = $.extend(true, {}, vector);
	pomVector.x.num = pomVector.x.num 	* dirVec.x; 
	pomVector.y.num = pomVector.y.num 	* dirVec.y; 
	pomVector.z.num = pomVector.z.num 	* dirVec.z; 
	
	pomVector.x.t 	= pomVector.x.t 	* dirVec.x; 
	pomVector.y.t 	= pomVector.y.t 	* dirVec.y; 
	pomVector.z.t 	= pomVector.z.t 	* dirVec.z; 
	
	// equation solution 
	let leftside  	= pomVector.x.num + pomVector.y.num + pomVector.z.num;
	let rightside 	= -(pomVector.x.t + pomVector.y.t + pomVector.z.t);
	let t  		  	= leftside / rightside;
	
	//substitute t
	let pomVector1 = [];
	pomVector1[0] = originalVec.x.num + originalVec.x.t * t;
	pomVector1[1] = originalVec.y.num + originalVec.y.t * t;
	pomVector1[2] = originalVec.z.num + originalVec.z.t * t;
	
	//calculate final vector
	let finalVector = createVector(p ,new Point(pomVector1[0], pomVector1[1], pomVector1[2]));
	
	//distance
	let dist = Math.sqrt(finalVector[0] * finalVector[0] + finalVector[1] * finalVector[1] + finalVector[2] * finalVector[2]);

	return dist;
}

function discreteCosineTransform(signal) {
	/*
		http://www.haberdar.org/discrete-cosine-transform-tutorial.htm
		
		input  = array of real numbers (signal)
		output = array of real numbers (cosine transform koeficients)
	*/
	
	let output 		= [];
	let n 	   		= signal.length;
	let sqrt1n 		= Math.sqrt(1 / n);
	let sqrt2n 		= Math.sqrt(2 / n);
	let PIn	   		= Math.PI / n;
	
	for (let k = 0; k < n; k++) {
		let pomValue = 0;
		for (let t = 0; t < n; t++) {
			if (k == 0) {
				pomValue += signal[t] * sqrt1n * Math.cos(PIn * (t + 0.5) * k);
			} else {
				pomValue += signal[t] * sqrt2n * Math.cos(PIn * (t + 0.5) * k);
			}
		}
		output[k] = pomValue;
	}
	
	return output;
}

function inverseDiscreteCosineTransform(cosine_koeficients) {
	/*
		input  = array of real numbers (cosine transform koeficients)
		output = array of real numbers (reconstructed signal)
	*/
	
	let output = [];
	let n 	   = cosine_koeficients.length;
	let sqrt1n = Math.sqrt(1 / n);
	let sqrt2n = Math.sqrt(2 / n);
	
	for (let t = 0; t < n; t++) {
		let a = 0;	
		for (let k = 0; k < n; k++) {
			if (k == 0) {
				a += cosine_koeficients[k] * sqrt1n * Math.cos(((2 * t + 1) * k * Math.PI) / (2 * n));
			} else {
				a += cosine_koeficients[k] * sqrt2n * Math.cos(((2 * t + 1) * k * Math.PI) / (2 * n));
			}
		}
		
		output[t] = a;
		
	}
	
	return output;
}

function signalPower(signal) {
	let output = 0;
	
	for (let i in signal) {
		output += signal[i] * signal[i];
	}

	return output / signal.length;
}

function covarianceMatrix(input_values) {
	/*
		creates covariance matrix from input data
		
		input  => vector MxN
		output => covariance matrix MxM
	*/
	
	let parsedArray	= [];
	let meanValues	= [];
	let outputMatix = [];
	
	//parse data
	for (let obj in input_values) {
		for (let i in input_values[obj]) {
			if (parsedArray[i] === undefined) {
				parsedArray[i] = [];
			}
			parsedArray[i].push(parseFloat(input_values[obj][i]));
		}
	}
	
	//calculate mean values
	for (let i in parsedArray) {
		meanValues[i] = meanValue(parsedArray[i]);
	}
	
	//create covariance matrix
	for (let i in parsedArray) {
		outputMatix.push([]);
		for (let j in parsedArray) {
			outputMatix[outputMatix.length - 1].push(cov(parsedArray[i], parsedArray[j], meanValues[i], meanValues[j]));
		} 
	} 

	return outputMatix;
}

function cov(x_arr, y_arr, x_mean, y_mean) {
	/*
		eq>> sum((X[i] - meanX) * (Y[i] - meanY)) / (n - 1)
		
		input  	=> 2 arrays of float values
				=> mean value
		output	=> covariance (float)		
	*/
	
	let pomSum = 0;
	
	for (let i in x_arr) {
		pomSum += (x_arr[i] -  x_mean) * (y_arr[i] -  y_mean);
	}
	
	return pomSum / (x_arr.length - 1);
}

function variance(arr) {
	
}

function meanValue(arr) {
	return sumOfArray(arr) / arr.length;
}

function variancePoints(arr, mean, coor) {
	/*
		eq>> sum((arr.coor - mean) ** 2) / arr.length
		
		input  	=> array of points
				=> mean value
				=> coordinate of point
		output	=> variance calculated by equation			
	*/
	
	var pomArr = JSON.parse(JSON.stringify(arr));			// clone of array (insted of refernce)
	for (var i in pomArr){
		pomArr[i][coor] -= mean;
	}
		
	for (let i in pomArr){
		pomArr[i][coor] = pomArr[i][coor] * pomArr[i][coor];
	}

	return summOfArrPoints(pomArr, coor) / pomArr.length;
}

function meanValuePoints(arr, coor) {
	/*
		eq>> sum(arr) / arr.length
		
		input  	=> array of points
				=> coordinate of point
		output	=> mean value calculated by equation			
	*/
	
	return summOfArrPoints(arr, coor) / arr.length;
} 

function autocorr(signal) {
	/*
		https://www.mathworks.com/help/econ/autocorr.html#btzjb3t
		
		
	*/
	
	let mx 	 	= meanValue(signal);
	let T		= signal.length;
	let sum1 	= 0;  //sum ((x[i] - mx) * (x[i+k] - mx))
	let c0 		= 0;  //sum ((x[i] - mx) ** 2)
	let output 	= [];
	
	for (let i = 0; i < T; i++) {
		c0 += ((signal[i] - mx) * (signal[i] - mx));
	}
	
	c0 = c0 / (T - 1);
	
	for (let k = 0; k < signal.length; k++) {
		for (let t = 0; t < T - k; t++) {
			sum1 += ((signal[t] - mx) * (signal[t + k] - mx));
		}
		ck = (1 / (T - 1)) * sum1;
		
		output.push(ck / c0);
	}
	
	return output.slice(0,20);
	
}

function xcorr(x, y) {
	/*
		cross correlation - measures the similarity between signals	
	*/

	let output 	 = [];
	let N 		 = (x.length > y.length ? y.length : x.length);
	let maxdelay = 50;
	let mx 	 	 = meanValue(x);
	let my 	 	 = meanValue(y);
	let sum1 	 = 0;	// sum((x(i) - mx) * (y(i) - my))
	let sum2 	 = 0;	// sum((x(i) - mx)**2)
	let sum3 	 = 0;	// sum((y(i) - my)**2)
	let denom    = 0;   // sqrt(sum2 * sum3)
	
	for (let i = 0; i < N; i++) {
		sum2 += (x[i] - mx) * (x[i] - mx);
		sum3 += (y[i] - my) * (y[i] - my);
	}
	denom = Math.sqrt(sum2 * sum3);

	for (let delay = -maxdelay; delay < maxdelay; delay++) {
		sum1 = 0;
		for (let i = 0; i < N; i++) {
			if (((i + delay) < 0) || ((i + delay) >= N)) {
				
			} else {
				sum1 += (x[i] - mx) * (y[i + delay] - my);
			}
		}
		output.push(sum1 / denom);
	}
	
	return arrayMaxValue(output);
}

function correlation(arr, meanX, meanY, coor1, coor2) {
	/*
		eq>> sum(x*y) / sqrt(sum(x) * sum(y))
		
		input 	=> array of points
				=> mean value of X
				=> mean value of Y
				=> coordinate 1 of point (for example 'x')
				=> coordinate 2 of point (for example 'y')
		output	=> (float) correlation between coor1 and coor2 of given points 
	*/
	
	let sumXY 	= 0;
	let sumSqrX = 0;
	let sumSqrY = 0;
	
	for (let i in arr) {
		sumXY 	+= (arr[i][coor1] - meanX) * (arr[i][coor2] - meanY); 
		sumSqrX += (arr[i][coor1] - meanX) * (arr[i][coor1] - meanX);
		sumSqrY += (arr[i][coor2] - meanY) * (arr[i][coor2] - meanY);
	}
	
	return sumXY / Math.sqrt(sumSqrX * sumSqrY);
}

function linearRegresion(arr, c1, c2, start , end) {
	/*		
		input 	=> array of points
				=> coordinate 1		(for example 'x')
				=> coordinate 2		(for example 'y')
				=> start point of line
				=> end points of line
		output	=> line calculated by linear regresion with end points
	*/
	
	var endPoints = [];
	var mx = meanValuePoints(arr, c1);
	var my = meanValuePoints(arr, c2);
	var sx = Math.sqrt(variancePoints(arr, mx, c1));
	var sy = Math.sqrt(variancePoints(arr, my, c2));
	var r  = correlation(arr, mx, my, c1, c2);
	var b  = r * (sy / sx);
	var A  = my - b * mx;
	
	
	endPoints.push(start, b * start + A);
	endPoints.push(end, b * end + A);
	
	return endPoints;
}

function linearRegresionReturnCoeficients(arr, c1, c2) {
	/*		
		input 	=> array of points
				=> coordinate 1		(for example 'x')
				=> coordinate 2		(for example 'y')
		output	=> regress line coeficients A, b
	*/
	
	var endPoints = [];
	var mx = meanValuePoints(arr, c1);
	var my = meanValuePoints(arr, c2);
	var sx = Math.sqrt(variancePoints(arr, mx, c1));
	var sy = Math.sqrt(variancePoints(arr, my, c2));
	var r  = correlation(arr, mx, my, c1, c2);
	var b  = r * (sy / sx);
	var A  = my - b * mx;
	
	return [A,b];
}

function matrix_multiply(m1, m2) {
	/*		
		input 	=> matrix 1, matrix 2
		output	=> product of matrix 1 and matrix 2
	*/
	
	let m1Rows = m1.length;
	let m1Cols = m1[0].length;
    let m2Rows = m2.length;
	let m2Cols = m2[0].length;
	let outputMatrix = new Array(m1Rows);  
	
	for (let r = 0; r < m1Rows; r++) {
		outputMatrix[r] = new Array(m2Cols); 
		for (let c = 0; c < m2Cols; c++) {
			outputMatrix[r][c] = 0;
			for (var i = 0; i < m1Cols; i++) {
				outputMatrix[r][c] += m1[r][i] * m2[i][c];
			}
		}
	}
	return outputMatrix;
}

function computeMedian(arr) {
	/*
		calculate maedian from input values
		if input array length is odd, for example 5, then middleIndex = 2.5
		from middleIndex we can get startIndex and endIndex, its floor and ceil of middleIndex in this case 2 and 3
		next we take input array, sort it and make average of arr[2] and arr[3]
		if middle index is equal to 2, start and end index will be equal to 2, so (arr[2] + arr[2] / 2) will be equal to arr[2]
	
		input  => array of (float)
		output => median value;
	*/
	
	let output 		= 0;
	let middleIndex = (arr.length - 1) / 2; 
	let startIndex  = Math.floor(middleIndex);
	let endIndex    = Math.ceil(middleIndex);
	
	arr.sort(function(a, b){return a - b});

	return (arr[startIndex] + arr[endIndex]) / 2;			
}

function radians2degrees(rad) {
	/*
		input  => radians
		ouput  => degrees
	*/
	
	return rad * (180 / Math.PI);
}

function degrees2radians(deg) {
	/*
		input  => degrees
		ouput  => radians
	*/
	
	return deg * (Math.PI / 180);
}

function arrayMaxValue(array) {
	/*
		input  => array of float
		output => max value from this array
	*/
	
	return Math.max.apply(Math, array);
}

function arrayMinValue(array) {
	/*
		input  => array of float
		output => min value from this array
	*/
	
	return Math.min.apply(Math, array);
}

function scalarProduct(v1, v2) {
	/*
		computes dot product of two vetors
		
		input  => 2 vectors (arrays of numbers)
		output => number
	*/
	
	let output = 0;
	
	for (let i in v1) {
		output += v1[i] * v2[i];
	}
	
	return output;
}

function vectorNormalize(vector) {
	/*
		input  => vector (array of numbers)
		output => normalized vector (array of numbers)
	*/
	
	let output 	= [];
	let l 		= vectorLength(vector);
	
	for (let i in vector) {
		output[i] = vector[i] / l;
	}
	
	return output;
}

function distanceCoords2D(x1, y1, x2, y2) {
	/*
		input  => coordinates of two points x,y
		ouput  => distance of points
	*/
	
	var x = x1 - x2;
	var y = y1 - y2;
	
	return Math.sqrt(x * x + y * y);
}

function distanceCoords3D(x1, y1, z1, x2, y2, z2) {
	/*
		input  => coordinates of two points x,y,z
		ouput  => distance of points
	*/
	
	var x = x1 - x2;
	var y = y1 - y2;
	var z = z1 - z2;
	
	return Math.sqrt(x * x + y * y + z * z);
}

function distancePoints(p1, p2) {
	/*
		input  => two points
		ouput  => distance of points
	*/
	
	var x = p1.x - p2.x;
	var y = p1.y - p2.y;
	var z = p1.z - p2.z;
	
	return Math.sqrt(x * x + y * y + z * z);
}

function maxDistance(points1, points2) {
	/*
		input  => two arrays od points
		output => maximal found distance
	*/
	
	var dist = 0; 
	
	for (var i = 0; i < points1.length; i++) {
		var pomDist = distancePoints(points1[i], points2[i]);
		if (pomDist > dist) {
			dist = pomDist;
		}
	}
	
	return dist;
} 

function minDistance(points1, points2) {
	/*
		input  => two arrays od points
		output => minimal found distance
	*/
	
	var dist = Number.MAX_VALUE; 
	
	for (var i = 0; i < points1.length; i++ ) {
		var pomDist = distancePoints(points1[i], points2[i]);
		if (pomDist < dist){
			dist = pomDist;
		}
	}
	
	return dist;
} 

function averageDistance(points1, points2) {
	/*
		input  => array of points, array of points
		output => average distance
	*/
	
	var distSum = 0;
	
	for (var i = 0; i < points1.length; i++) {
		distSum += this.distancePoints(points1[i], points2[i]);
	}
	
	return distSum / points1.length;
}

function sumOfArray(arr) {
	/*
		input  => array of numbers
		output => sum
	*/
	
	let sum = 0;
	for (let s of arr) {
		sum += s;
	}
	
	return sum;
}

function summOfArrPoints(arr, coor) {
	/*
		input  => array of points, coordinate of point
		output => sum of given point coordinate
	*/
	
	let sum = 0;
	
	for (let i in arr) {
		sum += arr[i][coor];
	}
	
	return sum;
}

function arrayToPoint(arr){
	/*
		input  => array of float
		output => point
	*/
	
	return new Point(arr[0], arr[1], arr[2]);
}

function adjacentAngleInTriangle(a, b) {
	/*
		eq>> tan α = a / b
			 α 	   = atan(a / b)
		
		input  => sides of triangle
		output => adjacent side angle
	*/
	
	return radians2degrees(Math.atan(a / b));
}

function signalFilterHips(arr) {
	/*
		pokus 1
	*/
	var signalTemplate = [-138.55527711042345, -143.505287010398, -170.95534191052198, 41.314582629102645, 78.50715701426567, 99.85519971030173,
								  49.932099864114015, 17.48703497405231, 121.252742505392, 149.60729921446506, 196.81239362457072, 207.84191568360825,
								  145.1567903134285, 60.480120960193894, -0.045000089988957645, 152.46030492045372, 281.3405626808468];
			
	let error    = 0;
	let matchArr = [];
	
	for (let i = 0; i < arr.length - signalTemplate.length; i++) {
		for (let j = 0; j < signalTemplate.length; j++) {
			error += signalTemplate[j] - arr[i + j];
		}
		if (error < 50) {
			matchArr = Array.prototype.concat(matchArr, arr.slice(i, i + signalTemplate.length));	
		}
		error = 0;
	}		  
	return matchArr;
}

function signalTriangularSmooth5P(arr) {
	/*
		eq>> (x[i - 2] + 2*x[i - 1] + 3*x[i] + 2*x[i + 1] + x[i + 2]) / 9
		
		input  => signal (array of float)
		output => smoothed signal by given equation
	*/
	let output = [];
	for (let i = 2; i < arr.length - 2; i++){
		output[i] = (arr[i - 2] + 2 * arr[i - 1] + 3 * arr[i] + 2 * arr[i + 1] + arr[i + 2]) / 9;
	}
	return output;
}

function signalFilterLowPass(signal, peak) {
	/*
		input  => signal (array of float)
		output => indices of peaks (array of int)
	*/
	
	let output  = [];
	let pom_arr = [];
	
	for (let i in signal) {
		if (signal[i] < peak) {
			pom_arr.push([parseInt(i), signal[i]]);
		} else {
			if (pom_arr.length > 1) {
				pom_arr.sort(function(a, b){return a[1] - b[1]});	
				
				output.push(pom_arr[0][0]);
				
				pom_arr = [];
			}
		}
	}
	
	return output;
}

function signalFilterHighPass(signal, peak) {
	let output  = [];
	let pom_arr = [];
	
	for (let i in signal) {
		if (signal[i] > peak) {
			pom_arr.push([parseInt(i), signal[i]]);
		} else {
			if (pom_arr.length > 1) {
				pom_arr.sort(function(a, b){return b[1] - a[1]});	
				
				output.push(pom_arr[0][0]);
				
				pom_arr = [];
			}
		}
	}
	
	return output;
}

//classes

function Point(x,y,z){
	this.x = parseFloat(x);
	this.y = parseFloat(y);
	this.z = parseFloat(z);
}

function Quaterm(axis, angle, inverse){
	let radAngle = degrees2radians(angle);

	let rotAngle = Math.sin(radAngle * 0.5);
	let x = 0;
	let y = 0; 
	let z = 0;
	
	if (inverse == 'inverse') {
		switch(axis) {
		case 'x':
			x = -1;
			break;
		case 'y':
			y = -1;
			break;
		case 'z':
			z = -1;
			break;
		}
	} else {
		switch(axis) {
		case 'x':
			x = 1;
			break;
		case 'y':
			y = 1;
			break;
		case 'z':
			z = 1;
			break;
		}
	}
	
	this.x = x * rotAngle;
	this.y = y * rotAngle;
	this.z = z * rotAngle;
	this.w = parseFloat(Math.cos(radAngle * 0.5).toFixed(15));
}







