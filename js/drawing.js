function point(x, y, color){
	if (color === undefined){
		color = "#000000";
	}
	ctx.beginPath();
	ctx.fillStyle = color;
	ctx.arc(x * scale, canvasHeight - y * scale, 3, 0, 2*Math.PI);
	ctx.fill();
}

function line(x1, y1, x2, y2, color){
	if (color === undefined){
		color = "#000000";
	}
	ctx.beginPath();
	ctx.strokeStyle = color;
	ctx.moveTo(x1 * scale, canvasHeight - y1 * scale);
	ctx.lineTo(x2 * scale, canvasHeight - y2 * scale);
	ctx.stroke();
}

function point_n(x, y, color, size){
	if (size === undefined){
		size = 4;
	}
	if (color === undefined){
		color = "#000000";
	}
	ctx.beginPath();
	ctx.fillStyle  = color;
	ctx.strokeStyle = '#000000';
	ctx.arc(x, canvasHeight - y, size, 0, 2*Math.PI);
	ctx.fill();
	ctx.stroke();
}

function line_n(x1, y1, x2, y2, color){
	// ctx.lineWidth = 1;
	if (color === undefined){
		color = "#000000";
	}
	ctx.beginPath();
	ctx.strokeStyle = color;
	ctx.moveTo(x1, canvasHeight - y1);
	ctx.lineTo(x2, canvasHeight - y2);
	ctx.stroke();
	ctx.strokeStyle = '#000000';
}

function axis(axis1, axis2, axis3, shiftX, shiftY){
	if (shiftX === undefined) {
		shiftX = 10;
	}
	
	if (shiftY === undefined) {
		shiftY = -10;
	}
	
	ctx.font	  = "20px Arial";
	ctx.fillStyle = '#000000';
	ctx.lineWidth = 1;
	
	ctx.beginPath();
	ctx.moveTo(20 + shiftX, 480 + shiftY);
	ctx.lineTo(60 + shiftX, 480 + shiftY);
	ctx.moveTo(20 + shiftX, 480 + shiftY);
	ctx.lineTo(30 + shiftX, 470 + shiftY);
	ctx.moveTo(20 + shiftX, 480 + shiftY);
	ctx.lineTo(20 + shiftX, 440 + shiftY);
	ctx.stroke();
	
	ctx.fillText(axis3, 65 + shiftX, 480 + shiftY);
	ctx.fillText(axis1, 35 + shiftX, 465 + shiftY);
	ctx.fillText(axis2, 20 + shiftX, 435 + shiftY);
}

function legend(s){
	let pomScale = 10 * scale;
	
	if (s === undefined) {
		s = 1;
	}
	
	ctx.lineWidth 	= 0.8;
	ctx.font 		= "16px helvetica";
	ctx.fillStyle  	= '#333333';
	ctx.textAlign	= "right"; 
	
	ctx.beginPath(); //horizontal
	ctx.moveTo(10, 480);
	ctx.lineTo(47 * pomScale - 64, 480);
	
	for (let i = 0; i < 45; i++){
		if (i % 10 == 0) {
			if (i != 0) {
				ctx.moveTo(i * pomScale + 16, 490);
				ctx.lineTo(i * pomScale + 16, 480);
			}
		} else {
			ctx.moveTo(i * pomScale + 16, 485);
			ctx.lineTo(i * pomScale + 16, 480);
		}
	}
	ctx.stroke();
	
	ctx.beginPath();  //vertical
	ctx.moveTo(20, canvasHeight - 10);
	ctx.lineTo(20, canvasHeight - (18 * pomScale - 10));
	
	for (let i = 1; i < 18; i++){	
		ctx.moveTo(10,  canvasHeight - (i * pomScale + 16));
		ctx.lineTo(20,  canvasHeight - (i * pomScale + 16));	
	}
	ctx.stroke();
	ctx.textAlign= "start"; 
}

function drawFocusedPart(parts) {
	clear_canvas();
	legend();
	axis('X','Y','Z');
	player.draw_walk_sequence_side_focusing(parts);
}

function drawStrideLengthLegend(arr) {
	for (let i in arr) {
		point_n(arr[i].z * scale + cx, arr[i].y - 3, legendColor, 5);
		
		ctx.beginPath();
		ctx.setLineDash([5]);
		ctx.fillStyle = legendColor;
		ctx.lineWidth = 2;
		ctx.moveTo(arr[i].z * scale + cx, canvasHeight - arr[i].y);
		ctx.lineTo(arr[i].z * scale + cx, canvasHeight - 480);
		ctx.stroke();
		ctx.setLineDash([0]);
	}
}

function drawLegendMeanValueX(value, focusedParts) {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(focusedParts);
	ctx.globalAlpha = 1;
	
	point_n(55, value * scale + cy, legendColor, 3);
		
	ctx.beginPath();
	ctx.setLineDash([5]);
	ctx.fillStyle = legendColor;
	ctx.lineWidth = 2;
	ctx.moveTo(55, canvasHeight - (value * scale + cy));
	ctx.lineTo(1250, canvasHeight - (value * scale + cy));
	ctx.stroke();
	ctx.setLineDash([0]);
}

function drawLegendVarianceX(regresPoints, focusedParts) {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(focusedParts);
	ctx.globalAlpha = 1;
	
	point_n(20,  ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + upCy), legendColor, 5);
	point_n(20,  ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + upCy), legendColor, 5);
	
	
	ctx.beginPath();
	ctx.setLineDash([0]);
	ctx.fillStyle = legendColor;
	ctx.lineWidth = 2;
	ctx.moveTo(20 , canvasHeight - ((regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + upCy));
	ctx.lineTo(1200, canvasHeight - ((regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + upCy));
	ctx.stroke();
	
	ctx.beginPath();
	ctx.setLineDash([5]);
	ctx.moveTo(20 , canvasHeight - ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + upCy));
	ctx.lineTo(1200, canvasHeight - ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + upCy));
	ctx.moveTo(20 , canvasHeight - ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + upCy));
	ctx.lineTo(1200, canvasHeight - ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + upCy));
	ctx.stroke();
	ctx.setLineDash([0]);
}

function drawLegendMeanValueY(value) {
	point_n(55, value * scale + cy, legendColor, 3);
		
	ctx.beginPath();
	ctx.setLineDash([5]);
	ctx.fillStyle = legendColor;
	ctx.lineWidth = 2;
	ctx.moveTo(55, canvasHeight - (value * scale + cy));
	ctx.lineTo(1250, canvasHeight - (value * scale + cy));
	ctx.stroke();
	ctx.setLineDash([0]);
}

function drawLegendVarianceY(regresPoints) { 	
	point_n(20,  ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + cy), legendColor, 5);
	point_n(20,  ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + cy), legendColor, 5);	
	
	ctx.beginPath();
	ctx.setLineDash([0]);
	ctx.fillStyle = legendColor;
	ctx.lineWidth = 2;
	ctx.moveTo(20 , canvasHeight - ((regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + cy));
	ctx.lineTo(1200, canvasHeight - ((regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + cy));
	ctx.stroke();

	
	ctx.beginPath();
	ctx.setLineDash([5]);
	ctx.moveTo(20 , canvasHeight - ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + cy));
	ctx.lineTo(1200, canvasHeight - ((regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + cy));
	ctx.moveTo(20 , canvasHeight - ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * 66) * scale + cy));
	ctx.lineTo(1200, canvasHeight - ((-regresPoints['mse'] + regresPoints['b_0'] + regresPoints['b_1'] * regresPoints['x2']) * scale + cy));
	ctx.stroke();
	ctx.setLineDash([0]);
}

function drawWalkSpeedLegend(arr) {
	for (let i in arr) {
		point_n(arr[i].z * scale + cx, arr[i].y + 8, legendColor, 5);
		
		ctx.beginPath();
		ctx.setLineDash([5]);
		ctx.fillStyle = legendColor;
		ctx.lineWidth = 2;
		ctx.moveTo(arr[i].z * scale + cx, canvasHeight - 20);
		ctx.lineTo(arr[i].z * scale + cx, canvasHeight - 480);
		ctx.stroke();
		ctx.setLineDash([0]);
	}
}

function drawStepWidthLegend(arr) {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(['LeftFoot', 'RightFoot', 'LeftToeBase', 'RightToeBase']);

	for (let i in arr['steps']) {
		point_n(arr['steps'][i].z * scale + cx,  (arr['steps'][i].x * scale + upCy), legendColor, 10);
	}
	ctx.lineWidth = 4;
	ctx.setLineDash([5]);

	for (let i in arr['regresLine']) {
		line_n(scale * arr['regresLine'][i][0] + cx , scale * arr['regresLine'][i][1] + upCy, scale * arr['regresLine'][i][2] + cx , scale * arr['regresLine'][i][3]  + upCy);
	}
	ctx.setLineDash([0]);
	ctx.lineWidth = 1;
}

function drawHandsDistancesLegend() {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(['LeftHand', 'RightHand']);
}

function drawElbowsDistancesLegend() {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(['LeftArm', 'RightArm']);
}

function drawKneesDistancesLegend() {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(['LeftLeg', 'RightLeg']);
}

function drawFootsDistancesLegend() {
	clear_canvas();
	legend();
	axis('Y','X','Z');
	player.draw_walk_sequence_up(['LeftFoot', 'RightFoot']);
}

function reDrawCanvas() {
	document.getElementById('myChart').style.display = "none";
	document.getElementById('myCanvas').style.display = "block";
	
	clear_canvas();
	ctx.globalAlpha = 1;
	axis('X', 'Y', 'Z');
	legend();
	player.draw_walk_sequence_side();
}

function plot(values, title) {
	document.getElementById('myChart').style.display = "block";
	document.getElementById('myCanvas').style.display = "none";
	
	if (values) {
		zingchart.render({
			id: 'myChart',
			height: 374, 
			width: '100%',
			data: {
				type: 'line',
					title: {
						text: title
				},
				plot:{
					"line-color":"#00FF00",
					"line-width":2,
					marker:{
						"background-color":"#00FF00",
						"size":4,
						"border-color":"#000000",
						"border-width":1
					}
				},
				"scale-x":{
					"label":{
						"text":"Time"
					},
					"zooming":true,
					"zoom-to":[0,100],
				},
				"scale-y":{
					"label":{
						"text":"Value"
					}
				},
				"scroll-x":{
					"handle":{
						"background-color":"#aaaaaa",
						"alpha":1,
						"border-radius":"10px",
						"height":"10px"
					}
				},
				series: [{
					values: values['signal']
				}]
			}
		});
	}
}

function drawSignalGraph(signal, graphScale) {   //signal = [signal, boundary points]
	let graphScaleFactor = 8;
	let XstepSize		 = 10;
	let mean 			 = 0;
	
	clear_canvas();
	
	if (graphScale !== undefined) {
		graphScaleFactor = 0.08;
		mean 			 = meanValue(signal['signal']) * 12;
		legend(5);
	} else {
		legend(0.05);
	}
	
	
	ctx.lineWidth = 1;
	ctx.strokeStyle = '#bbbbbb';
	
	// grig lines
	ctx.beginPath();
	for (let i = 0; i < 18; i++){	
		ctx.moveTo(55, canvasHeight - (i * 10 * scale + 10));
		ctx.lineTo(1250, canvasHeight - (i * 10 * scale + 10));	
	}
	ctx.stroke();
	
	// signal
	let pomColor    = '#00FF00';
	for (let i = 0; i < signal['signal'].length - 1; i++) {
		if ((i >= signal['boundaryPoints'][0]) && (i <= signal['boundaryPoints'][signal['boundaryPoints'].length - 1])) {
			ctx.globalAlpha = 1;
			pomColor    = '#00FF00';
		} else {
			ctx.globalAlpha = 0.1;
			pomColor    = '#FF0000';
		}
			
		point_n((i * XstepSize) + 55,  signal['signal'][i] / graphScaleFactor + cy - mean, pomColor, 5);
		line_n((i * XstepSize) + 55, signal['signal'][i] / graphScaleFactor + cy - mean, ((i + 1) * XstepSize) + 55, signal['signal'][i + 1] / graphScaleFactor + cy - mean, pomColor);
	}
	
	ctx.globalAlpha = 1;
}

function drawLegendHeight() {
	let pomcX = 350 - scale * player.baseBodyFrame['Head'].x;
	let pomcZ = 900 - scale * player.baseBodyFrame['Head'].z;
	clear_canvas();
	legend();
	
	drawBaseSkeletSideAndFront(pomcX, pomcZ);

	ctx.lineWidth = 4;
	
	// from head to hips
	line_n(	scale * player.baseBodyFrame['Head'].x 			+ pomcX, scale * player.baseBodyFrame['Head'].y 			+ cy, 
			scale * player.baseBodyFrame['Neck'].x 			+ pomcX, scale * player.baseBodyFrame['Neck'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['Neck'].x 			+ pomcX, scale * player.baseBodyFrame['Neck'].y 			+ cy, 
			scale * player.baseBodyFrame['Spine1'].x 		+ pomcX, scale * player.baseBodyFrame['Spine1'].y 			+ cy, legendColor);
			
	if (player.baseBodyFrame['Spine']) {
		line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ pomcX, scale * player.baseBodyFrame['Spine1'].y 			+ cy, 
			scale * player.baseBodyFrame['Spine'].x			+ pomcX, scale * player.baseBodyFrame['Spine'].y 			+ cy, legendColor);
		line_n(	scale * player.baseBodyFrame['Spine'].x 		+ pomcX, scale * player.baseBodyFrame['Spine'].y 			+ cy, 
			scale * player.baseBodyFrame['ToSpine'].x 		+ pomcX, scale * player.baseBodyFrame['ToSpine'].y 			+ cy, legendColor);
	} else {
		line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ pomcX, scale * player.baseBodyFrame['Spine1'].y 			+ cy, 
			scale * player.baseBodyFrame['ToSpine'].x 		+ pomcX, scale * player.baseBodyFrame['ToSpine'].y 			+ cy, legendColor);
	}
	
	line_n(	scale * player.baseBodyFrame['ToSpine'].x 		+ pomcX, scale * player.baseBodyFrame['ToSpine'].y 			+ cy, 
			scale * player.baseBodyFrame['Hips'].x 			+ pomcX, scale * player.baseBodyFrame['Hips'].y 			+ cy, legendColor);

	
	// from head to hips		
	line_n(	scale * player.baseBodyFrame['Head'].z 			+ pomcZ, scale * player.baseBodyFrame['Head'].y 		+ cy, 
			scale * player.baseBodyFrame['Neck'].z 			+ pomcZ, scale * player.baseBodyFrame['Neck'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['Neck'].z 			+ pomcZ, scale * player.baseBodyFrame['Neck'].y 		+ cy, 
			scale * player.baseBodyFrame['Spine1'].z 		+ pomcZ, scale * player.baseBodyFrame['Spine1'].y 	+ cy, legendColor);
			
	if (player.baseBodyFrame['Spine']) {
		line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ pomcZ, scale * player.baseBodyFrame['Spine1'].y 	+ cy, 
			scale * player.baseBodyFrame['Spine'].z			+ pomcZ, scale * player.baseBodyFrame['Spine'].y 		+ cy, legendColor);
		line_n(	scale * player.baseBodyFrame['Spine'].z 		+ pomcZ, scale * player.baseBodyFrame['Spine'].y 		+ cy, 
			scale * player.baseBodyFrame['ToSpine'].z 		+ pomcZ, scale * player.baseBodyFrame['ToSpine'].y	+ cy, legendColor);
	} else {
		line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ pomcZ, scale * player.baseBodyFrame['Spine1'].y 	+ cy, 
			scale * player.baseBodyFrame['ToSpine'].z 		+ pomcZ, scale * player.baseBodyFrame['ToSpine'].y	+ cy, legendColor);
	}
	
	line_n(	scale * player.baseBodyFrame['ToSpine'].z 		+ pomcZ, scale * player.baseBodyFrame['ToSpine'].y	+ cy, 
			scale * player.baseBodyFrame['Hips'].z 			+ pomcZ, scale * player.baseBodyFrame['Hips'].y 		+ cy, legendColor);
			
	ctx.lineWidth = 1;
}

function drawLegendLeftArm() {
	let pomcX = 350 - scale * player.baseBodyFrame['Head'].x;
	let pomcZ = 900 - scale * player.baseBodyFrame['Head'].z;

	clear_canvas();
	legend();
	
	drawBaseSkeletSideAndFront(pomcX, pomcZ);
	
	ctx.lineWidth = 4;
	
	line_n(	scale * player.baseBodyFrame['LeftShoulder'].x 	+ pomcX, scale * player.baseBodyFrame['LeftShoulder'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftArm'].x 		+ pomcX, scale * player.baseBodyFrame['LeftArm'].y 				+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftArm'].x 		+ pomcX, scale * player.baseBodyFrame['LeftArm'].y 				+ cy, 
			scale * player.baseBodyFrame['LeftForeArm'].x	+ pomcX, scale * player.baseBodyFrame['LeftForeArm'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftForeArm'].x 	+ pomcX, scale * player.baseBodyFrame['LeftForeArm'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftHand'].x 		+ pomcX, scale * player.baseBodyFrame['LeftHand'].y 			+ cy, legendColor);
			
	line_n(	scale * player.baseBodyFrame['LeftShoulder'].z 	+ pomcZ, scale * player.baseBodyFrame['LeftShoulder'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftArm'].z 		+ pomcZ, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftArm'].z 		+ pomcZ, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftForeArm'].z	+ pomcZ, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftForeArm'].z 	+ pomcZ, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftHand'].z 		+ pomcZ, scale * player.baseBodyFrame['LeftHand'].y 		+ cy, legendColor);
	
	ctx.lineWidth = 1;
}

function drawLegendRightArm() {
	let pomcX = 350 - scale * player.baseBodyFrame['Head'].x;
	let pomcZ = 900 - scale * player.baseBodyFrame['Head'].z;
		
	clear_canvas();
	legend();
	
	drawBaseSkeletSideAndFront(pomcX, pomcZ);
	
	ctx.lineWidth = 4;
	
	line_n(	scale * player.baseBodyFrame['RightShoulder'].x + pomcX, scale * player.baseBodyFrame['RightShoulder'].y 		+ cy, 
			scale * player.baseBodyFrame['RightArm'].x 		+ pomcX, scale * player.baseBodyFrame['RightArm'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightArm'].x 		+ pomcX, scale * player.baseBodyFrame['RightArm'].y 			+ cy, 
			scale * player.baseBodyFrame['RightForeArm'].x	+ pomcX, scale * player.baseBodyFrame['RightForeArm'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightForeArm'].x 	+ pomcX, scale * player.baseBodyFrame['RightForeArm'].y 		+ cy, 
			scale * player.baseBodyFrame['RightHand'].x 	+ pomcX, scale * player.baseBodyFrame['RightHand'].y 			+ cy, legendColor);

	line_n(	scale * player.baseBodyFrame['RightShoulder'].z + pomcZ, scale * player.baseBodyFrame['RightShoulder'].y 	+ cy, 
			scale * player.baseBodyFrame['RightArm'].z 		+ pomcZ, scale * player.baseBodyFrame['RightArm'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightArm'].z 		+ pomcZ, scale * player.baseBodyFrame['RightArm'].y 		+ cy, 
			scale * player.baseBodyFrame['RightForeArm'].z	+ pomcZ, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightForeArm'].z 	+ pomcZ, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['RightHand'].z 	+ pomcZ, scale * player.baseBodyFrame['RightHand'].y 		+ cy, legendColor);
	
	ctx.lineWidth = 1;
}

function drawLegendLeftLeg() {
	let pomcX = 350 - scale * player.baseBodyFrame['Head'].x;
	let pomcZ = 900 - scale * player.baseBodyFrame['Head'].z;
		
	clear_canvas();
	legend();
	
	drawBaseSkeletSideAndFront(pomcX, pomcZ);
	
	ctx.lineWidth = 4;
	
	line_n(	scale * player.baseBodyFrame['Hips'].x 		  + pomcX, scale * player.baseBodyFrame['Hips'].y 				+ cy, 
			scale * player.baseBodyFrame['LeftUpLeg'].x   + pomcX, scale * player.baseBodyFrame['LeftUpLeg'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftUpLeg'].x   + pomcX, scale * player.baseBodyFrame['LeftUpLeg'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftLeg'].x 	  + pomcX, scale * player.baseBodyFrame['LeftLeg'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftLeg'].x 	  + pomcX, scale * player.baseBodyFrame['LeftLeg'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftFoot'].x	  + pomcX, scale * player.baseBodyFrame['LeftFoot'].y 			+ cy, legendColor);
	
	if (player.baseBodyFrame['LeftToeBase']) {
		line_n(	scale * player.baseBodyFrame['LeftFoot'].x 	  + pomcX, scale * player.baseBodyFrame['LeftFoot'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftToeBase'].x + pomcX, scale * player.baseBodyFrame['LeftToeBase'].y 		+ cy, legendColor);
	}
	
	line_n(	scale * player.baseBodyFrame['Hips'].z 		  + pomcZ, scale * player.baseBodyFrame['Hips'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftUpLeg'].z   + pomcZ, scale * player.baseBodyFrame['LeftUpLeg'].y 	+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftUpLeg'].z   + pomcZ, scale * player.baseBodyFrame['LeftUpLeg'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftLeg'].z 	  + pomcZ, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['LeftLeg'].z 	  + pomcZ, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftFoot'].z	  + pomcZ, scale * player.baseBodyFrame['LeftFoot'].y 	+ cy, legendColor);
	
	if (player.baseBodyFrame['LeftToeBase']) {
		line_n(	scale * player.baseBodyFrame['LeftFoot'].z 	  + pomcZ, scale * player.baseBodyFrame['LeftFoot'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftToeBase'].z + pomcZ, scale * player.baseBodyFrame['LeftToeBase'].y 	+ cy, legendColor);
	}
	
	ctx.lineWidth = 1;
}

function drawLegendRightLeg() {
	let pomcX = 350 - scale * player.baseBodyFrame['Head'].x;
	let pomcZ = 900 - scale * player.baseBodyFrame['Head'].z;
	
	clear_canvas();
	legend();
	
	drawBaseSkeletSideAndFront(pomcX, pomcZ);
	
	ctx.lineWidth = 4;
	
	line_n(	scale * player.baseBodyFrame['Hips'].x 			+ pomcX, scale * player.baseBodyFrame['Hips'].y 				+ cy, 
			scale * player.baseBodyFrame['RightUpLeg'].x 	+ pomcX, scale * player.baseBodyFrame['RightUpLeg'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightUpLeg'].x 	+ pomcX, scale * player.baseBodyFrame['RightUpLeg'].y 			+ cy, 
			scale * player.baseBodyFrame['RightLeg'].x 		+ pomcX, scale * player.baseBodyFrame['RightLeg'].y 			+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightLeg'].x 		+ pomcX, scale * player.baseBodyFrame['RightLeg'].y 			+ cy, 
			scale * player.baseBodyFrame['RightFoot'].x		+ pomcX, scale * player.baseBodyFrame['RightFoot'].y 			+ cy, legendColor);
	
	if (player.baseBodyFrame['RightToeBase']) {
		line_n(	scale * player.baseBodyFrame['RightFoot'].x 	+ pomcX, scale * player.baseBodyFrame['RightFoot'].y 			+ cy, 
			scale * player.baseBodyFrame['RightToeBase'].x 	+ pomcX, scale * player.baseBodyFrame['RightToeBase'].y 		+ cy, legendColor);
	}
	
	
	line_n(	scale * player.baseBodyFrame['Hips'].z 			+ pomcZ, scale * player.baseBodyFrame['Hips'].y 			+ cy, 
			scale * player.baseBodyFrame['RightUpLeg'].z 	+ pomcZ, scale * player.baseBodyFrame['RightUpLeg'].y 	+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightUpLeg'].z 	+ pomcZ, scale * player.baseBodyFrame['RightUpLeg'].y 	+ cy, 
			scale * player.baseBodyFrame['RightLeg'].z 		+ pomcZ, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, legendColor);
	line_n(	scale * player.baseBodyFrame['RightLeg'].z 		+ pomcZ, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['RightFoot'].z		+ pomcZ, scale * player.baseBodyFrame['RightFoot'].y 		+ cy, legendColor);
	
	if (player.baseBodyFrame['RightToeBase']) {
		line_n(	scale * player.baseBodyFrame['RightFoot'].z 	+ pomcZ, scale * player.baseBodyFrame['RightFoot'].y 		+ cy, 
			scale * player.baseBodyFrame['RightToeBase'].z 	+ pomcZ, scale * player.baseBodyFrame['RightToeBase'].y 	+ cy, legendColor);
	}
	
	ctx.lineWidth = 1;
}

function drawBaseSkeletSideAndFront(cx1, cz1) {
	ctx.lineWidth = 1;
	
	axis('Z','Y','X');
	for (let part in player.baseBodyFrame) {
		if (part != '-') {
			color = getColor(part);		
			point_n(scale * player.baseBodyFrame[part].x + cx1, scale * player.baseBodyFrame[part].y + cy, color, 6);
		}
	}
	
	// left arm
	line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ cx1, scale * player.baseBodyFrame['Spine1'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftShoulder'].x 	+ cx1, scale * player.baseBodyFrame['LeftShoulder'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftShoulder'].x 	+ cx1, scale * player.baseBodyFrame['LeftShoulder'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftArm'].x 		+ cx1, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftArm'].x 		+ cx1, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftForeArm'].x	+ cx1, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftForeArm'].x 	+ cx1, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftHand'].x 		+ cx1, scale * player.baseBodyFrame['LeftHand'].y 		+ cy, baseColor);

	// rigth arm                                                
	line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ cx1, scale * player.baseBodyFrame['Spine1'].y 			+ cy, 
			scale * player.baseBodyFrame['RightShoulder'].x + cx1, scale * player.baseBodyFrame['RightShoulder'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightShoulder'].x + cx1, scale * player.baseBodyFrame['RightShoulder'].y 	+ cy, 
			scale * player.baseBodyFrame['RightArm'].x 		+ cx1, scale * player.baseBodyFrame['RightArm'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightArm'].x 		+ cx1, scale * player.baseBodyFrame['RightArm'].y 		+ cy, 
			scale * player.baseBodyFrame['RightForeArm'].x	+ cx1, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightForeArm'].x 	+ cx1, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['RightHand'].x 	+ cx1, scale * player.baseBodyFrame['RightHand'].y 		+ cy, baseColor);

	// from head to hips    
	line_n(	scale * player.baseBodyFrame['Head'].x 			+ cx1, scale * player.baseBodyFrame['Head'].y 			+ cy, 
			scale * player.baseBodyFrame['Neck'].x 			+ cx1, scale * player.baseBodyFrame['Neck'].y 			+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['Neck'].x 			+ cx1, scale * player.baseBodyFrame['Neck'].y 			+ cy, 
			scale * player.baseBodyFrame['Spine1'].x 		+ cx1, scale * player.baseBodyFrame['Spine1'].y 			+ cy, baseColor);
	
	if (player.baseBodyFrame['Spine']) {
		line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ cx1, scale * player.baseBodyFrame['Spine1'].y 			+ cy, 
			scale * player.baseBodyFrame['Spine'].x			+ cx1, scale * player.baseBodyFrame['Spine'].y 			+ cy, baseColor);
		line_n(	scale * player.baseBodyFrame['Spine'].x 		+ cx1, scale * player.baseBodyFrame['Spine'].y 			+ cy, 
			scale * player.baseBodyFrame['ToSpine'].x 		+ cx1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, baseColor);
	} else {
		line_n(	scale * player.baseBodyFrame['Spine1'].x 		+ cx1, scale * player.baseBodyFrame['Spine1'].y 			+ cy,  
			scale * player.baseBodyFrame['ToSpine'].x 		+ cx1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, baseColor);
	}
	
	line_n(	scale * player.baseBodyFrame['ToSpine'].x 		+ cx1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, 
			scale * player.baseBodyFrame['Hips'].x 			+ cx1, scale * player.baseBodyFrame['Hips'].y 			+ cy, baseColor);

	// left leg         
	line_n(	scale * player.baseBodyFrame['Hips'].x 		  	+ cx1, scale * player.baseBodyFrame['Hips'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftUpLeg'].x   	+ cx1, scale * player.baseBodyFrame['LeftUpLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftUpLeg'].x   	+ cx1, scale * player.baseBodyFrame['LeftUpLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftLeg'].x 	  	+ cx1, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftLeg'].x 	  	+ cx1, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftFoot'].x	  	+ cx1, scale * player.baseBodyFrame['LeftFoot'].y 		+ cy, baseColor);
	
	if (player.baseBodyFrame['LeftToeBase']) {
		line_n(	scale * player.baseBodyFrame['LeftFoot'].x 	  	+ cx1, scale * player.baseBodyFrame['LeftFoot'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftToeBase'].x 	+ cx1, scale * player.baseBodyFrame['LeftToeBase'].y 	+ cy, baseColor);
	}
	
	// right leg 
	line_n(	scale * player.baseBodyFrame['Hips'].x 			+ cx1, scale * player.baseBodyFrame['Hips'].y 			+ cy, 
			scale * player.baseBodyFrame['RightUpLeg'].x 	+ cx1, scale * player.baseBodyFrame['RightUpLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightUpLeg'].x 	+ cx1, scale * player.baseBodyFrame['RightUpLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['RightLeg'].x 		+ cx1, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightLeg'].x 		+ cx1, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['RightFoot'].x		+ cx1, scale * player.baseBodyFrame['RightFoot'].y 		+ cy, baseColor);
	if (player.baseBodyFrame['RightToeBase']) {
		line_n(	scale * player.baseBodyFrame['RightFoot'].x 	+ cx1, scale * player.baseBodyFrame['RightFoot'].y 		+ cy, 
			scale * player.baseBodyFrame['RightToeBase'].x 	+ cx1, scale * player.baseBodyFrame['RightToeBase'].y 	+ cy, baseColor);
	}
	
	//-------------------------------side view-----------------------------------------------------------------------------------------------------
	axis('X','Y','Z', 600);
	for (let part in player.baseBodyFrame) {
		if (part != '-') {
			color = getColor(part);		
			point_n(scale * player.baseBodyFrame[part].z + cz1, scale * player.baseBodyFrame[part].y + cy, color, 6);
		}
	}
	
	// left arm
	line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ cz1, scale * player.baseBodyFrame['Spine1'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftShoulder'].z 	+ cz1, scale * player.baseBodyFrame['LeftShoulder'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftShoulder'].z 	+ cz1, scale * player.baseBodyFrame['LeftShoulder'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftArm'].z 		+ cz1, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftArm'].z 		+ cz1, scale * player.baseBodyFrame['LeftArm'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftForeArm'].z	+ cz1, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftForeArm'].z 	+ cz1, scale * player.baseBodyFrame['LeftForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftHand'].z 		+ cz1, scale * player.baseBodyFrame['LeftHand'].y 		+ cy, baseColor);

	// rigth arm 
	line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ cz1, scale * player.baseBodyFrame['Spine1'].y 		+ cy, 
			scale * player.baseBodyFrame['RightShoulder'].z + cz1, scale * player.baseBodyFrame['RightShoulder'].y + cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightShoulder'].z + cz1, scale * player.baseBodyFrame['RightShoulder'].y + cy, 
			scale * player.baseBodyFrame['RightArm'].z 		+ cz1, scale * player.baseBodyFrame['RightArm'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightArm'].z 		+ cz1, scale * player.baseBodyFrame['RightArm'].y 		+ cy, 
			scale * player.baseBodyFrame['RightForeArm'].z	+ cz1, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightForeArm'].z 	+ cz1, scale * player.baseBodyFrame['RightForeArm'].y 	+ cy, 
			scale * player.baseBodyFrame['RightHand'].z 	+ cz1, scale * player.baseBodyFrame['RightHand'].y 	+ cy, baseColor);

	// from head to hips	
	line_n(	scale * player.baseBodyFrame['Head'].z 			+ cz1, scale * player.baseBodyFrame['Head'].y 			+ cy, 
			scale * player.baseBodyFrame['Neck'].z 			+ cz1, scale * player.baseBodyFrame['Neck'].y 			+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['Neck'].z 			+ cz1, scale * player.baseBodyFrame['Neck'].y 			+ cy, 
			scale * player.baseBodyFrame['Spine1'].z 		+ cz1, scale * player.baseBodyFrame['Spine1'].y 		+ cy, baseColor);
			
	if (player.baseBodyFrame['Spine']) {
		line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ cz1, scale * player.baseBodyFrame['Spine1'].y 		+ cy, 
			scale * player.baseBodyFrame['Spine'].z			+ cz1, scale * player.baseBodyFrame['Spine'].y 		+ cy, baseColor);
		line_n(	scale * player.baseBodyFrame['Spine'].z 		+ cz1, scale * player.baseBodyFrame['Spine'].y 		+ cy, 
			scale * player.baseBodyFrame['ToSpine'].z 		+ cz1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, baseColor);
	} else {
		line_n(	scale * player.baseBodyFrame['Spine1'].z 		+ cz1, scale * player.baseBodyFrame['Spine1'].y 		+ cy, 
			scale * player.baseBodyFrame['ToSpine'].z 		+ cz1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, baseColor);
	}
	
	line_n(	scale * player.baseBodyFrame['ToSpine'].z 		+ cz1, scale * player.baseBodyFrame['ToSpine'].y 		+ cy, 
			scale * player.baseBodyFrame['Hips'].z 			+ cz1, scale * player.baseBodyFrame['Hips'].y 			+ cy, baseColor);

	// left leg  
	line_n(	scale * player.baseBodyFrame['Hips'].z 		  	+ cz1, scale * player.baseBodyFrame['Hips'].y 			+ cy, 
			scale * player.baseBodyFrame['LeftUpLeg'].z   	+ cz1, scale * player.baseBodyFrame['LeftUpLeg'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftUpLeg'].z   	+ cz1, scale * player.baseBodyFrame['LeftUpLeg'].y 	+ cy, 
			scale * player.baseBodyFrame['LeftLeg'].z 	  	+ cz1, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['LeftLeg'].z 	  	+ cz1, scale * player.baseBodyFrame['LeftLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftFoot'].z	  	+ cz1, scale * player.baseBodyFrame['LeftFoot'].y 		+ cy, baseColor);
	
	if (player.baseBodyFrame['LeftToeBase']) {		
		line_n(	scale * player.baseBodyFrame['LeftFoot'].z 	  	+ cz1, scale * player.baseBodyFrame['LeftFoot'].y 		+ cy, 
			scale * player.baseBodyFrame['LeftToeBase'].z 	+ cz1, scale * player.baseBodyFrame['LeftToeBase'].y 	+ cy, baseColor);			
	}
	
	// right leg  
	line_n(	scale * player.baseBodyFrame['Hips'].z 			+ cz1, scale * player.baseBodyFrame['Hips'].y 			+ cy, 
			scale * player.baseBodyFrame['RightUpLeg'].z 	+ cz1, scale * player.baseBodyFrame['RightUpLeg'].y 	+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightUpLeg'].z 	+ cz1, scale * player.baseBodyFrame['RightUpLeg'].y 	+ cy, 
			scale * player.baseBodyFrame['RightLeg'].z 		+ cz1, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, baseColor);
	line_n(	scale * player.baseBodyFrame['RightLeg'].z 		+ cz1, scale * player.baseBodyFrame['RightLeg'].y 		+ cy, 
			scale * player.baseBodyFrame['RightFoot'].z		+ cz1, scale * player.baseBodyFrame['RightFoot'].y 	+ cy, baseColor);
	
	if (player.baseBodyFrame['RightToeBase']) {
		line_n(	scale * player.baseBodyFrame['RightFoot'].z 	+ cz1, scale * player.baseBodyFrame['RightFoot'].y 	+ cy, 
			scale * player.baseBodyFrame['RightToeBase'].z 	+ cz1, scale * player.baseBodyFrame['RightToeBase'].y 	+ cy, baseColor);
	}
}

function clear_canvas() {
	ctx.clearRect(0, 0, c.width, c.height);
}
