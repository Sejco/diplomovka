function Bvh_play(array_of_content, file_name) {	
	this.bvh = new Bvh_parse(array_of_content, file_name);
	this.bvh.create_body_structure();
	this.bvh.prepare_motions();
	this.bvh.calculate_coords();
	
	this.walkFilter = new Walk_filter(this.bvh.body, this.bvh.frame_time);
	this.walkFilter.init();	
	
	this.file_name 		 = file_name;
	this.num_frames 	 = Math.floor(this.bvh.num_frames / this.walkFilter.divider) - 1;
	this.frame_time		 = this.bvh.frame_time * this.walkFilter.divider;
	this.body_anim_root	 = this.walkFilter.body;
	this.filteredFrames  = this.walkFilter.filterWalkGetObject();
	
	this.draw_trajectory = false;
	this.play_anim		 = true; 

	var frame = 3;
	var timer;
	var color;
	
	this.init = function() {
		this.setZaxixShift();
		this.writeHeading();
	}
	
	this.draw = function(frame) {	
		this.writeCurFrame(frame);
		this.writeCurTime(frame);
		this.draw_frame(frame);
		ctx.clearRect(0, 280, 70, 70);
		axis('X','Y','Z', -10, -140);
	}
	
	this.play = function() {
		let that  = this;
			
		function next_frame() {
			frame++;
			if (frame > that.num_frames){
				frame = 3;
			}
			if (!that.draw_trajectory) {
				clear_canvas();	
			}
			
			that.draw(frame);
		}
		
		next_frame();
		
		timer = setInterval(function(){ next_frame() }, 1000 * this.frame_time);	
	}
	
	this.pause = function() {
		clearInterval(timer);
	}
	
	
	this.changePlayStop = function() {
		this.play_anim = !this.play_anim;
		
		if (this.play_anim) {
			this.play();
			$('#playPauseButton').html('<span class="glyphicon glyphicon-pause"></span>');
			$('#playPauseButton').attr({'title' : 'Pause'});
		} else {
			this.pause();
			$('#playPauseButton').html('<span class="glyphicon glyphicon-play"></span>');
			$('#playPauseButton').attr({'title' : 'Play'});
		}
	}
	
	this.repeatAnimation = function() {
		frame = 3;
		
		if (!this.draw_trajectory) {
			clear_canvas();	
		}
	
		this.draw(frame);
	}
	
	this.nextFrame = function(){
		frame++;
		if (frame > this.num_frames){
			frame = 3;
		}
		
		if (!this.draw_trajectory) {
			clear_canvas();	
		}
	
		this.draw(frame);
	}
	
	this.prevFrame = function() {
		frame--;
		if (frame < 3){
			frame = this.num_frames;
		}
		
		if (!this.draw_trajectory) {
			clear_canvas();	
		}
		
		this.draw(frame);
	}
	
	this.changeShowTraj = function() {
		this.draw_trajectory = !this.draw_trajectory;
		
		if (this.draw_trajectory) {
			$('#trajectoryButton').html('<span class="glyphicon glyphicon-eye-close"></span>');
			$('#trajectoryButton').attr({'title' : 'Skryť trajektóriu'});
		} else {
			$('#trajectoryButton').html('<span class="glyphicon glyphicon-eye-open"></span>');
			$('#trajectoryButton').attr({'title' : 'Ukázať trajektóriu'});
			
			clear_canvas();	
			
			this.draw(frame);
		}
	}
	
	this.draw_walk_sequence_side = function(){	
		var that = this;
		this.setCy2();
		
		ctx.lineWidth = 1;

		function draw_child_node(n, node, parent_x, parent_y, parent_z) {
			if (node.part != '-') {	
				color = getColor(node.part);
				
				line_n(scale * parent_z + cx, scale * parent_y + cy, scale * node.coords[n].z + cx, scale * node.coords[n].y + cy, '#444444');
				point_n(scale * node.coords[n].z + cx, scale * node.coords[n].y + cy, color, 6);
			}
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					draw_child_node(n, node.joints[i], node.coords[n].x, node.coords[n].y, node.coords[n].z);
				}
			}
		}
		
		for (let j = 0; j < this.body_anim_root.coords.length; j++){
			color = getColor(this.body_anim_root.part);
			point_n(scale * this.body_anim_root.coords[j].z + cx, scale * this.body_anim_root.coords[j].y + cy, color, 6);
			
			for (let i = 0; i < this.body_anim_root.joints.length; i++){
				draw_child_node(j, this.body_anim_root.joints[i], this.body_anim_root.coords[j].x, this.body_anim_root.coords[j].y, this.body_anim_root.coords[j].z);
			}
		}
	}
	
	this.draw_walk_sequence_side_focusing = function(focusedBodyParts){	
		var that 			= this;

		ctx.lineWidth = 1;
	
		function draw_child_node(n, node, parent_x, parent_y, parent_z) {
			if (node.part != '-') {	
				color = getColor(node.part);
				
				setOpacityForBodyPart(node.part, focusedBodyParts);
				if (focusedBodyParts.length > 1) {
					line_n(scale * parent_z + cx, scale * parent_y + cy, scale * node.coords[n].z + cx, scale * node.coords[n].y + cy, '#444444');
				}
				point_n(scale * node.coords[n].z + cx, scale * node.coords[n].y + cy, color, 6);
			}
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					draw_child_node(n, node.joints[i], node.coords[n].x, node.coords[n].y, node.coords[n].z);
				}
			}
		}
		
		for (let j = 0; j < this.body_anim_root.coords.length; j++){
			setOpacityForBodyPart(this.body_anim_root.part, focusedBodyParts);
			color = getColor(this.body_anim_root.part);
			point_n(scale * this.body_anim_root.coords[j].z + cx, scale * this.body_anim_root.coords[j].y + cy, color, 6);
			
			for (let i = 0; i < this.body_anim_root.joints.length; i++){
				draw_child_node(j, this.body_anim_root.joints[i], this.body_anim_root.coords[j].x, this.body_anim_root.coords[j].y, this.body_anim_root.coords[j].z);
			}
		}
	}
	
	this.draw_walk_sequence_front = function(){	
		var that = this;
		
		ctx.lineWidth = 1;
	
		function draw_child_node(n, node, parent_x, parent_y, parent_z) {
			if (node.part != '-') {	
				color = getColor(node.part);
				
				line_n(scale * parent_x + cx, scale * parent_y + cy, scale * node.coords[n][0] + cx, scale * node.coords[n][1] + cy, '#444444');
				point_n(scale * node.coords[n][0] + cx, scale * node.coords[n][1] + cy, color, 6);
			}
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					draw_child_node(n, node.joints[i], node.coords[n][0], node.coords[n][1], node.coords[n][2]);
				}
			}
		}
		
		for (let j = this.filteredFrames.start; j < this.filteredFrames.end; j++){
			point_n(scale * this.body_anim_root.coords[j][0] + cx, scale * this.body_anim_root.coords[j][1] + cy, undefined, 6);
			
			for (let i = 0; i < this.body_anim_root.joints.length; i++){
				draw_child_node(j, this.body_anim_root.joints[i], this.body_anim_root.coords[j][0], this.body_anim_root.coords[j][1], this.body_anim_root.coords[j][2]);
			}
		}
	}
	
	this.draw_walk_sequence_up = function(focusedBodyParts){	
		var that  = this;
		this.setYaxis(scale * this.body_anim_root.coords[0].x + cy);
		
		ctx.lineWidth = 1;

		function draw_child_node(n, node, parent_x, parent_y, parent_z) {
			if (node.part != '-') {	
				color = getColor(node.part);
				
				setOpacityForBodyPart(node.part, focusedBodyParts);
				line_n(scale * parent_z + cx, scale * parent_x + upCy, scale * node.coords[n].z + cx, scale * node.coords[n].x + upCy, '#444444');
				point_n(scale * node.coords[n].z + cx, scale * node.coords[n].x + upCy, color, 6);
			}
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					draw_child_node(n, node.joints[i], node.coords[n].x, node.coords[n].y, node.coords[n].z);
				}
			}
		}
		
		for (let j = 0; j < this.body_anim_root.coords.length; j++){
			setOpacityForBodyPart(this.body_anim_root.part, focusedBodyParts);
			point_n(scale * this.body_anim_root.coords[j].z + cx, scale * this.body_anim_root.coords[j].x + upCy, undefined, 6);
			
			for (let i = 0; i < this.body_anim_root.joints.length; i++){
				draw_child_node(j, this.body_anim_root.joints[i], this.body_anim_root.coords[j].x, this.body_anim_root.coords[j].y, this.body_anim_root.coords[j].z);
			}
		}
	}
	
	
	this.draw_frame = function(n) {		
		var that = this;
		this.setCy();
		
		function draw_child_node(node, parent_x, parent_y, parent_z) {
			color = getColor(node.part);
			
			if (node.part != '-') {	
				if ((n > that.filteredFrames.start) && (n < that.filteredFrames.end)) {
					line_n(scale * parent_z + cx, scale * parent_y + cy, scale * node.coords[n][2] + cx, scale * node.coords[n][1] + cy, '#00CC33');
				} else {
					line_n(scale * parent_z + cx, scale * parent_y + cy, scale * node.coords[n][2] + cx, scale * node.coords[n][1] + cy);
				}
				
				point_n(scale * node.coords[n][2] + cx, scale * node.coords[n][1] + cy, color, 4);
			}
			
			
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					draw_child_node(node.joints[i], node.coords[n][0], node.coords[n][1], node.coords[n][2]);
				}
			}
		}
		
		color = getColor(this.body_anim_root.part);
		point_n(scale * this.body_anim_root.coords[n][2] + cx, scale * this.body_anim_root.coords[n][1] + cy, color, 4);
		
		for (let i = 0; i < this.body_anim_root.joints.length; i++){
			draw_child_node(this.body_anim_root.joints[i], this.body_anim_root.coords[n][0], this.body_anim_root.coords[n][1], this.body_anim_root.coords[n][2]);
		}
	}
	
	this.setYaxis = function(y) {
		upCy = cy - y + 290;
	}
	
	this.setZaxixShift = function() {
		cx = (550 - this.body_anim_root.coords[3][2]) * scale;
	}
	
	this.setZaxixShiftForFilteredWalk = function(hipsCoord) {
		cx = 200 - hipsCoord[0].z * scale;
	}
	
	this.setCy = function() {
		cy = 200 - this.body_anim_root.coords[3][1] * scale;
	}
	
	this.setCy2 = function() {
		cy = 300 - this.body_anim_root.coords[3].y * scale;
	}
	
	this.writeHeading = function() {
		document.getElementById('fileName').innerHTML = this.file_name;
	} 
	
	this.writeCurFrame = function(f) {	
		function addZero(i) {
			let nuly = '';
			if (i < 100) {
				nuly += "0";
			}
			if (i < 10) {
				nuly += "0";
			}
			return nuly + '' + i;
		}	
		document.getElementById('frameCounter').innerHTML = addZero(f - 2)+"/"+addZero(this.num_frames - 2);
	}
	
	this.writeCurTime = function(f) {		
		let currentTime 	= (f - 2) * this.frame_time;
		let currentSec 		= Math.floor(currentTime);
		let currentMiliSec 	= Math.floor((currentTime - currentSec) * 100);
		let currentMin		= Math.floor(currentSec / 60);
		currentSec = Math.floor(currentSec % 60);
				
		let totalTime 		= (this.num_frames - 2) * this.frame_time;
		let totalSec 		= Math.floor(totalTime);
		let totalMiliSec 	= Math.floor((totalTime - totalSec) * 100);
		let totalMin		= Math.floor(totalSec / 60);
		totalSec = Math.floor(totalSec % 60);

		function addZero(i) {
			if (i < 10) {
				i = "0" + i;
			}
			return i;
		}

		document.getElementById('timeCounter').innerHTML = addZero(currentMin) + ':' + addZero(currentSec) + '.' + addZero(currentMiliSec) + '/' + addZero(totalMin) + ':' + addZero(totalSec) + '.' + addZero(totalMiliSec);
	}
}



