function Features(bodyPoints){
	this.decriptor = new Gait_descriptor(bodyPoints);
	this.allFeatures = [];
	
	this.allFeatures.height 				= this.decriptor.computePointTraceDistance(	   bodyPoints.Head, bodyPoints.Neck, bodyPoints.Spine1, bodyPoints.Spine, bodyPoints.ToSpine, bodyPoints.Hips);
	this.allFeatures.L_arm_length 			= this.decriptor.computePointTraceDistance(	   bodyPoints.LeftArm, bodyPoints.LeftForeArm, bodyPoints.LeftHand); 
	this.allFeatures.R_arm_length 			= this.decriptor.computePointTraceDistance(	   bodyPoints.RightArm, bodyPoints.RightForeArm, bodyPoints.RightHand); 
	this.allFeatures.L_leg_length 			= this.decriptor.computePointTraceDistance(	   bodyPoints.LeftUpLeg, bodyPoints.LeftLeg, bodyPoints.LeftFoot); 
	this.allFeatures.R_leg_length 			= this.decriptor.computePointTraceDistance(	   bodyPoints.RightUpLeg, bodyPoints.RightLeg, bodyPoints.RightFoot);
	this.allFeatures.head_leaning			= this.decriptor.computeHeadLeaning(		   bodyPoints.Head, 		bodyPoints.Neck, 'head_leaning');
	this.allFeatures.body_leaning			= this.decriptor.computeHeadLeaning(		   bodyPoints.Head, 		bodyPoints.Hips, 'body_leaning');
	this.allFeatures.L_arm_bend 			= this.decriptor.computeBend(	   			   [bodyPoints.LeftArm, bodyPoints.LeftForeArm, bodyPoints.LeftHand],		'L_arm_bend');
	this.allFeatures.R_arm_bend 			= this.decriptor.computeBend(	   			   [bodyPoints.RightArm, bodyPoints.RightForeArm, bodyPoints.RightHand],	'R_arm_bend');
	this.allFeatures.L_leg_bend 			= this.decriptor.computeBend(	   			   [bodyPoints.RightUpLeg, bodyPoints.RightLeg, bodyPoints.RightFoot],		'L_leg_bend');
	this.allFeatures.R_leg_bend 			= this.decriptor.computeBend(	   			   [bodyPoints.LeftUpLeg, bodyPoints.LeftLeg, bodyPoints.LeftFoot],			'R_leg_bend');		
	this.allFeatures.L_stride_length		= this.decriptor.computeStrideLength(		   bodyPoints.LeftFoot, 	'L_stride_length');
	this.allFeatures.R_stride_length		= this.decriptor.computeStrideLength(		   bodyPoints.RightFoot, 	'R_stride_length');
	this.allFeatures.step_width				= this.decriptor.computeStepWidth(			   bodyPoints.LeftFoot, 	bodyPoints.RightFoot, 'step_width');
	this.allFeatures.head_variance_x 		= this.decriptor.conputeMeanSquareError( 			   bodyPoints.Head, 		"x", 'head_variance_x');
	this.allFeatures.head_variance_y 		= this.decriptor.conputeMeanSquareError( 			   bodyPoints.Head, 		"y", 'head_variance_y');
	this.allFeatures.L_knee_variance_y 		= this.decriptor.conputeMeanSquareError(			   bodyPoints.LeftLeg, 		"y", 'L_knee_variance_y');
	this.allFeatures.R_knee_variance_y 		= this.decriptor.conputeMeanSquareError(			   bodyPoints.RightLeg, 	"y", 'R_knee_variance_y');
	this.allFeatures.L_shoulder_variance_x 	= this.decriptor.conputeMeanSquareError(			   bodyPoints.LeftShoulder, "x", 'L_shoulder_variance_x');
	this.allFeatures.R_shoulder_variance_x 	= this.decriptor.conputeMeanSquareError(			   bodyPoints.RightShoulder,"x", 'R_shoulder_variance_x');
	this.allFeatures.walk_speed 			= this.decriptor.computeWalkSpeed(			   bodyPoints.Hips, 		player.frame_time, 'walk_speed');
	this.allFeatures.hands_distances 		= this.decriptor.computeDistances(	  		   bodyPoints.LeftHand, 	bodyPoints.RightHand	,'hands_distances');
	this.allFeatures.elbow_distances 		= this.decriptor.computeDistances(	  		   bodyPoints.LeftForeArm, 	bodyPoints.RightForeArm	,'elbow_distances');
	this.allFeatures.knee_distances  		= this.decriptor.computeDistances(	  		   bodyPoints.LeftLeg, 		bodyPoints.RightLeg		,'knee_distances');
	this.allFeatures.foot_distances  		= this.decriptor.computeDistances(	  		   bodyPoints.LeftFoot, 	bodyPoints.RightFoot	,'foot_distances');
	this.allFeatures.toe_distances  		= this.decriptor.computeDistances(	  		   bodyPoints.LeftToeBase, 	bodyPoints.RightToeBase	,'toe_distances');
	this.allFeatures.shouldersHorizontalRot	= this.decriptor.computeShoulderRotation(	   bodyPoints.LeftArm, 		bodyPoints.RightArm,'y', 'shouldersHorizontalRot');
	this.allFeatures.shouldersVericalRot	= this.decriptor.computeShoulderRotation(	   bodyPoints.LeftArm, 		bodyPoints.RightArm,'z', 'shouldersVericalRot');
	this.allFeatures.head_acceleration		= this.decriptor.computeAccelerationOfPointSet(bodyPoints.Head, 		player.frame_time, 'head_acceleration');
	this.allFeatures.hips_acceleration		= this.decriptor.computeAccelerationOfPointSet(bodyPoints.Hips, 		player.frame_time, 'hips_acceleration');
	this.allFeatures.left_fore_arm_acc		= this.decriptor.computeAccelerationOfPointSet(bodyPoints.LeftForeArm, 	player.frame_time, 'left_fore_arm_acc');
	this.allFeatures.right_fore_arm_acc		= this.decriptor.computeAccelerationOfPointSet(bodyPoints.RightForeArm, player.frame_time, 'right_fore_arm_acc');
	this.allFeatures.left_hand_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.LeftHand, 	player.frame_time, 'left_hand_acc');
	this.allFeatures.right_hand_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.RightHand, 	player.frame_time, 'right_hand_acc');
	this.allFeatures.left_leg_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.LeftLeg, 		player.frame_time, 'left_leg_acc');
	this.allFeatures.right_leg_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.RightLeg, 	player.frame_time, 'right_leg_acc');
	this.allFeatures.left_foot_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.LeftFoot, 	player.frame_time, 'left_foot_acc');
	this.allFeatures.right_foot_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.RightFoot, 	player.frame_time, 'right_foot_acc');
	this.allFeatures.left_toe_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.LeftToeBase, 	player.frame_time, 'left_toe_acc');
	this.allFeatures.right_toe_acc			= this.decriptor.computeAccelerationOfPointSet(bodyPoints.RightToeBase, player.frame_time, 'right_toe_acc');
	

	this.fillTableWithValues = function(){
		for (let featureName in this.allFeatures) {
			if (typeof this.allFeatures[featureName] == "number") {
				$("#" + featureName).text(this.allFeatures[featureName].toFixed(2));
			} else {
				if (this.allFeatures[featureName].signal_power !== undefined) {
					$("#" + featureName).text(this.allFeatures[featureName].signal_power.toFixed());
				} else {
					$("#" + featureName).text('-');
				}
			}
		}
	}
	
	this.fillTableWithValuesArrayConversion = function(){	
		for (let featureName in this.allFeatures) {
			if (typeof this.allFeatures[featureName] == "number") {
				if (isNaN(this.allFeatures[featureName])) {
					$("#" + featureName).val(0);
				} else {
					$("#" + featureName).val(this.allFeatures[featureName].toFixed(5));	// if value is a number 
				}				
			} else {
				$("#" + featureName).val(JSON.stringify(this.allFeatures[featureName]));			// if value is a signal
				$("#" + featureName).height(100);
			}
		}
	}
}

