﻿function Walk_filter(body, frameTime) {	

	this.cutFramesByFPS = function(body, divider) {		
		function cutFrames(node) {
			let pomCoords = [];
			
			for (let i = 3; i < node.coords.length; i++) {
				if (((i - 3) % divider) == 0) {
					pomCoords.push(node.coords[i]);
				}
			}
			
			node.coords = pomCoords;
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					cutFrames(node.joints[i]);
				}
			}
		}	
		cutFrames(body);
		
		return body;
	}
	
	var FPS 		  = Math.round(1 / frameTime);
	
	this.divider 	  = FPS / 30;
	this.body 		  = this.cutFramesByFPS(body, this.divider);
	this.frameTime    = frameTime * this.divider;
	this.hipsCoords   = [];
	this.directedWalk = [];
	this.directedBody = JSON.parse(JSON.stringify(this.body));
	this.boundaryPoints;

	this.init = function() {
		this.hipsCoords = this.getHipsCoords();
	}
	
	this.filterWalkGetObject = function() {
		return this.getStartAndEndFrameOfWalk(this.getLongestArrayOfConsecutiveFrames(this.checkStraightDirectionOfWalk(this.checkHipsVelocity())));
	}
	
	this.filterWalkGetArrays = function() {
		let boundaryPoints = this.getStartAndEndFrameOfWalk(this.getLongestArrayOfConsecutiveFrames(this.checkStraightDirectionOfWalk(this.checkHipsVelocity())));
		var outputArray    = [];
		
		function getBodyPartCoords(bodyPart) {
			outputArray[bodyPart.part] = [];	
			outputArray[bodyPart.part] = bodyPart.coords.slice(boundaryPoints.start, boundaryPoints.end);
			
			for (let i = 0; i < bodyPart.joints.length; i++) {
				if (bodyPart.joints[i] != null) {
					getBodyPartCoords(bodyPart.joints[i]);
				}
			}
		}
		
		getBodyPartCoords(this.body);

		return this.convertWalkArrayToPoints(this.computeRightWalkDirection(outputArray));
	}
	
	this.setRightDirectionOfBody = function() {
		var that = this;
		
		function setBodyPartCoords(bodyPart) {
			bodyPart.coords = that.directedWalk[bodyPart.part];
			
			for (let i = 0; i < bodyPart.joints.length; i++) {
				if (bodyPart.joints[i] != null) {
					setBodyPartCoords(bodyPart.joints[i]);
				}
			}
		}
		
		setBodyPartCoords(this.directedBody);
		
		return this.directedBody;
	}
	
	this.computeRightWalkDirection = function(filtered_walk) {
		/*
			rotates filtered sequence around Y-axis for analyse purpose 
		*/
		
		let outputArr = [];
		
		let a 		= filtered_walk.Hips[filtered_walk.Hips.length - 1][0] - filtered_walk.Hips[0][0];
		let b 		= filtered_walk.Hips[filtered_walk.Hips.length - 1][2] - filtered_walk.Hips[0][2];
		let start	= filtered_walk.Hips[0][2];
		let end		= filtered_walk.Hips[filtered_walk.Hips.length - 1][2];
		let α 		= adjacentAngleInTriangle(a, b);							// angle between start and end point of walk sequence
		let ß 		= this.checkWalkOrientaion(start, end);						// 0 or 180 depends of walk orienation
		
		for (let i in filtered_walk) {
			outputArr[i] = [];
			for (let j in filtered_walk[i]) {
				outputArr[i][j] = rotatePointY(filtered_walk[i][j], -α + ß);
			}
		}
		
		this.directedWalk = outputArr;

		return outputArr;
	}
	
	this.checkWalkOrientaion = function(start, end) {
		/*
			checks if walk orientation is along Z-axis
			
			input  => start, end point of walk sequence
			output => angle of desired rotation (0 or 180)
		*/
		
		if (start > end) {
			return 180;
		} else {
			return 0;
		}
	}
	
	this.convertWalkArrayToPoints = function(arr) {
		/*
			converts vector to point
			
			input  => array [x,y,z]
			output => Point(x,y,z)
		*/
		
		for (let i in arr){
			for (let j = 0; j < arr[i].length; j++){
				arr[i][j] = arrayToPoint(arr[i][j]);
			}
		}
		
		return arr;
	}
	
	this.getHipsCoords = function() {	
		/*
			extract only hips coords from entire sequence
			
			output => hips coordinates
		*/
	
		function checkPart(bodyPart) {
			if (bodyPart.part.toLowerCase() == 'hips'){
				return bodyPart.coords;  
			}
			
			for (let i = 0; i < bodyPart.joints.length; i++) {
				checkPart(bodyPart.joints[i]);
			}
		}
		
		return checkPart(this.body);
	}
	
	this.checkHipsVelocity = function() {
		/*
			filters out frames with acceleration from range 55-130 units
		*/
	
		let filteredFrames = [];
	
		for (let i = 0; i < this.hipsCoords.length - 10; i++) {
			let speed = distanceCoords3D(this.hipsCoords[i][0], this.hipsCoords[i][1], this.hipsCoords[i][2], this.hipsCoords[i + 10][0], this.hipsCoords[i + 10][1], this.hipsCoords[i + 10][2]) / (this.frameTime * (i + 10) - this.frameTime * (i));

			if ((speed > 30) && (speed < 200)) { 
				filteredFrames.push(i);
			}	
		}

		return filteredFrames;
		console.log(filteredFrames);
	}
	
	
	this.getLongestArrayOfConsecutiveFrames = function(frames) {
		/*
			if there is more than one walk sequence from accelartion filter, this function will choose the longer one
		*/
		
		var maxLenArr = []
		
		let sections  = [];
		let section   = [];
		
		
		for (let i = 0; i < frames.length; i++) {
			if ((frames[i] + 1) == frames[i + 1]) {
				section.push(frames[i]);
			} else {
				sections.push(section);
				section = [];
			}
		}	
		
		if (sections.length > 0) {
			maxLenArr = sections[0];
			for (let i = 1; i < sections.length; i++) {
				if (maxLenArr.length < sections[i].length) {
					maxLenArr = sections[i];
				}
			}
		}

		return (maxLenArr.length > 20 ? maxLenArr : undefined);
	}
	
	this.checkStraightDirectionOfWalk = function(frames) {
		/*
			checks if there is no turnings in the walk sequence
		*/
		
		let outpuFrames = [];
		
		for (let i = 0; i < frames.length - 14; i++) {
			let vec1 = createVectorArr(this.hipsCoords[frames[i]],this.hipsCoords[frames[i + 7]]);
			let vec2 = createVectorArr(this.hipsCoords[frames[i + 7]],this.hipsCoords[frames[i + 14]]);
			if (scalarProduct(vectorNormalize(vec1), vectorNormalize(vec2)) > 0.96) {
				outpuFrames.push(frames[i]);
			}
		}
		
		return outpuFrames;
	}
	
	this.getStartAndEndFrameOfWalk = function(arr) {
		var result = {start:0, end:0};
		
		if (typeof arr !== 'undefined'){ 
			if (arr.constructor === Array){	
				result.start = arr[0];
				result.end 	 = arr[arr.length - 1];
			}
		}
		
		this.boundaryPoints = result;
		
		return result;
	}
	
}







