function Gait_descriptor(bodyPoints){
	this.visualizationPoints = [];
	
	var that = this;
	
	this.computePointTraceDistance = function(){
		let sum = 0;
		
		for (let i = 0; i < arguments.length - 1; i++) {
			if (arguments[i] &&  arguments[i + 1]) {
				sum += averageDistance(arguments[i], arguments[i + 1]); 
			}
		}
		
		return sum;
	}
	
	this.computeDistances = function(input_points1, input_points2, featureName) {			
		/*
			input  => points1, points2 (arrays of points)
			output => distances (array of float)
		*/
		
		if (input_points1 && input_points2) {	
			function calculateDistances(points1, points2) {
				let output = [];
				
				for (let i in points1) {
					output.push(distancePoints(points1[i], points2[i]));
				}
				
				return output;
			}
			
			let output = calculateDistances(input_points1, input_points2)
			
			this.visualizationPoints[featureName] 					= [] ;
			this.visualizationPoints[featureName]['signal']	    	= output;
			this.visualizationPoints[featureName]['boundaryPoints'] = [0, output.length];

			return output;		
		} else {
			return 'N/A';
		}
	}
	

	this.computeStrideLength = function(input_points, featureName) {	
		/*
			1. finds duplicates in array (duplicates represent same positions of one leg in several frames)
			2. computes distances between this positions
			3. return median length
		*/	
		
		function extractDuplicates(arr) {
			let farPoint 		  = new Point(player.bvh.body.coords[3][0], player.bvh.body.coords[3][1], player.bvh.body.coords[3][2]);
			let farPointDistances = []; 
			let duplicates		  = [];
			let duplPointsAvg	  = [];
			let counter 		  = 0;
			
			for (let i in arr) {
				let pomDist = Math.round(distanceCoords2D(farPoint.x, farPoint.z, arr[i].x, arr[i].z));
				
				if (farPointDistances[pomDist] === undefined) {
					farPointDistances[pomDist] = [];
				}

				farPointDistances[pomDist].push(i);
			}
			
			for (let i in farPointDistances) {				
				if (farPointDistances[i].length > 1) {
					if (duplicates[counter] === undefined) {
						duplicates[counter] = [];
					}
					for (let j in farPointDistances[i]) {
						duplicates[counter].push(parseInt(farPointDistances[i][j])); 
					}
				} else {
					counter++;
				} 
			}
			
			for (let i in duplicates) {
				if (duplicates[i].length > 5) {		// 5 - bulharska konstanta, je potrebnych aspon 5 framov, aby sa to dalo povazovat za krok
					let sumX = 0, sumY = 0, sumZ = 0, sum = 0;
				
					for (let j of duplicates[i]) {
						sumX += input_points[j].x;
						sumY += input_points[j].y;
						sumZ += input_points[j].z;
						sum  += 1;
					}
					duplPointsAvg.push(new Point(sumX / sum, sumY / sum, sumZ / sum));
				}
			}

			that.visualizationPoints[featureName] = createVisualizationPoints(duplPointsAvg);
			
			return duplPointsAvg;
		}
		
		function computeDistances(arr) {
			let distances = [];	
			
			for (let i = 0; i < arr.length - 1; i++) {
				distances.push(distancePoints(arr[i], arr[i + 1]));
			}

			return distances;
		}	
		
		function createVisualizationPoints(arr) {
			let output = [];
			
			for (let i in arr) {
				output.push(new Point(0, 20, arr[i].z))
			}
			
			return output;
		}

		return computeMedian(computeDistances(extractDuplicates(input_points)));
	}
	
	this.computeMeanValue = function(points, coor, featureName) {	
		/*
			eq>> sum(points) / N
			
			compute mean value by given equation
			
			input  => array of points
				   => coordinate (for expample 'x')
				   => name of the feature (used for visualization)
			output => mean value (float)
		*/
		
		let output;
		
		function extractCoordValues() {
			let values = [];
			if (coor == "x") {
				for (let p of points) {
					values.push(parseFloat(p.x));
				}
			} else if(coor == "y") {
				for (let p of points){
					values.push(parseFloat(p.y));
				}
			}
			return values;
		}
		
		output = sumOfArray(extractCoordValues()) / points.length;
		that.visualizationPoints[featureName] = output;
		
		return output;
	}
	
	this.conputeMeanSquareError = function(points, coor, featureName) {
		/*
			mean square error
			https://onlinecourses.science.psu.edu/stat501/node/254
			
			input  => array of points
				   => coordinate (for example "x")
			output => mean square error
		*/
		
		let pom = linearRegresionReturnCoeficients(points, 'z', coor);
		let n 	= points.length;		
		let b_0 = pom[0];	//regress line coef b0
		let b_1 = pom[1];	//regress line coef b1
		let y	= [];		//estimated mean
		let mse;			//mean square error
		
		for (let i = 0; i < n; i++) {
			y[i] = b_0 + b_1 * points[i].z;
		}
		
		let pomSum = 0;
		
		for (let i = 0; i < n; i++) {
			pomSum += (points[i][coor] - y[i])**2;
		}
		
		mse = pomSum / (n - 2);		
		
		this.visualizationPoints[featureName] 			= [];
		this.visualizationPoints[featureName]['b_0'] 	= b_0;
		this.visualizationPoints[featureName]['b_1'] 	= b_1;
		this.visualizationPoints[featureName]['x1'] 	= points[0].z;
		this.visualizationPoints[featureName]['x2'] 	= points[points.length - 1].z;
		this.visualizationPoints[featureName]['mse'] 	= mse;
		
		return mse;
	}
	
	this.computeVariance = function(points, coor, featureName) {	
		/*
			eq>> sum((x - mean)**2) / N
			
			compute variance of number set by given equation
			
			input  => array of points
				   => coordinate (for expample 'x')
				   => name of the feature (used for visualization)
			output => variance (float)
		*/
		let mean;
		let output;
		
		function extractCoordValues() {
			let values = [];	
			
			if (coor == "x"){
				for (let p of points) {
					values.push(parseFloat(p.x));
				}
			} else if(coor == "y") {
				for (let p of points){
					values.push(parseFloat(p.y));
				}
			}

			return values;
		}
		
		function diffValuesByMean(arr) {
			let output = [];
			mean   = meanValue(arr);
			
			for (var i in arr){
				output[i] = arr[i] - mean;
			}
			
			return output;
		}
		
		function sedondPowerValues(arr) {
			let output = [];
			
			for (let i in arr){
				output[i] = arr[i] * arr[i];
			}
			
			return output;
		}
		
		function calculateDifrences(arr) {
			let output = [];
			output.push(0);
			
			for (let i = 0; i < arr.length - 1; i++) {
				output.push(output[i] + (arr[i] - arr[i + 1]));
			}
			
			return output;
		}
		
		output = sumOfArray(sedondPowerValues(diffValuesByMean(calculateDifrences(extractCoordValues())))) / (points.length - 1);
		that.visualizationPoints[featureName] = [output, mean];

		return output;
	}
	
	this.computeWalkSpeed = function(p, t, featureName) {	

		function createArrayWithTime(points, frameTime) {				
			/*
				calculates how many frames are included in one second
				then cuts points by-second
				
				input  => array of points
					   => frameTime - time between two frames 
				output => array of positions (point)
			*/	

			let framesInOneSec = Math.round(1 / frameTime);			
			let pom_array 	   = [];
			
			for (let i in points) {
				if (i % framesInOneSec == 0) {
					pom_array.push(points[i]);
				}
			}	

			that.visualizationPoints[featureName] = createVisualizationPoints(pom_array);
			
			return pom_array;
		}
			
		function computeDistanceDiff(arr) {
			/*
				input  => array of positions (float)
				output => array of distances (float)
			*/
			let distances = [];
			
			for (let i = 0; i < arr.length - 1; i++) {
				distances.push(distancePoints(arr[i + 1], arr[i]));	
			}
			
			return distances;
		}
		
		function createVisualizationPoints(arr) {
			let output = [];
			
			for (let i in arr) {
				output.push(new Point(0, 10, arr[i].z))
			}

			return output;
		}

		return computeMedian(computeDistanceDiff(createArrayWithTime(p, t)));		
	}
		
	this.computeAccelerationOfPointSet = function(pointSet, time, featureName) {	
		/*
			eq>> v = distance(positon1, position2) / (time2 - time1)
				 a = (v2 - v1) / (time2 - time1)
				 
			first we compute velocity for every consequent points
			then we compute acceleration of this points to get array of float
			this array is signal which has to be smoothed to reduce noise
			at the end, from the signal were extrated repeating parts
			
		
			input  => array of points
				   => frame time
				   => peakValue - each repeating part of singal starts with some high or low value, this peak is value used in filter
				   => pass - peakValue can represent top or bottom value. If pass == 'low' it means that peak is propably negative. 
				   => feature name
			output => array of accelerations (float)
		*/
		
		function computeVelocity(points) {
			let velocity = [];
			
			for (let i = 0; i < points.length - 2; i++) {
				velocity.push(distancePoints(points[i + 2], points[i]) / (time * (i + 2) - time * i));
			}

			return velocity;
		}
		
		function computeAcceleration(velocities) {
			let accelerations = [];
			
			for (let i = 0; i < velocities.length - 2; i++) {
				accelerations.push((velocities[i + 2] - velocities[i]) / (time * (i + 2) - time * i));
			}
			
			return accelerations;	
		}
		
		if (pointSet) {
			let smoothedSignal = cleanArray(signalTriangularSmooth5P(computeAcceleration(computeVelocity(pointSet))));
			
			this.visualizationPoints[featureName] 					= [] ;
			this.visualizationPoints[featureName]['signal']	    	= smoothedSignal;

			return smoothedSignal;
		} else {
			return 'N/A';
		}
	}
	
	this.computeStepWidth = function(leftFootPoints, rightFootPoints, featureName) {		
		that.visualizationPoints[featureName] 			  	= [];
		that.visualizationPoints[featureName]['steps'] 	  	= [];
		that.visualizationPoints[featureName]['regresLine'] = [];
		
		function findFootOnGround(arr) {
			let farPoint 		  = new Point(player.bvh.body.coords[3][0], player.bvh.body.coords[3][1], player.bvh.body.coords[3][2]);
			let farPointDistances = []; 
			let duplicates		  = [];
			let duplPointsAvg	  = [];
			let counter 		  = 0;
			
			for (let i in arr) {
				let pomDist = Math.round(distanceCoords2D(farPoint.x, farPoint.z, arr[i].x, arr[i].z));
				
				if (farPointDistances[pomDist] === undefined) {
					farPointDistances[pomDist] = [];
				}

				farPointDistances[pomDist].push(i);
			}
			
			for (let i in farPointDistances) {				
				if (farPointDistances[i].length > 1) {
					if (duplicates[counter] === undefined) {
						duplicates[counter] = [];
					}
					for (let j in farPointDistances[i]) {
						duplicates[counter].push(parseInt(farPointDistances[i][j])); 
					}
				} else {
					counter++;
				} 
			}
			
			for (let i in duplicates) {
				if (duplicates[i].length > 5) {		// 5 - bulharska konstanta, je potrebnych aspon 5 framov, aby sa to dalo povazovat za krok
					let sumX = 0, sumY = 0, sumZ = 0, sum = 0;
				
					for (let j of duplicates[i]) {
						sumX += arr[j].x;
						sumY += arr[j].y;
						sumZ += arr[j].z;
						sum  += 1;
					}
					
					let pomPoint = new Point(sumX / sum, sumY / sum, sumZ / sum);
					
					duplPointsAvg.push(pomPoint);
					
					that.visualizationPoints[featureName]['steps'].push(pomPoint);
				}
			}

			return duplPointsAvg;
		}
		
		function concatArrays(arr1, arr2) {
			/*
				input  => two arrays of points
				output => one array of sorted points
			*/
			let output = [];
			
			output = Array.prototype.concat(arr1, arr2);																			// concatenation of arrays
			output.sort(function(a, b){return a.z - b.z});																			// sort of points by Z coordinate
			
			return output;
		}
			
		function computeDistances(arr) {
			/*
				create two points by linear regresion for each 3 steps (start and end point)
				create straight from these points
				calculate distance between straight and middle point
				
				input  => array of points
				output => array of (float)distances
			*/
			
			let output 		    = [];
			let lienarRegPoints = [];
			
			for (let i = 0; i < arr.length - 2; i++) {
				lienarRegPoints = linearRegresion(arr.slice(i, i + 3), 'z', 'x', arr[i].z, arr[i + 2].z);  							// calculate start and end point by linear regresion
				
				that.visualizationPoints[featureName]['regresLine'].push(lienarRegPoints);
				
				let startPoint = new Point(lienarRegPoints[1], arr[i].y, 	 lienarRegPoints[0]);
				let endPoint   = new Point(lienarRegPoints[3], arr[i + 2].y, lienarRegPoints[2]);
				
				output.push(computeDistancePointStraight(arr[i + 1], createStraightFromTwoPoints(startPoint, endPoint)));			// calculate distance between straight and point
			}
			return output;
		}
		
		return computeMedian(computeDistances(concatArrays(findFootOnGround(leftFootPoints), findFootOnGround(rightFootPoints))));
	}
	
	this.computeShoulderRotation = function(leftShoulder, rightShoulder, axis, featureName) {
		let planeNormalVector = [];
		let rotations  = [];
		let difference = [];
		
		if (axis == 'y') {	//horizontal
			planeNormalVector = [0,1,0];
		}
		if (axis == 'z') {
			planeNormalVector = [0,0,1];
		}		
		
		for (let i = 0; i < leftShoulder.length; i++) {
			let straight 	   = createStraightFromTwoPoints(leftShoulder[i], rightShoulder[i]);
			let directVector   = vectorNormalize([straight.x.t, straight.y.t, straight.z.t]);
			
			rotations.push(radians2degrees(Math.acos(scalarProduct(directVector, planeNormalVector))));
		}
		
		for (let i = 0; i < rotations.length - 1; i++) {
			difference.push(Math.abs(rotations[i] - rotations[i + 1]));
		}
		
		let pomSignal = cleanArray(signalTriangularSmooth5P(difference));
		
		this.visualizationPoints[featureName] = [];
		this.visualizationPoints[featureName]['signal'] = pomSignal;
		
		return pomSignal;
	}
	
	this.computeBend = function(pointsTrace, featureName) {
		/*
			computes bend of extremity
			result is ration bettew aerial length and real length of extremity
			
			input  => trace of points
			output => array of float (bend ratio)
		*/

		let realLength   = 0;
		let bendRatioArr = [];
		
		//compute real lenght of extremity
		for (let i = 0; i < pointsTrace.length - 1; i++) {
			realLength += distancePoints(pointsTrace[i][0], pointsTrace[i + 1][0]); 
		}	

		//create ratio array (bend lenght / real length)
		for (let i in pointsTrace[0]) {
			bendRatioArr.push(distancePoints(pointsTrace[0][i], pointsTrace[pointsTrace.length - 1][i]) / realLength);
		}

		let smoothedSignal = cleanArray(signalTriangularSmooth5P(bendRatioArr));
		
		this.visualizationPoints[featureName] = [];
		this.visualizationPoints[featureName]['signal'] = smoothedSignal;
		
		return smoothedSignal;
	}
	
	this.computeHeadLeaning = function(headPoints, hipsPoints, featureName) {
		/*
			creates straight line from hips point to the ground (Y coordination is 0)
			head leaning is calculated as a distance from this line to head point
		
			input  => array of points (head), array of points (hips)
			output => signal (array of float)
		*/
		
		let signal    = [];

		for (let i in headPoints) {		
			signal.push(computeDistancePointStraight(headPoints[i], createStraightFromTwoPoints(hipsPoints[i], new Point(hipsPoints[i].x, 0, hipsPoints[i].z))));
		}
		
		let smoothedSignal = cleanArray(signalTriangularSmooth5P(signal));
		
		this.visualizationPoints[featureName] = [];
		this.visualizationPoints[featureName]['signal'] = smoothedSignal;
		
		return smoothedSignal;
	}
	
}