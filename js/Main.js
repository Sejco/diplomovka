function checkAll(){
	checkboxName = document.getElementsByTagName('input');
	console.log(checkboxName);
}

function cleanArray(actual) {
	/*
		reindex array, start at 0
	*/
	var newArray = new Array();
	
	for (var i = 0; i < actual.length; i++) {
		if (actual[i]) {
			newArray.push(actual[i]);
		}
	}
	
	return newArray;
}

function toFixedArray(arr, decimals) {
	for (var i = 0; i < arr.length; i++) {
		arr[i] = arr[i].toFixed(decimals);
	}
	
	return arr;
}

function changeDisplay(elemId) {
	if (document.getElementById(elemId).style.display  == 'block') {
		document.getElementById(elemId).style.display  = 'none';
	} else {
		document.getElementById(elemId).style.display  = 'block';
	}
}

function cl() {
	console.log(arguments);
}