function Bvh_parse(input_array, file_name){
	this.hierarchy 	 	= input_array[0];	
	this.motions     	= input_array[1];
	this.index 		 	= 0;
	this.token 		 	= this.hierarchy[this.index][0];
	this.num_frames  	= parseInt(this.motions[1][1]);
	this.frame_time		= parseFloat(this.motions[2][2]);
	this.body;
	this.bodyBaseFrame;
	
	this.create_body_structure = function() {	
		var that = this;
		while (this.token != "ROOT") {
			this.next_token();
		}
		
		function parse() {
			var body_part_name;
			var offset 			= [];
			var body_part_obj 	= [];
			var rotOrder		= '';
			var channels;
			
			if ((that.token == "JOINT") || (that.token == "ROOT")) {
				that.next_token();			
				body_part_name = that.token;				
				that.next_token();
				if (that.token == "{") {
					that.next_token();
					if (that.token == "OFFSET") {				
						that.next_token();
						offset.push(parseFloat(that.token));
						that.next_token();
						offset.push(parseFloat(that.token));
						that.next_token();
						offset.push(parseFloat(that.token));
						that.next_token();				
					}
					if (that.token == "CHANNELS") {
						that.next_token();
						channels = parseInt(that.token);
						for (let i = 0; i < 3; i++) {
							that.next_token();
							while((that.token != 'Xrotation') && (that.token != 'Yrotation') && (that.token != 'Zrotation')) {
								that.next_token();
							}
							if (that.token == 'Xrotation') {
								rotOrder += 'X';
							}
							if (that.token == 'Yrotation') {
								rotOrder += 'Y';
							}
							if (that.token == 'Zrotation') {
								rotOrder += 'Z';
							}
						}
					}
					while (true) {
						that.next_token();
						if (that.token == "JOINT") {
							body_part_obj.push(parse());						 
						} else if (that.token == "End") {
							body_part_obj.push(null);
							while (that.token != "}") {
								that.next_token();
							}							
						} else if (that.token == "}") {
							return new Body_part(body_part_name, offset, body_part_obj, channels, rotOrder, that.num_frames);
						}
					}	
				}
			} 
		}
		
		this.body = parse();	
	}
	
	this.next_token = function() {
		let token = this.hierarchy[this.index];
		
		this.index++;		
		this.token = token;
	}
	
	this.calculate_structure_coords = function(frame) {
		/*
			computes coordinates of all points using rotations
		*/
		var root  		 = this.body;
		var that 		 = this;
		var motion_index = 0;
		var rotX,rotY,rotZ;
		
		let	p1;	

		function calculate_child_coords(node) {
			var rx, ry, rz;
		
		
		
		
			rz = that.motions[frame][motion_index + node.rotOrder.indexOf('Z')];
			ry = that.motions[frame][motion_index + node.rotOrder.indexOf('Y')];
			rx = that.motions[frame][motion_index + node.rotOrder.indexOf('X')];
			
			that.rotateBodyPartRecursively(frame, node, rx, ry, rz);
					
			motion_index += node.channels;
							
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++) {
					calculate_child_coords(node.joints[i]);
				}
			}
		}
		
		motion_index += root.channels;
	
		for (let j = 0; j < root.joints.length; j++){
			calculate_child_coords(root.joints[j]);
		}

		rotX = this.motions[frame][3 + root.rotOrder.indexOf('X')];
		rotY = this.motions[frame][3 + root.rotOrder.indexOf('Y')];	
		rotZ = this.motions[frame][3 + root.rotOrder.indexOf('Z')];		

		this.rotateCoordsAboutCenter(frame, root, rotX, rotY, rotZ);
	}	
	
	this.rotateBodyPartRecursively = function(frame, node, rx, ry, rz) {
		/*
			rotates all points around first parent
		*/
		var that = this;
		
		function rotateChildNode(nod, parentCoords) {
			let p, rp;
		
			p  = [nod.coords[frame][0], nod.coords[frame][1], nod.coords[frame][2], 1];
			rp = [parentCoords[0], parentCoords[1], parentCoords[2], 1];
			
			p  = quatermRot3axis(rx, ry, rz, rp, p, nod.rotOrder);

			nod.coords[frame] = p;
			
			if (nod.joints[0] !== null){
				for (let i = 0; i < nod.joints.length; i++) {
					rotateChildNode(nod.joints[i], parentCoords);										//vsetky body zrotujem okolo parent nodu o rovnaky uhol
				}
			}	
		}
		
		if (node.joints[0] !== null){
			for (let i = 0; i < node.joints.length; i++) {
				rotateChildNode(node.joints[i], node.coords[frame]);
			}
		}
	}
	
	this.rotateCoordsAboutCenter = function(frame, root, rx, ry, rz) {
		/*
			rotates all points around hips point
		*/
		function rotateChildNode(node) {
			let p, rp;
		
			p  = [node.coords[frame][0], node.coords[frame][1], node.coords[frame][2], 1];
			rp = [root.coords[frame][0], root.coords[frame][1], root.coords[frame][2], 1];
	
			p  = quatermRot3axis(rx, ry, rz, rp, p, node.rotOrder);

			node.coords[frame] = p;
			
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++) {
					rotateChildNode(node.joints[i]);
				}
			}	
		}
		
		rotateChildNode(root);
	}
	
	this.applyBaseOffsets = function(frame) {
		var root  		 = this.body;
		var that 		 = this;
		
		let	p1;	

		function calculate_child_coords(node, dx, dy, dz) {	
			var p;	
			var xx, yy, zz;			
					
			xx = node.offset[0] + dx;
			yy = node.offset[1] + dy;
			zz = node.offset[2] + dz;	
						
			p  = [xx, yy, zz, 1];	

			node.coords[frame] = p;
							
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					calculate_child_coords(node.joints[i], p[0], p[1], p[2]);
				}
			}
		}	

		p1 = [this.motions[frame][0], this.motions[frame][1], this.motions[frame][2], 1];

		root.coords[frame] = p1;

		for (let j = 0; j < root.joints.length; j++){
			calculate_child_coords(root.joints[j], p1[0], p1[1], p1[2]);
		}
	}
	
	this.calculateBaseFrame = function() {
		var root  		 = this.body;
		var that 		 = this;
		var output	     = [];
		
		let	p1;	

		function calculate_child_coords(node, dx, dy, dz) {	
			var p;	
			var xx, yy, zz;			
					
			xx = node.offset[0] + dx;
			yy = node.offset[1] + dy;
			zz = node.offset[2] + dz;		
						
			p  = [xx, yy, zz, 0];

			output[node.part] = [];
			output[node.part] = new Point(p[0], p[1], p[2]);		
							
			if (node.joints[0] !== null){
				for (let i = 0; i < node.joints.length; i++){
					calculate_child_coords(node.joints[i], p[0], p[1], p[2]);
				}
			}
		}

		p1 = [this.motions[3][0], this.motions[3][1], this.motions[3][2], 1];
		
		output[root.part] = [];
		output[root.part] = new Point(p1[0], p1[1], p1[2]);

		for (let j = 0; j < root.joints.length; j++){
			calculate_child_coords(root.joints[j], p1[0], p1[1], p1[2]);
		}
		
		return output;
	}
	
	this.motions_parse_float = function() {
		for (let i = 3; i < this.motions.length; i++) {		// index of metadata is 0-2
			for (let j = 0; j < this.motions[i].length; j++) {
				this.motions[i][j] = parseFloat(this.motions[i][j]);
			}
		}
	}
	
	this.translateSequence = function() {
		for (let i = 3; i < this.motions.length; i++) {
			// this.motions[i][0] -= this.body.offset[0]; 	
			this.motions[i][1] -= this.body.offset[1];	
			// this.motions[i][2] -= this.body.offset[2];	
		}	
	}
	
	this.setDirectionOfWalk = function() {
		if (!file_name.includes('Backwards')) {
			if (this.motions[3][2] > this.motions[this.motions.length - 1][2]) {
				for (let i = 3; i < this.motions.length; i++) {
					this.motions[i][2] *= -1;
				}
			} 
		}
	}
	
	this.prepare_motions = function() {
		this.motions_parse_float();
		this.translateSequence();
		// this.setDirectionOfWalk();
	}
	
	this.calculate_coords = function() {			
		for (let i = 3; i < this.motions.length; i++) {			
			this.applyBaseOffsets(i);
			this.calculate_structure_coords(i);
		}
	}
}

function Body_part(part, offset, joints, channels, rotOrder, num_frames) {
	var realPartName = '-';
	
	switch (part) {
		case 'hip':
		case 'hips':
		case 'Hips':
			realPartName = 'Hips';
			break;
		case 'abdomen':
		case 'ToSpine':
		case 'toSpine':
			realPartName = 'ToSpine';
			break;
		case 'Spine':
			realPartName = 'Spine';
			break;
		case 'chest':
		case 'Spine1':
			realPartName = 'Spine1';
			break;
		case 'neck':
		case 'Neck':
			realPartName = 'Neck';
			break;
		case 'head':
		case 'Head':
			realPartName = 'Head';
			break;
		case 'lCollar':
		case 'LeftShoulder':
			realPartName = 'LeftShoulder';
			break;
		case 'lShldr':
		case 'LeftArm':
			realPartName = 'LeftArm';
			break;
		case 'lForeArm':
		case 'LeftForeArm':
			realPartName = 'LeftForeArm';
			break;
		case 'lHand':
		case 'LeftHand':
			realPartName = 'LeftHand';
			break;
		case 'rCollar':
		case 'RightShoulder':
			realPartName = 'RightShoulder';
			break;
		case 'rShldr':
		case 'RightArm':
			realPartName = 'RightArm';
			break;
		case 'rForeArm':
		case 'RightForeArm':
			realPartName = 'RightForeArm';
			break;
		case 'rHand':
		case 'RightHand':
			realPartName = 'RightHand';
			break;
		case 'lThigh':
		case 'LeftUpLeg':
			realPartName = 'LeftUpLeg';
			break;
		case 'lShin':
		case 'LeftLeg':
			realPartName = 'LeftLeg';
			break;
		case 'lFoot':
		case 'LeftFoot':
			realPartName = 'LeftFoot';
			break;
		case 'LeftToeBase':
			realPartName = 'LeftToeBase';
			break;
		case 'rThigh':
		case 'RightUpLeg':
			realPartName = 'RightUpLeg';
			break;
		case 'rShin':
		case 'RightLeg':
			realPartName = 'RightLeg';
			break;
		case 'rFoot':
		case 'RightFoot':
			realPartName = 'RightFoot';
			break;
		case 'RightToeBase':
			realPartName = 'RightToeBase';
			break;
	}
	
	this.part 		= realPartName;
	this.offset 	= offset;
	this.joints 	= joints;	
	this.channels 	= channels;
	this.rotOrder	= rotOrder;
	this.coords		= new Array(num_frames);
}


