<?php
	session_start();
	
	define('ROOT', dirname(__FILE__));
	
	include('php/Db/Connection.php');
	include('php/Db/Functions.php');
	
	$DB = new spojenie();
	
	$podstranka = 'analysis';
	if (isset($_GET['podstranka'])){
		$podstranka = $_GET['podstranka'];
	}
	
?>
<html lang="sk">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<base href="http://<?= $_SERVER['HTTP_HOST'] . preg_replace("/(\/\w+\.\w+)$/", "/", $_SERVER['PHP_SELF']); ?>">
			
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		
		<link rel="stylesheet" href="lib/css/bootstrap.min.css">
		<link rel="stylesheet" href="lib/css/bootstrap-theme.min.css">
		<link href="css/style.css"    rel="stylesheet" />

	    <script src="lib/js/jquery-1.11.2.min.js"></script>
	    <script src="lib/js/jquery.textareaAutoResize.js"></script>
		<script src="lib/js/bootstrap.min.js"></script>
		<script src='lib/js/zingchart.min.js'></script>
		<script src="lib/js/bootstrap-filestyle.min.js"></script>
		<script src="lib/js/sortable.js"></script>
			
		<script src="js/Main.js"></script>
		<script src="js/Config.js"></script>
		<script src="js/geometry.js"></script>
		<script src="js/drawing.js"></script>
		<script src="js/Gait_descriptor.js"></script>
		<script src="js/Features.js"></script>
		<script src="js/Bvh_parser.js"></script>
		<script src="js/Walk_filter.js"></script>
		<script src="js/Bvh_player.js"></script>
		
		<title>Diplomovka</title>
	</head>

	<body>
		<header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
			<nav class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 nopadding">
				<ul>
					<li><a href="analysis/" class="<?=($podstranka == 'analysis' ? 'header_visited' : '' );?>">Analýza</a></li>
					<li><a href="play/"		class="<?=($podstranka == 'play' 	 ? 'header_visited' : '' );?>">Prehrať záznam</a></li>
					<li><a href="compare/"  class="<?=($podstranka == 'compare'  ? 'header_visited' : '' );?>">Porovnať osoby</a></li>
					<li><a href="overview/" class="<?=($podstranka == 'overview' ? 'header_visited' : '' );?>">Prehľad osôb</a></li>
					<li><a href="add/"		class="<?=($podstranka == 'add' 	 ? 'header_visited' : '' );?>">Pridať osobu</a></li>				
					<li><a href="search/"	class="<?=($podstranka == 'search' 	 ? 'header_visited' : '' );?>">Vyhľadávanie</a></li>				
				</ul>
			</nav>
		</header>
		
		<div class="main col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding" style="margin-top:60px;">
			<?php
				switch ($podstranka) {
					case 'analysis':
						include('php/Views/gait_analysis.php');
						break;
					case 'overview':
						include('php/Views/overview.php');
						break;
					case 'compare':
						include('php/Views/compare.php');
						break;
					case 'add':
						include('php/Views/add_person.php');
						break;
					case 'play':
						include('php/Views/player.php');						
						break;
					case 'search':
						include('php/Views/search.php');						
						break;
					default:
						include('php/Views/gait_analysis.php');
				}
			?>
		</div>
			
		<footer>
			Michal Sejč - Diplomová práca
		</footer>
	</body>
</html>