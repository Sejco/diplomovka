<?php
	class Person_compare {
		public $submited_option = false;
		public $person1;
		public $person2;
		public $signals1;
		public $signals2;
		
		function __construct() {
			if (isset($_POST['submited_comparison'])) {
				$this->submited_option = true;
				$this->person1  = loadFeatures($_POST['person1']);
				$this->person2  = loadFeatures($_POST['person2']);
				$this->signals1 = loadSignals($_POST['person1']);
				$this->signals2 = loadSignals($_POST['person2']);
			}
		}
		
		public function computePercetageDifference($val1, $val2) {
			if ($val1 != 0) {
				return number_format(abs(round(100 - ((100 / $val1) * $val2), 2)), 2, '.', '');
			} else {
				return 'N/A';
			}
		}
	}
?>	