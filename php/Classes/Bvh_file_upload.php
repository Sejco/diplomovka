<?php
	class Bvh_file_upload { 
		public $file_content = false;
		public $file_name    = false;
		public $error;
		
		function __construct() {
			if (isset($_POST['submit_step_1'])){
				if ($this->upload_file($_POST, $_FILES)){
					$file 					= "mocap/".$_FILES["user_gait_key_frames"]["name"];
					$this->file_content 	= Bvh_file_loader::get_file_content($file);
					$this->file_name		= $_FILES["user_gait_key_frames"]["name"];
				}
			}	
			
			if (isset($_POST['submit_step_2'])) {				
				$this->uploadData($_POST);
				
				$this->file_content = false;
				
				$_SESSION['ppl_upload_done'] = true;
				
				header("Refresh:0; url=/add/");
			}
		}
		
		function upload_file($post, $file){
			$target_dir 	= "mocap/";
			$target_file 	= $target_dir . basename($file["user_gait_key_frames"]["name"]);
			$file_type 		= pathinfo($target_file, PATHINFO_EXTENSION);
			$file_size		= $file['user_gait_key_frames']['size'];
		
			if ($file_type == 'bvh') {
				if ($file_size < 1000000) {
					if (move_uploaded_file($file["user_gait_key_frames"]["tmp_name"], $target_file)) {
						return true;
					} else {
						$this->error = "Pri nahrávaní došlo k chybe.";
					}
				} else {
					$this->error = "Súbor je príliš veľký. Musí byť menší ako 1MB.";
				}
			} else {
				$this->error = "Neplatný formát. Súbor musí mať príponu .bvh";
			}
	
			return false;	
		}
		
		function uploadData($data) {
			$userId = 	insertUserReturnId($data['name'], $data['surname']);
			
			insertBvhFile($userId, $data['gait_bvh_file']);
			insertFeatures($userId, 
				$data['height'], 
				$data['L_arm'], 
				$data['R_arm'], 
				$data['L_leg'], 
				$data['R_leg'], 
				$data['walk_speed'], 
				$data['L_stride'], 
				$data['R_stride'], 
				$data['step_width'],
				$data['head_variance_x'], 
				$data['head_variance_y'], 
				$data['L_knee_variance_y'],
				$data['R_knee_variance_y'], 
				$data['L_shoulder_variance_x'], 
				$data['R_shoulder_variance_x']
			);	
	
			insertSignal($userId, 'head_leaning', 			$_POST['head_leaning']);
			insertSignal($userId, 'body_leaning', 			$_POST['body_leaning']);
			insertSignal($userId, 'L_arm_bend', 			$_POST['L_arm_bend']);
			insertSignal($userId, 'R_arm_bend', 			$_POST['R_arm_bend']);
			insertSignal($userId, 'L_leg_bend', 			$_POST['L_leg_bend']);
			insertSignal($userId, 'R_leg_bend', 			$_POST['R_leg_bend']);
			insertSignal($userId, 'hands_distances', 		$_POST['hands_distances']);
			insertSignal($userId, 'elbow_distances', 		$_POST['elbow_distances']);
			insertSignal($userId, 'knee_distances', 		$_POST['knee_distances']);
			insertSignal($userId, 'foot_distances', 		$_POST['foot_distances']);
			insertSignal($userId, 'toe_distances', 			$_POST['toe_distances']);
			insertSignal($userId, 'shouldersHorizontalRot', $_POST['shouldersHorizontalRot']);
			insertSignal($userId, 'shouldersVericalRot', 	$_POST['shouldersVericalRot']);
			insertSignal($userId, 'head_acceleration', 		$_POST['head_acceleration']);
			insertSignal($userId, 'hips_acceleration', 		$_POST['hips_acceleration']);
			insertSignal($userId, 'left_fore_arm_acc', 		$_POST['left_fore_arm_acc']);
			insertSignal($userId, 'right_fore_arm_acc', 	$_POST['right_fore_arm_acc']);
			insertSignal($userId, 'left_hand_acc',   		$_POST['left_hand_acc']);
			insertSignal($userId, 'right_hand_acc', 		$_POST['right_hand_acc']);	
			insertSignal($userId, 'left_leg_acc',   		$_POST['left_leg_acc']);
			insertSignal($userId, 'right_leg_acc',  		$_POST['right_leg_acc']);
			insertSignal($userId, 'left_foot_acc',  		$_POST['left_foot_acc']);
			insertSignal($userId, 'right_foot_acc', 		$_POST['right_foot_acc']);	
			insertSignal($userId, 'left_toe_acc',   		$_POST['left_toe_acc']);
			insertSignal($userId, 'right_toe_acc',  		$_POST['right_toe_acc']);			
		}
	} 
?>