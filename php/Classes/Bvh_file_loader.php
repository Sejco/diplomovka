<?php
	class Bvh_file_loader { 
		public  $file_content;
		public  $file_list = Array();
		public  $file_name;
		
		private $path 	   = "mocap/";
		private $extension = ".bvh";		
		
		function __construct() {
			if (isset($_GET['file'])) {
				$this->file_name = $_GET['file'];
			} else {
				$this->file_name = FALSE;
			}
			
			if (file_exists($this->path.''.$this->file_name.''.$this->extension)) {
				$this->file_content = $this->get_file_content($this->path.''.$this->file_name.''.$this->extension);
			} else {
				$this->file_content = FALSE;
			}
			
			$this->file_list['file_name'] = $this->getAllFiles();
			$this->file_list['file_size'] = $this->addFileSize();
		}
		
		function getAllFiles() {
			return scandir($this->path);
		}
		
		function addFileSize() {
			$output = [];
			
			for ($i = 2; $i < count($this->file_list['file_name']); $i++) {
				$output[$i] = filesize($this->path.''.$this->file_list['file_name'][$i]);
			}
			
			return $output;
		}
		
		public static function get_file_content($f){
			$motion_section = false;
			$hierarchy 		= [];
			$motion 		= [];
			$pom_stream 	= @fopen($f, 'r');
			if ($pom_stream) {
				while (($buffer = fgets($pom_stream)) !== false) {
					$buffer_arr = explode(' ', trim(preg_replace('!\s+!', ' ', $buffer)));						
					if ($motion_section) {
						$motion[] = $buffer_arr;
					} else {
						if ($buffer_arr[0] == "MOTION") {
							$motion_section = true;
							$motion[] = $buffer_arr;
						} else {
							$hierarchy = array_merge($hierarchy, $buffer_arr);
						}
					}									
				}
				if (!feof($pom_stream)) {
					echo "Error: unexpected fgets() fail\n";
				}
				fclose($pom_stream);
			}
			return [$hierarchy, $motion];
		}
	}
?>	