<?php
	class Person_search { 
		public $file_content = false;
		public $file_name    = false;
		public $found		 = null;
		public $personName;
		public $match;
		
		function __construct() {
			if (isset($_POST['submit_step_1'])){			
				$file 					= $_FILES["input_bvh"]["tmp_name"];
				$this->file_content 	= Bvh_file_loader::get_file_content($file);			
				$this->file_name		= $_FILES["input_bvh"]["name"];
			}
			
			if (isset($_POST['submit_search'])){			
				$result = $this->selectVectors();
				
				if ($result) {
					$this->addSignals($result);
					$this->computeFeaturesMatch($result);
					$this->computeSignalMatch($result);
					$this->countWeightedTotalMatch($result);
					// echo "<pre>";
					// print_r($result);
					// echo "</pre>";
					$this->selectBestMatch($result);

					$this->personName = $this->getPeopleById($result['user_id']);
					$this->match 	  = round((1 - $result['totalMatch']) * 100, 3);
					$this->found 	  = true;
				}
			}
		}
		
		function selectBestMatch(&$arr) {
			usort($arr, array("Person_search", "definedSort"));
			$arr = $arr[0];
		}
		
		function countWeightedTotalMatch(&$arr) {
			for ($i = 0; $i < count($arr); $i++) {  
				$totalWeightSum = $_POST['height_weight'] +	      
								  $_POST['L_arm_weight'] +			  
								  $_POST['R_arm_weight'] +			  
								  $_POST['L_leg_weight'] +			  
								  $_POST['R_leg_weight'] +	  
								  $_POST['head_variance_y_weight'] +  
								  $_POST['L_knee_variance_y_weight'] +
								  $_POST['R_knee_variance_y_weight'] +
								  $_POST['walk_speed_weight'] +
								  $_POST['step_width_weight'] +		
								  $_POST['L_stride_length_weight'] +		
								  $_POST['R_stride_length_weight'] +		
								  $_POST['head_variance_x_weight'] +
								  $_POST['L_shoulder_variance_x_weight'] +
								  $_POST['R_shoulder_variance_x_weight'];
								  
				$arr[$i]['totalMatch'] = $arr[$i]['height'] +			
				                         $arr[$i]['L_arm'] +		
					                     $arr[$i]['R_arm'] +		
					                     $arr[$i]['L_leg'] +		
					                     $arr[$i]['R_leg'] +
					                     $arr[$i]['head_variance_y'] +
					                     $arr[$i]['L_knee_variance_y'] +
					                     $arr[$i]['R_knee_variance_y'] +
										 $arr[$i]['walk_speed'] +
										 $arr[$i]['step_width'] +		
										 $arr[$i]['L_stride_length'] +		
										 $arr[$i]['R_stride_length'] +		
										 $arr[$i]['head_variance_x'] +
										 $arr[$i]['L_shoulder_variance_x'] +	
										 $arr[$i]['R_shoulder_variance_x'];	 
										 
				for ($j = 0; $j < count($arr[$i]['signal']); $j++) {
					if ($arr[$i]['signal'][$j]['signals'] != 'N/A') {
						$totalWeightSum 	   += $_POST[$arr[$i]['signal'][$j]['body_part'].'_weight'];
						$arr[$i]['totalMatch'] += $arr[$i]['signal'][$j]['match'] * $_POST[$arr[$i]['signal'][$j]['body_part'].'_weight'];
					}
				}
				
				$arr[$i]['wsum'] = $totalWeightSum;
				$arr[$i]['totalMatch'] /= $totalWeightSum;
			}
		}
		
		function computeSignalMatch(&$arr) {
			for ($i = 0; $i < count($arr); $i++) {
				for ($j = 0; $j < count($arr[$i]['signal']); $j++) {
					$arr[$i]['signal'][$j]['match'] = 1 - Person_search::xcorr(json_decode($_POST[$arr[$i]['signal'][$j]['body_part']]), json_decode($arr[$i]['signal'][$j]['signals']));
					$arr[$i]['signal'][$j]['match_w'] = (1 - Person_search::xcorr(json_decode($_POST[$arr[$i]['signal'][$j]['body_part']]), json_decode($arr[$i]['signal'][$j]['signals']))) * $_POST[$arr[$i]['signal'][$j]['body_part'].'_weight'];
				}
			}
		}
		
		function computeFeaturesMatch(&$arr) {
			for ($i = 0; $i < count($arr); $i++) {
				$arr[$i]['height'] 					= Person_search::computePercetageDifference($arr[$i]['height'] 					, $_POST['height'] 					) * $_POST['height_weight'];
				$arr[$i]['L_arm'] 					= Person_search::computePercetageDifference($arr[$i]['L_arm'] 					, $_POST['L_arm'] 					) * $_POST['L_arm_weight'];
				$arr[$i]['R_arm'] 					= Person_search::computePercetageDifference($arr[$i]['R_arm'] 					, $_POST['R_arm'] 					) * $_POST['R_arm_weight'];
				$arr[$i]['L_leg'] 					= Person_search::computePercetageDifference($arr[$i]['L_leg'] 					, $_POST['L_leg'] 					) * $_POST['L_leg_weight'];
				$arr[$i]['R_leg'] 					= Person_search::computePercetageDifference($arr[$i]['R_leg'] 					, $_POST['R_leg'] 					) * $_POST['R_leg_weight'];
				$arr[$i]['head_variance_y'] 		= Person_search::computePercetageDifference($arr[$i]['head_variance_y'] 		, $_POST['head_variance_y'] 		) * $_POST['head_variance_y_weight'];
				$arr[$i]['L_knee_variance_y'] 		= Person_search::computePercetageDifference($arr[$i]['L_knee_variance_y'] 		, $_POST['L_knee_variance_y'] 		) * $_POST['L_knee_variance_y_weight'];
				$arr[$i]['R_knee_variance_y'] 		= Person_search::computePercetageDifference($arr[$i]['R_knee_variance_y'] 		, $_POST['R_knee_variance_y'] 		) * $_POST['R_knee_variance_y_weight'];
				$arr[$i]['walk_speed'] 				= Person_search::computePercetageDifference($arr[$i]['walk_speed'] 				, $_POST['walk_speed'] 				) * $_POST['walk_speed_weight'];
				$arr[$i]['step_width'] 				= Person_search::computePercetageDifference($arr[$i]['step_width'] 				, $_POST['step_width'] 				) * $_POST['step_width_weight'];
				$arr[$i]['L_stride_length'] 		= Person_search::computePercetageDifference($arr[$i]['L_stride_length'] 		, $_POST['L_stride_length'] 		) * $_POST['L_stride_length_weight'];
				$arr[$i]['R_stride_length'] 		= Person_search::computePercetageDifference($arr[$i]['R_stride_length'] 		, $_POST['R_stride_length'] 		) * $_POST['R_stride_length_weight'];
				$arr[$i]['head_variance_x'] 		= Person_search::computePercetageDifference($arr[$i]['head_variance_x'] 		, $_POST['head_variance_x'] 		) * $_POST['head_variance_x_weight'];
				$arr[$i]['L_shoulder_variance_x'] 	= Person_search::computePercetageDifference($arr[$i]['L_shoulder_variance_x'] 	, $_POST['L_shoulder_variance_x'] 	) * $_POST['L_shoulder_variance_x_weight'];
				$arr[$i]['R_shoulder_variance_x'] 	= Person_search::computePercetageDifference($arr[$i]['R_shoulder_variance_x'] 	, $_POST['R_shoulder_variance_x'] 	) * $_POST['R_shoulder_variance_x_weight'];
			}
		}
		
		function addSignals(&$usersId) {
			/*
				joins signals to users
			*/
			foreach ($usersId as $id => $key) {
				$userId = $usersId[$id]['user_id'];
				$sql = "
					SELECT signals_main.body_part, signals_main.signals
					FROM signals_main
					WHERE signals_main.user_id = $userId;
				";
				
				$usersId[$id]['signal'] = $GLOBALS['DB']->fetch_multi($sql);
			}		
		}
		
		function selectVectors() {			
			$sql = "
				SELECT user_id, height, L_arm, R_arm, L_leg, R_leg, head_variance_y, L_knee_variance_y, R_knee_variance_y, walk_speed, step_width, L_stride AS L_stride_length, R_stride AS R_stride_length, head_variance_x, L_shoulder_variance_x, R_shoulder_variance_x  
				FROM features_main	
			";
			
			return $GLOBALS['DB']->fetch_multi($sql);
		}
		
		function getPeopleById($id) {
			$sql = "
				SELECT users_main.*
				FROM users_main
				WHERE id = $id
			";
			
			return $GLOBALS['DB']->fetch_single($sql);
		}
		
		static function definedSort($a, $b){
			if ($a['totalMatch'] == $b['totalMatch']) {
				return 0;
			}
			return ($a['totalMatch'] < $b['totalMatch']) ? -1 : 1;
		}
		
		public static function vectorLength($vec1, $vec2, $vec3, $vec4, $vec5) {
			return sqrt($vec1 * $vec1 + $vec2 * $vec2 + $vec3 * $vec3 + $vec4 * $vec4 + $vec5 * $vec5);
		}
		
		public static function xcorr($x, $y) {
			/*
				cross correlation - measures the similarity between signals			
			*/
			if (($x != 'N/A') && ($y != 'N/A')) {
				$output 	= [];
				$N 		 	= (count($x) > count($y) ? count($y) : count($x));
				$maxdelay 	= $N;
				$mx 	 	= Person_search::meanValue($x);
				$my 	 	= Person_search::meanValue($y);
				$sum1 	 	= 0;	// sum((x(i) - mx) * (y(i) - my))
				$sum2 	 	= 0;	// sum((x(i) - mx)**2)
				$sum3 	 	= 0;	// sum((y(i) - my)**2)
				$denom    	= 0;    // sqrt(sum2 * sum3)
				
				for ($i = 0; $i < $N; $i++) {
					$sum1 += ($x[$i] - $mx) * ($y[$i] - $my);
					$sum2 += pow($x[$i] - $mx, 2);
					$sum3 += pow($y[$i] - $my, 2);
				}
				$denom = sqrt($sum2 * $sum3);

				for ($delay = -$maxdelay; $delay < $maxdelay; $delay++) {
					$sum1 = 0;
					for ($i = 0; $i < $N; $i++) {
						if ((($i + $delay) < 0) || (($i + $delay) >= $N)) {
							
						} else {
							$sum1 += ($x[$i] - $mx) * ($y[$i + $delay] - $my);
						}
					}
					$output[] = ($sum1 / $denom);
				}
				
				return max($output);
			} else {
				return 1;
			}
		}
		
		public function computePercetageDifference($val1, $val2) {
			if ($val1 != 0) {
				return abs((((100 / $val1) * $val2) / 100) - 1);
			} else {
				return 'N/A';
			}
		}
		
		public static function meanValue($arr) {
			return array_sum($arr) / count($arr);
		}
	} 
?>