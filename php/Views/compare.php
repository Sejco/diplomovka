<?php
	include('php/Classes/Person_compare.php');
	
	$users = get_all_users();
	
	$person_comparison = new Person_compare();
?>
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
	<div>
		<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
			<h2 class="nomargin" style="padding-top:5px; padding-left:10px;">Porovnnaie osôb</h2>
		</div>
	</div>
	<div class="standard col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<form method="post" class="margin-top-30">
			<div class="form-group col-md-6 nopadding-left">
				<label for="person1"><strong>Osoba 1</strong></label>
				<select class="form-control" id="person1" name="person1">
				<?php
					foreach ((array)$users as $user){
						?>
							<option value="<?=$user['id']?>" <?=(($user['id'] == @$_POST['person1']) ? 'selected' : '')?>><?=$user['firstname'].' '.$user['lastname']?></option>
						<?php
					}
				?>
				</select>
			</div>
			<div class="form-group col-md-6 nopadding-right">
				<label for="person2"><strong>Osoba 2</strong></label>
				<select class="form-control" id="person2" name="person2">
				<?php
					foreach ((array)$users as $user){
						?>
							<option value="<?=$user['id']?>" <?=(($user['id'] == @$_POST['person2']) ? 'selected' : '')?>><?=$user['firstname'].' '.$user['lastname']?></option>
						<?php
					}
				?>
				</select>
			</div>
			<div class="form-group col-md-6 nopadding">	
				<button type="submit" name="submited_comparison" class="btn btn-primary">Porovnať</button>
			</div>
		</form>
	</div>
</div>

<?php
	if ($person_comparison->submited_option === true) {
		?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2" style="margin-top:5px;">
				<table class="table table-style-1 table-hover table-bordered table-condensed sortable inner_border" style="margin-bottom:15px;">
					<thead>
						<tr>
							<th class="col-md-4">Attribút</th>
							<th class="col-md-3">Osoba 1</th>
							<th class="col-md-3">Osoba 2</th>
							<th class="col-md-2">Rozdiel (%)</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($person_comparison->person1 as $key => $value) {
								?>
									<tr>
										<td><?=str_replace('_', ' ', ucfirst($key));?></td>
										<td><?=$person_comparison->person1[$key]?></td>
										<td><?=$person_comparison->person2[$key]?></td>
										<td><?=$person_comparison->computePercetageDifference($person_comparison->person1[$key], $person_comparison->person2[$key])?></td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<table class="table table-style-1 table-hover table-bordered table-condensed sortable inner_border">
					<thead>
						<tr>
							<th class="col-md-4">Attribút</th>
							<th class="col-md-2">Zhoda - Cross correlation (%)</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($person_comparison->signals1 as $key => $value) {
								?>
									<tr>
										<td><?=str_replace('_', ' ', ucfirst($person_comparison->signals1[$key]['body_part']));?></td>
										<script>
											var s1 = <?=$person_comparison->signals1[$key]['signals'];?>;
											var s2 = <?=$person_comparison->signals2[$key]['signals'];?>;

											document.write('<td>' + ((xcorr(s1, s2)) * 100).toFixed(2) + '</td>');
										</script>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
			</div>
		<?php
	}
?>
