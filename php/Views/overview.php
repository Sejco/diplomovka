<?php
	include('php/Classes/Person_search.php');
	
	$users 		= get_all_users();
	$attibutes 	= loadFeaturesOfAllUsers();
	addSignalsTest($users);
	
	$vyskumZhodnostMatic['left_leg_acc'] 			= '0.0586, 0.0567, 0.0204, 0.0074, 0.0000,-0.0000 ,-0.0005, -0.0025, -0.0094, -0.0137';
	$vyskumZhodnostMatic['R_leg_bend'] 				= '0.0600, 0.0305, 0.0249, 0.0020, 0.0001, 0.0000 ,-0.0000, -0.0041, -0.0083, -0.0167';
	$vyskumZhodnostMatic['toe_distances'] 			= '0.0617, 0.0237, 0.0181, 0.0037, 0.0009, 0.0003 ,-0.0000, -0.0000, -0.0010, -0.0101';
	$vyskumZhodnostMatic['left_toe_acc'] 			= '0.0651, 0.0303, 0.0148, 0.0018, 0.0011, 0.0000 ,-0.0000, -0.0005, -0.0080, -0.0148';
	$vyskumZhodnostMatic['foot_distances'] 			= '0.0658, 0.0216, 0.0149, 0.0043, 0.0014, 0.0000 , 0.0000, -0.0005, -0.0039, -0.0094';
	$vyskumZhodnostMatic['right_foot_acc'] 			= '0.0706, 0.0416, 0.0099, 0.0044, 0.0003, 0.0000 , 0.0000, -0.0004, -0.0084, -0.0222';
	$vyskumZhodnostMatic['right_toe_acc'] 			= '0.0733, 0.0536, 0.0152, 0.0061, 0.0000, 0.0000 ,-0.0003, -0.0007, -0.0107, -0.0276';
	$vyskumZhodnostMatic['L_leg_bend'] 				= '0.0805, 0.0193, 0.0093, 0.0014, 0.0000, 0.0000 ,-0.0000, -0.0022, -0.0106, -0.0125';
	$vyskumZhodnostMatic['knee_distances'] 			= '0.0831, 0.0257, 0.0148, 0.0039, 0.0003, 0.0000 ,-0.0000, -0.0009, -0.0038, -0.0166';
	$vyskumZhodnostMatic['left_foot_acc'] 			= '0.0836, 0.0332, 0.0230, 0.0028, 0.0001, 0.0000 ,-0.0000, -0.0047, -0.0095, -0.0215';
	$vyskumZhodnostMatic['hips_acceleration'] 		= '0.1077, 0.0483, 0.0284, 0.0110, 0.0013, 0.0000 , 0.0000, -0.0013, -0.0037, -0.0233';	
	$vyskumZhodnostMatic['head_acceleration'] 		= '0.1114, 0.0819, 0.0617, 0.0293, 0.0164, 0.0105 ,-0.0000, -0.0000, -0.0108, -0.0127';
	$vyskumZhodnostMatic['right_fore_arm_acc'] 		= '0.1147, 0.0862, 0.0597, 0.0170, 0.0127, 0.0000 , 0.0000, -0.0015, -0.0056, -0.0171';
	$vyskumZhodnostMatic['right_leg_acc'] 			= '0.1156, 0.0584, 0.0266, 0.0130, 0.0032, 0.0012 , 0.0000, -0.0000, -0.0026, -0.0269';
	$vyskumZhodnostMatic['left_fore_arm_acc']		= '0.1385, 0.0780, 0.0362, 0.0224, 0.0146, 0.0040 ,-0.0000, -0.0000, -0.0068, -0.0185';
	$vyskumZhodnostMatic['right_hand_acc'] 			= '0.1399, 0.1194, 0.0926, 0.0373, 0.0173, 0.0056 , 0.0000, -0.0000, -0.0094, -0.0192';
	$vyskumZhodnostMatic['left_hand_acc'] 			= '0.1426, 0.1145, 0.0434, 0.0359, 0.0198, 0.0064 , 0.0000, -0.0000, -0.0020, -0.0176';	
	$vyskumZhodnostMatic['elbow_distances'] 		= '0.1809, 0.1000, 0.0611, 0.0311, 0.0046, 0.0021 , 0.0000,  0.0000, -0.0087, -0.0260';	
	$vyskumZhodnostMatic['body_leaning'] 			= '0.2010, 0.1376, 0.0770, 0.0260, 0.0160, 0.0009 , 0.0000, -0.0014, -0.0199, -0.0656';	
	$vyskumZhodnostMatic['hands_distances'] 		= '0.2036, 0.1651, 0.1172, 0.0628, 0.0251, 0.0070 ,-0.0000, -0.0000, -0.0183, -0.0326';
	$vyskumZhodnostMatic['R_arm_bend'] 				= '0.3648, 0.2733, 0.1725, 0.1537, 0.0573, 0.0423 , 0.0115, -0.0000, -0.0000, -0.0515';
	$vyskumZhodnostMatic['shouldersHorizontalRot'] 	= '0.3923, 0.3671, 0.2839, 0.1911, 0.1502, 0.1192 , 0.0736,  0.0063,  0.0000, -0.0000';
	$vyskumZhodnostMatic['L_arm_bend'] 				= '0.3935, 0.2405, 0.1735, 0.0432, 0.0035, 0.0000 ,-0.0000, -0.0092, -0.0198, -0.0589';
	$vyskumZhodnostMatic['shouldersVericalRot'] 	= '0.4794, 0.0961, 0.0700, 0.0465, 0.0448, 0.0005 ,-0.0000, -0.0000, -0.0040, -0.0470';
	$vyskumZhodnostMatic['head_leaning'] 			= '0.5272, 0.3449, 0.1672, 0.1120, 0.0785, 0.0424 ,-0.0000, -0.0008, -0.0216, -0.0779';
		
?>

<script>
	var allFeturesOfUsers = <?php echo json_encode($attibutes);?>;
	var covMatrix 		  = covarianceMatrix(allFeturesOfUsers);
</script>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
	<div>
		<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
			<h2 class="nomargin" style="padding-top:5px; padding-left:10px;">Prehľad osôb</h2>
		</div>
	</div>
	<table class="table table-hover table-bordered borderless inner_border">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Meno
				</th>
				<th>
					Priezvisko
				</th>
				<th>
					Možnosti
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ((array)$users as $user){
					$bvh_file_name = substr($user['bvh_file'], 0, -4);
					?>
						<tr>
							<td><?=$user['id']?></td>
							<td><?=$user['firstname']?></td>
							<td><?=$user['lastname']?></td>
							<td>
								<a href="play/<?=$bvh_file_name?>" title="Play"  class="btn btn-lg btn-success myButton">
									<span class="glyphicon glyphicon-play"></span>
								</a>
								<a href="analysis/<?=$bvh_file_name?>" title="Show analysis"  class="btn btn-lg btn-success myButton">
									<span class="glyphicon glyphicon-stats"></span>
								</a>
							</td>
						</tr>
					<?php
				}
			?>
		</tbody>
	</table>
	
	<?php
		if (false) {
	?>
		<h1>Statické atribúty</h1>
		<table id="covMaticaTable1" class="table table-hover table-striped table-bordered inner_border">
		</table>
		<h1>Dynamické atribúty</h1>
		<table id="covMaticaTable2" class="table table-hover table-striped table-bordered inner_border">
		</table>
		<a onclick="changeDisplay('textarea_dyn');">Kód pre MATLAB</a>
		<textarea id="<?='textarea_dyn';?>" class="form-control textarea-no-resize" readonly><?=$content?></textarea>
		
		
		<h1>Signály</h1>
		<?php
			$pocetSignalov = count($users[0]['signal']);
			
			for ($i = 0; $i < $pocetSignalov; $i++) {
				$content = '';
				?>
					<h2><?=str_replace('_', ' ', ucfirst($users[0]['signal'][$i]['body_part']));?></h2><p><?=$vyskumZhodnostMatic[$users[0]['signal'][$i]['body_part']]?></p>
					<table id="mat_<?=$i?>" class="table table-hover table-striped table-bordered inner_border">
						<?php
							$content .= "D = ";
							$content .= "[";	
							
							for ($j = 0; $j < 8; $j++) {		
								?>
									<tr>
										<?php			
											for ($k = 0; $k < 8; $k++) {
												$pomValue = (1 - Person_search::xcorr(json_decode($users[$j]['signal'][$i]['signals']), json_decode($users[$k]['signal'][$i]['signals'])));
												$content .= round($pomValue, 5);
												?>
													<td style="<?=($j != $k ? 'background:rgb(255,'.(255 - round($pomValue * 255)).','.(255 - round($pomValue * 255)) : 'background:yellow');?>">
														<?=round($pomValue, 3);?>
													</td>
												<?php
												if ($k < count($users) - 1) {
													$content .= ' ';
												}
											}
										?>
									</tr>
								<?php
								if ($j < count($users) - 1) {
									$content .= ';';
								}
							}
							
							$content .= "]; \n";
							$content .= "[Y,e] = cmdscale(D)";
						?>
					</table>
					<a onclick="changeDisplay('textarea_<?=$i;?>');">Kód pre MATLAB</a>
					<textarea id="<?='textarea_'.$i;?>" class="form-control textarea-no-resize" readonly><?=$content?></textarea>
				<?php
			}
		?>
		
		<script>
			$(function(){
				$("textarea").each(function(){
					$(this).height(this.scrollHeight);
					$(this).hide();
				});
			});
		</script>
		
		<script>
			var pomString = 'D = [';
			
			for (let i in covMatrix) {
				if (i < 5) {
					$("#covMaticaTable1").append("<tr id='"+i+"'><td>"+(+i + +1)+"</td>");
					for (let j in covMatrix[i]) {
						if (j < 5) {
							var row = document.getElementById(i);
							var cell = row.insertCell(-1);
							if (i == j) {
								cell.style.background = 'yellow';
							}
							cell.innerHTML = covMatrix[i][j].toFixed(5);
						}
					}
					$("#covMaticaTable1").append("</tr>");
				} else {
					$("#covMaticaTable2").append("<tr id='"+i+"'><td>"+(+i - 4)+"</td>");
					var pomStringInner = '';
					
					for (let j in covMatrix[i]) {
						if (j >= 5) {
							var row = document.getElementById(i);
							var cell = row.insertCell(-1);
							if (i == j) {
								cell.style.background = 'yellow';
							}
							cell.innerHTML = covMatrix[i][j].toFixed(3);
							pomStringInner += covMatrix[i][j].toFixed(5) + ' ';
						}
					}
					pomStringInner += ';';
					pomString 	   += pomStringInner;
					pomStringInner = '';
					
					$("#covMaticaTable2").append("</tr>");

				}
			}
			pomString = pomString.substring(0, pomString.length - 1);
			pomString += ']';
			
			$("textarea#textarea_dyn").val(pomString);
			
			// console.log(pomString);
		</script>
	<?php
		}
	?>
</div>