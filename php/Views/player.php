<?php
	include('php/Classes/Bvh_file_loader.php');

	$file_loader = new Bvh_file_loader();
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
	<div>
		<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
			<h2 class="nomargin" id="fileName" style="padding-top:5px; padding-left:10px;">-</h2>
			<a href="analysis/<?=$file_loader->file_name?>" title="Spustiť analýzu"  class="btn btn-lg btn-success myButton" style="float:right; margin-top:-40px;"><span class="glyphicon glyphicon-stats"></span></a>
		</div>
	</div>
	<canvas id="myCanvas" width="1230" height="350"></canvas>
</div>	
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
	<div class="btn-group">
		<button onclick="player.changePlayStop();"  title="Pause" id="playPauseButton" class="btn btn-lg btn-primary myButton borderRadius-top-left-none"><span class="glyphicon glyphicon-pause"></span></button>
		<button onclick="player.changeShowTraj();"  title="Ukázať trajektóriu" id="trajectoryButton" class="btn btn-lg btn-primary myButton"><span class="glyphicon glyphicon-eye-open"></span></button>
		<button onclick="player.prevFrame();" 		title="Predchádzajúci snímok" 	class="btn btn-lg btn-primary myButton"><span class="glyphicon glyphicon-step-backward"></span></button>
		<button onclick="player.nextFrame();" 		title="Nasledujúci snímok" class="btn btn-lg btn-primary myButton"><span class="glyphicon glyphicon-step-forward"></span></button>
		<button onclick="player.repeatAnimation();" title="Začať znova"  class="btn btn-lg btn-primary myButton borderRadius-top-right-none"><span class="glyphicon glyphicon-repeat"></span></button>

		
	</div>
	<div class="btn-group" style="	float:right;">
		<div title="Snímky" id="timeCounter" class="player_info">00:00.00/00:00.00</div>	
		<div title="Snímky" id="frameCounter" class="player_info">000/000</div>	
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 nopadding-right margin-top-15">
	<?php			
		for ($i = 2; $i < count($file_loader->file_list['file_name']); $i++) {		
			?>
				<a href="play/<?=substr($file_loader->file_list['file_name'][$i],0,-4)?>">
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 margin-top-15 nopadding-left">	
						<div class="box_prehravaca">
							<p style="font-weight:bold; color:#000000; margin-bottom:2px;"><?=$file_loader->file_list['file_name'][$i]?></p>
							<p style="color:#4d4d4d; margin-bottom:5px;"><?=number_format($file_loader->file_list['file_size'][$i] / 1000, 1, '.', '')?> kB</p>
							<button class="btn btn-success btn-sm" style="float:right; margin-top:-30px; padding-bottom:8px; padding-top:2px;"><span class="glyphicon glyphicon-play-circle"></span></button>
						</div>
					</div>
				</a>
			<?php
		}
	?>
</div>	

<script>
		var c 				= document.getElementById("myCanvas");
		var ctx 			= c.getContext("2d");
		var canvasHeight 	= c.height;	
		
		var array_of_content = <?php echo json_encode($file_loader->file_content);?>;
		var file_name 		 = <?php echo json_encode($file_loader->file_name);?>;
		if (array_of_content !== false) {
			player = new Bvh_play(array_of_content, file_name);
			player.init();
			player.play();
		} else {
			ctx.font = "48px Roboto";
			ctx.fillText("Prehrávač",505,85);
			ctx.font = "18px Roboto";
			ctx.fillText("Vyberte záznam zo zoznamu (dole), ktorý chcete prehrať.",395,115);
			
			ctx.beginPath();
			ctx.moveTo(30, 350);
			ctx.lineTo(30, 180);
			ctx.stroke();
			point_n(30,170,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Play/Pause",40,185);
			
			ctx.beginPath();
			ctx.moveTo(90, 350);
			ctx.lineTo(90, 215);
			ctx.stroke();
			point_n(90,135,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Zobraziť/Skryť trajektóriu",100,220);
			
			ctx.beginPath();
			ctx.moveTo(150, 350);
			ctx.lineTo(150, 250);
			ctx.stroke();
			point_n(150,100,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Predchádzajúci snímok",160,255);
			
			ctx.beginPath();
			ctx.moveTo(210, 350);
			ctx.lineTo(210, 285);
			ctx.stroke();
			point_n(210,65,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Nasledujúci snímok",220,290);
				
			ctx.beginPath();
			ctx.moveTo(270, 350);
			ctx.lineTo(270, 320);
			ctx.stroke();
			point_n(270,30,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Začať znova",280,325);
			
			ctx.beginPath();
			ctx.moveTo(897, 350);
			ctx.lineTo(897, 300);
			ctx.stroke();
			point_n(897,50,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Čas",907,305);
			
			ctx.beginPath();
			ctx.moveTo(1119, 350);
			ctx.lineTo(1119, 300);
			ctx.stroke();
			point_n(1119,50,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Snímok",1129,305);
			
			ctx.beginPath();
			ctx.moveTo(30,0);
			ctx.lineTo(30,50);
			ctx.stroke();
			point_n(30,300,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Názov záznamu",40,55);
			
			ctx.beginPath();
			ctx.moveTo(1190,0);
			ctx.lineTo(1190,25);
			ctx.lineTo(1050,25);
			ctx.lineTo(1050,50);
			ctx.stroke();		
			point_n(1050,300,'#000000',1);
			ctx.font = "15px Roboto";
			ctx.fillText("Analýza záznamu",1060,55);
		}
	</script>