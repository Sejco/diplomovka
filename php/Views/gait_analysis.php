<?php
	include('php/Classes/Bvh_file_loader.php');

	$file_loader = new Bvh_file_loader();
?>

<div class="col-xs-9 col-sm-9 col-md-9 col-lg-6 col-lg-offset-2">
	<div>
		<div>
			<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
				<h2 class="nomargin" id="fileName" style="padding-top:5px; padding-left:10px;"></h2>
			</div>
		</div>
		<div id="myChart" width="1240" height="500" style="box-shadow: 0 1px 2px rgba(0,0,0,.1); background-color:rgb(234,209,188);"></div>
		<canvas id="myCanvas" width="1240" height="498">
		</canvas>
<!--
		<table class="table table-style-1 table-style-2 table-xtra-condensed nomargin font-size-12">
			<tbody>
				<tr>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['Head']);" 			 class="hand width-9-percent" title="Head"			name="Head"			>Head</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['Spine1']);" 		 class="hand width-9-percent" title="Spine 1"		name="Spine1"		>Spine 1</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['ToSpine']);" 		 class="hand width-9-percent" title="To Spine"		name="ToSpine"		>To Spine</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftShoulder']);" 	 class="hand width-9-percent" title="Left Shoulder" name="LeftShoulder"	>L Shoulder</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftArm']);" 		 class="hand width-9-percent" title="Left Arm"		name="LeftArm"		>L Arm</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftForeArm']);" 	 class="hand width-9-percent" title="Left Fore Arm" name="LeftForeArm"	>L Fore Arm</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftHand']);" 		 class="hand width-9-percent" title="Left Hand"	 	name="LeftHand"		>L Hand</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftUpLeg']);" 	 class="hand width-9-percent" title="Left Up Leg"	name="LeftUpLeg"	>L Up Leg</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftLeg']);" 		 class="hand width-9-percent" title="Left Leg"		name="LeftLeg"		>L Leg</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftFoot']);" 		 class="hand width-9-percent" title="Left Foot"	 	name="LeftFoot"		>L Foot</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['LeftToeBase']);" 	 class="hand width-9-percent" title="Left Toe Base" name="LeftToeBase" 	>L Toe Base</td>
				</tr>
				<tr>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['Neck']);" 			 class="hand width-9-percent"  title="Neck"			 name="Neck"		 >Neck</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['Spine']);" 		 class="hand width-9-percent"  title="Spine"		 name="Spine"		 >Spine</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['Hips']);" 			 class="hand width-9-percent"  title="Hips"			 name="Hips"		 >Hips</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightShoulder']);"	 class="hand width-9-percent"  title="Right Shoulder"name="RightShoulder">R Shoulder</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightArm']);" 		 class="hand width-9-percent"  title="Right Arm"	 name="RightArm"	 >R Arm</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightForeArm']);" 	 class="hand width-9-percent"  title="Right Fore Arm"name="RightForeArm" >R Fore Arm</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightHand']);" 	 class="hand width-9-percent"  title="Right Hand"	 name="RightHand"	 >R Hand</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightUpLeg']);" 	 class="hand width-9-percent"  title="Right Up Leg"	 name="RightUpLeg"	 >R Up Leg</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightLeg']);" 		 class="hand width-9-percent"  title="Right Leg"	 name="RightLeg"	 >R Leg</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightFoot']);" 	 class="hand width-9-percent"  title="Right Foot"    name="RightFoot"    >R Foot</td>
					<td onclick="reDrawCanvas(); player && drawFocusedPart(['RightToeBase']);" 	 class="hand width-9-percent"  title="Right Toe Base"name="RightToeBase" >R Toe Base</td>
				</tr>
			</body>
		</table>
-->
	</div>
	<div class="margin-top-15">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-left">
			<table class="table table-xtra-condensed table-hover table-bordered nomargin inner_border">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Dĺžka</td>
				</tr>
				<tr onmouseenter="player && drawLegendHeight();" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Trupu</td>
					<td id="height" class="cislo"><center><center>-</center></center></td>
				</tr>
				<tr onmouseenter="player && drawLegendLeftArm();" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Ľavej ruky</td>
					<td id="L_arm_length" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendRightArm();" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Pravej ruky</td>
					<td id="R_arm_length" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendLeftLeg();" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Ľavej nohy</td>
					<td id="L_leg_length" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendRightLeg();" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Pravej nohy</td>
					<td id="R_leg_length" class="cislo"><center>-</center></td>
				</tr>
			</table>
			
			<table class="table table-xtra-condensed table-hover table-bordered inner_border margin-top-15"  style="margin-bottom:0px;">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Rýchlosť</td>
				</tr>
				<tr onmouseenter="player && drawWalkSpeedLegend(features.decriptor.visualizationPoints.walk_speed);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Chôdze</td>
					<td id="walk_speed" class="cislo"><center>-</center></td>
				</tr>
			</table>

			<table class="table table-xtra-condensed table-hover table-bordered inner_border  margin-top-15">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Kroky</td>
				</tr>
				<tr onmouseenter="player && drawStrideLengthLegend(features.decriptor.visualizationPoints.L_stride_length);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Ľavý krok</td>
					<td id="L_stride_length" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawStrideLengthLegend(features.decriptor.visualizationPoints.R_stride_length);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Pravý krok</td>
					<td id="R_stride_length" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawStepWidthLegend(features.decriptor.visualizationPoints.step_width);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Šírka kroku</td>
					<td id="step_width" class="cislo"><center>-</center></td>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding">
			<table class="table table-xtra-condensed table-hover table-bordered nomargin inner_border">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Str. kvad. odchýlka</td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceX(features.decriptor.visualizationPoints.head_variance_x, ['Head']);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Hlavy X</td>
					<td id="head_variance_x" class="cislo"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceY(features.decriptor.visualizationPoints.head_variance_y);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Hlavy Y</td>
					<td class="cislo" id="head_variance_y"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceY(features.decriptor.visualizationPoints.L_knee_variance_y);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Ľavého kolena Y</td>
					<td class="cislo" id="L_knee_variance_y"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceY(features.decriptor.visualizationPoints.R_knee_variance_y);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Pravého kolena Y</td>
					<td class="cislo" class="cislo" id="R_knee_variance_y"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceX(features.decriptor.visualizationPoints.L_shoulder_variance_x, ['LeftShoulder']);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Ľavého ramena X</td>
					<td class="cislo" id="L_shoulder_variance_x"><center>-</center></td>
				</tr>
				<tr onmouseenter="player && drawLegendVarianceX(features.decriptor.visualizationPoints.R_shoulder_variance_x, ['RightShoulder']);" onmouseleave="player && reDrawCanvas();" class="hand">
					<td>Pravého ramena X</td>
					<td class="cislo" id="R_shoulder_variance_x"><center>-</center></td>
				</tr>
			</table>
			<table class="table table-xtra-condensed table-hover table-bordered margin-top-15 inner_border">
				<tr>
					<td colspan="3" class="nadpis_tabulky">Rotácia ramien</td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.shouldersHorizontalRot, 'Horizontálna rotácia ramien');" class="hand">				
					<td>Horizontálna</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.shouldersVericalRot, 'Vertikálna rotácia ramien');" class="hand">
					<td>Vertikálna</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-right">
			<table class="table table-xtra-condensed table-hover table-bordered nomargin inner_border">
				<tr>
					<td colspan="3" class="nadpis_tabulky">Ohyb</td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.L_leg_bend, 'Ohyb ľavej nohy');" 	class="hand">
					<td>Ľavej nohy</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.R_leg_bend, 'Ohyb pravej nohy');"	 class="hand">
					<td>Pravej nohy</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>		
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.L_arm_bend, 'Ohyb ľavej ruky');" 	class="hand">
					<td>Ľavej ruky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.R_arm_bend, 'Ohyb pravej ruky');"	 class="hand">
					<td>Pravej ruky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.head_leaning, 'Náklon hlavy');"	 class="hand">
					<td>Náklon hlavy</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.body_leaning, 'Náklon tela');"	 class="hand">
					<td>Náklon tela</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
			</table>

			<table class="table table-xtra-condensed table-hover table-bordered margin-top-15 inner_border">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Vzdialenosť</td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.hands_distances, 'Vzdialenosť rúk');" class="hand">
					<td>Rúk</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.elbow_distances, 'Vzdialenosť lakťov');" class="hand">
					<td>Lakťov</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.knee_distances, 'Vzdialenosť kolien');" class="hand">
					<td>Kolien</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.foot_distances, 'Vzdialenosť piat');" class="hand">
					<td>Piat</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.toe_distances, 'Vzdialenosť špičiek');" class="hand">
					<td>Spičiek</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
			</table>
		</div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopadding-right">
			<table class="table table-xtra-condensed table-hover table-bordered nomargin inner_border">
				<tr>
					<td colspan="2" class="nadpis_tabulky">Zrýchlenie</td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.head_acceleration, 'Zrýchlenie hlavy');" class="hand">
					<td>Hlavy</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.hips_acceleration, 'Zrýchlenie bedier');" class="hand">
					<td>Bedier</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.left_fore_arm_acc, 'Zrýchlenie ľavého lakťa');" class="hand">
					<td>Ľavého lakťa</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.right_fore_arm_acc, 'Zrýchlenie pravého lakťa');" class="hand">
					<td>Pravého lakťa</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.left_hand_acc, 'Zrýchlenie ľavej ruky');" class="hand">
					<td>Ľavej ruky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.right_hand_acc, 'Zrýchlenie pravej ruky');" class="hand">
					<td>Pravej ruky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.left_leg_acc, 'Zrýchlenie ľavého kolena');" class="hand">
					<td>Ľavého kolena</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.right_leg_acc, 'Zrýchlenie pravého kolena');" class="hand">
					<td>Pravého kolena</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.left_foot_acc, 'Zrýchlenie ľavej päty');" class="hand">
					<td>Ľavej päty</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.right_foot_acc, 'Zrýchlenie pravej päty');" class="hand">
					<td>Pravej päty</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.left_toe_acc, 'Zrýchlenie ľavej špičky');" class="hand">
					<td>Ľavej špičky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
				<tr onmouseenter="player && plot(features.decriptor.visualizationPoints.right_toe_acc, 'Zrýchlenie pravej špičky');" class="hand">
					<td>Pravej špičky</td>
					<td class="cislo"><center><span class="glyphicon glyphicon-signal"></span></center></td>
				</tr>
			</table>
		</div>	
	</div>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2" style="padding-right:10px; padding-left:0px;">
	<div style=" padding-right:1px;">
		<div class="standard2" style="background-color:#419641; color:#ffffff;">
			<h2 class="nomargin" style="padding-top:5px"><center>Záznamy chôdze</center></h2>
		</div>
	</div>
		<?php
			for ($i = 2; $i < count($file_loader->file_list['file_name']); $i++) {
				?>
					<a class="wrrr" href="analysis/<?=substr($file_loader->file_list['file_name'][$i],0,-4)?>">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 nopadding-left" style="padding-right:1px;">
							<div class="box_analyzera">
								<p style="color:#000000; font-weight:bold;"><?=substr($file_loader->file_list['file_name'][$i],0,-4)?><button class="btn btn-success btn-xs" style="padding-left:3px; padding-bottom:2px; padding-top:0px; float:right;"><span class="glyphicon glyphicon-stats"></span></button></p>
							</div>
						</div>
					</a>
				<?php
			}
		?>

</div>

<script>
	scale = 2.7;
	cy 	  = 290;
	cx    = 500;

	var c 			 = document.getElementById("myCanvas");
	var ctx 		 = c.getContext("2d");
	var canvasHeight = c.height;

	var array_of_content = <?php echo json_encode($file_loader->file_content);?>;
	var file_name 		 = <?php echo json_encode($file_loader->file_name);?>;
	var player = false;

	if (array_of_content !== false) {
		axis('X', 'Y', 'Z');
		legend();

		player 			 = new Bvh_play(array_of_content, file_name);
		player.arrPoints = player.walkFilter.filterWalkGetArrays();
		if (player.arrPoints['Hips'].length > 0) {
			player.body_anim_root = player.walkFilter.setRightDirectionOfBody();
			player.setZaxixShiftForFilteredWalk(player.arrPoints.Hips);
			player.draw_walk_sequence_side();
			player.baseBodyFrame = player.bvh.calculateBaseFrame();

			features = new Features(player.arrPoints);

			// for (let i in colors) {    // farba legendy ku klbom
				// document.getElementsByName(i)[0].style.backgroundColor = colors[i];
			// }
		} else {
			player = false;
		}
	} else {
		ctx.font = "60px Roboto";
		ctx.fillText("Analyzér chôdze",400,125);
		ctx.font = "25px Roboto";
		ctx.fillText("Vyberte záznam z pravého menu, ktorý chcete analyzovať.",305,180);
		ctx.font = "22px Roboto";
		ctx.fillText("Vypočítané parametre si následne bude môcť graficky znázorniť.",310,270);
		ctx.fillText("Pre grafické znázornenie stačí myšou, nabehnúť na názov parametra zo zoznamu dole.",200,310);
		
		ctx.beginPath();
		ctx.moveTo(50,0);
		ctx.lineTo(50,50);
		ctx.stroke();
		
		point_n(50,450,'#000000',1);
		ctx.font = "20px Roboto";
		ctx.fillText("Názov záznamu",60,55);
		
		ctx.beginPath();
		ctx.moveTo(1190,0);
		ctx.lineTo(1190,25);
		ctx.lineTo(1050,25);
		ctx.lineTo(1050,50);
		ctx.stroke();
		
		point_n(1050,450,'#000000',1);
		ctx.font = "20px Roboto";
		ctx.fillText("Prehrať záznam",1060,55);
	}

	player && features.fillTableWithValues();
	$("#fileName").html((player.file_name || '-') + '<a href="play/' + (player.file_name || '-') + '"title="Prehrať záznam" class="btn btn-success myButton" style="float:right; margin-top:-5px;"><span class="glyphicon glyphicon-play"></span></a>');
</script>