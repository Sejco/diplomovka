<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
	<div>
		<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
			<h2 class="nomargin" id="fileName" style="padding-top:5px; padding-left:10px;">Vyhľadávanie osôb</h2>
		</div>
	</div>
	<div class="standard col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php
			include('php/Classes/Person_search.php');
			include('php/Classes/Bvh_file_loader.php');
			
			$person_search = new Person_search();

			if ($person_search->found !== null) {
				if ($person_search->found === true) {
					?>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 margin-top-30">
							<center><h2>Hľadaná osoba je <strong><?=$person_search->personName['firstname'];?>
							</strong> so zhodou <strong><?=$person_search->match;?>%</strong></h2></center>
						</div>
					<?php
				} else {
					?>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 margin-top-30">
							<center><h2>Žiadna zhoda!</h2></center>
						</div>
					<?php
				}
			}
			
			if ($person_search->file_content === false) {
				?>
					<p>Vyberte súbor vo formáte .bvh</p>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form class="form-horizontal" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<input type="file" class="filestyle" id="input_bvh" name="input_bvh" data-buttonName="btn-primary" required>
							</div>

							<div class="form-group"> 
								<button type="submit" class="btn btn-primary" name="submit_step_1">Pokračovať</button>
							</div>
						</form>
					</div>
				<?php
			}
			
			if ($person_search->file_content !== false) {
				?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-30">
						<form class="form-horizontal" method="post">
							<div class="form-group"> 
								<div class="col-sm-12 col-md-12 col-lg-12">
									<button type="submit" class="btn btn-primary" name="submit_search">Potvrdiť</button>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2 col-md-2 col-lg-2">
									<strong>Názov parametra</strong>
								</div>
								<div class="col-sm-7 col-md-7 col-lg-7">
									<center><strong>Hodnota</strong></center>
								</div>
								<div class="col-sm-1 col-md-1 col-lg-1">
									<center><strong>Váha</strong></center>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="height">Výška trupu</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="height" name="height" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="height_weight" name="height_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_arm_length">Dĺžka ľavej ruky</label>		
								<div class="col-sm-7">
									<input type="text" class="form-control" id="L_arm_length" name="L_arm" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_arm_length_weight" name="L_arm_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_arm_length">Dĺžka pravej ruky</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="R_arm_length" name="R_arm" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_arm_length_weight" name="R_arm_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_leg_length">Dĺžka ľavej nohy</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="L_leg_length" name="L_leg" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_leg_length_weight" name="L_leg_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_leg_length">Dĺžka pravej nohy</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="R_leg_length" name="R_leg" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_leg_length_weight" name="R_leg_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="walk_speed">Rýchlosť chôdze</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="walk_speed" name="walk_speed" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="walk_speed_weight" name="walk_speed_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_stride_length">Dĺžka ľavého kroku</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="L_stride_length" name="L_stride_length" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_stride_length_weight" name="L_stride_length_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_stride_length">Dĺžka pravého kroku</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="R_stride_length" name="R_stride_length" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_stride_length_weight" name="R_stride_length_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="step_width">Šírka kroku</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="step_width" name="step_width" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="step_width_weight" name="step_width_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="head_variance_x">MSE hlavvy po X</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="head_variance_x" name="head_variance_x" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="head_variance_x_weight" name="head_variance_x_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="head_variance_y">MSE hlavvy po Y</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="head_variance_y" name="head_variance_y" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="head_variance_y_weight" name="head_variance_y_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_knee_variance_y">MSE ľavého kolena po Y</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="L_knee_variance_y" name="L_knee_variance_y" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_knee_variance_y_weight" name="L_knee_variance_y_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_knee_variance_y">MSE pravého kolena po Y</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="R_knee_variance_y" name="R_knee_variance_y" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_knee_variance_y_weight" name="R_knee_variance_y_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_shoulder_variance_x">MSE ľavého ramena po X</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="L_shoulder_variance_x" name="L_shoulder_variance_x" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_shoulder_variance_x_weight" name="L_shoulder_variance_x_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_shoulder_variance_x">MSE pravého ramena po X</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="R_shoulder_variance_x" name="R_shoulder_variance_x" readonly>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_shoulder_variance_x_weight" name="R_shoulder_variance_x_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="head_leaning">Náklon hlavy</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="head_leaning" name="head_leaning" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="head_leaning_weight" name="head_leaning_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="body_leaning">Náklon tela</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="body_leaning" name="body_leaning" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="body_leaning_weight" name="body_leaning_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_arm_bend">Ohyb ľavej ruky</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="L_arm_bend" name="L_arm_bend" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_arm_bend_weight" name="L_arm_bend_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_arm_bend">Ohyb pravej ruky</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="R_arm_bend" name="R_arm_bend" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_arm_bend_weight" name="R_arm_bend_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="L_leg_bend">Ohyb ľavej nohy</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="L_leg_bend" name="L_leg_bend" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="L_leg_bend_weight" name="L_leg_bend_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="R_leg_bend">Ohyb pravej nohy</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="R_leg_bend" name="R_leg_bend" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="R_leg_bend_weight" name="R_leg_bend_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="hands_distances">Vzdialenosť rúk</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="hands_distances" name="hands_distances" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="hands_distances_weight" name="hands_distances_weight">
								</div>
							</div>
							<div class="form-group"><label style="text-align:left;"class="control-label col-sm-2" for="elbow_distances">Vzdialenosť lakťov</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="elbow_distances" name="elbow_distances" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="elbow_distances_weight" name="elbow_distances_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="knee_distances">Vzdialenosť kolien</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="knee_distances" name="knee_distances" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="knee_distances_weight" name="knee_distances_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="foot_distances">Vzdialenosť piat</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="foot_distances" name="foot_distances" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="foot_distances_weight" name="foot_distances_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="toe_distances">Vzdialenosť špičiek</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="toe_distances" name="toe_distances" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="toe_distances_weight" name="toe_distances_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="shouldersHorizontalRot">Horizontálna rotácia ramien</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="shouldersHorizontalRot" name="shouldersHorizontalRot" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="shouldersHorizontalRot_weight" name="shouldersHorizontalRot_weight">
								</div>
							</div>	
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="shouldersVericalRot">Vertikálna rotácia ramien</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="shouldersVericalRot" name="shouldersVericalRot" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="shouldersVericalRot_weight" name="shouldersVericalRot_weight">
								</div>
							</div>	
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="head_acceleration">Zrýchlenie hlavy</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="head_acceleration" name="head_acceleration" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="head_acceleration_weight" name="head_acceleration_weight">
								</div>
							</div>	
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="hips_acceleration">Zrýchlenie bedier</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="hips_acceleration" name="hips_acceleration" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="1" class="form-control" id="hips_acceleration_weight" name="hips_acceleration_weight">
								</div>
							</div>				
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="left_fore_arm_acc">Zrýchlenie ľavého lakťa</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="left_fore_arm_acc" name="left_fore_arm_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="left_fore_arm_acc_weight" name="left_fore_arm_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="right_fore_arm_acc">Zrýchlenie pravého lakťa</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="right_fore_arm_acc" name="right_fore_arm_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="right_fore_arm_acc_weight" name="right_fore_arm_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="left_hand_acc">Zrýchlenie ľavej dlane</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="left_hand_acc" name="left_hand_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="1" class="form-control" id="left_hand_acc_weight" name="left_hand_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="right_hand_acc">Zrýchlenie pravej dlane</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="right_hand_acc" name="right_hand_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="1" class="form-control" id="right_hand_acc_weight" name="right_hand_acc_weight">
								</div>
							</div>							
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="left_leg_acc">Zrýchlenie ľavého kolena</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="left_leg_acc" name="left_leg_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="left_leg_acc_weight" name="left_leg_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="right_leg_acc">Zrýchlenie pravého kolena</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="right_leg_acc" name="right_leg_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="right_leg_acc_weight" name="right_leg_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="left_foot_acc">Zrýchlenie ľavej päty</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="left_foot_acc" name="left_foot_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="1" class="form-control" id="left_foot_acc_weight" name="left_foot_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="right_foot_acc">Zrýchlenie pravej päty</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="right_foot_acc" name="right_foot_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="1" class="form-control" id="right_foot_acc_weight" name="right_foot_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="left_toe_acc">Zrýchlenie ľavej špičky</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="left_toe_acc" name="left_toe_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="left_toe_acc_weight" name="left_toe_acc_weight">
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;"class="control-label col-sm-2" for="right_toe_acc">Zrýchlenie pravej špičky</label>
								<div class="col-sm-7">
									<textarea class="form-control textarea-no-resize" id="right_toe_acc" name="right_toe_acc" readonly></textarea>
								</div>
								<div class="col-sm-1">
									<input type="text" value="0" class="form-control" id="right_toe_acc_weight" name="right_toe_acc_weight">
								</div>
							</div>

							<div class="form-group"> 
								<div class="col-sm-12 col-md-12 col-lg-12">
									<button type="submit" class="btn btn-primary" name="submit_search">Potvrdiť</button>
								</div>
							</div>
						</form>
					</div>

					<script>
						var array_of_content = <?php echo json_encode($person_search->file_content);?>;
						var file_name 		 = <?php echo json_encode($person_search->file_name);?>;
						var player = false;

						if (array_of_content !== false) {
							player 			 = new Bvh_play(array_of_content, file_name);
							player.arrPoints = player.walkFilter.filterWalkGetArrays();
							if (player.arrPoints['Hips'].length > 0) {
								features = new Features(player.arrPoints);

								features.fillTableWithValuesArrayConversion();
							} else {
								player = false;
							}
						}
					</script>
				<?php
			}
		?>
	</div>
</div>
