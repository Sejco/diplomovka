<?php
	include('php/Classes/Bvh_file_upload.php');
	include('php/Classes/Bvh_file_loader.php');
	
	$file_upload = new Bvh_file_upload();
?>

<?php

	if (@$_SESSION['ppl_upload_done'] == true) {
		?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 margin-top-30">
				<div id="myModal" class="modal_win" onclick="document.getElementById('myModal').style.display = 'none'">
					<div class="modal-content">
						<span class="close" onclick="document.getElementById('myModal').style.display = 'none'">x</span>
						<p><strong>Nahrávanie bolo úspešne dokončené.</strong></p>
					</div>
				</div>
			</div>
		<?php
		
		$_SESSION['ppl_upload_done'] = false;
	}
?>
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">
	<div>
		<div class="standard2" style="background-color:#337ab7; color:#ffffff;">
			<h2 class="nomargin" id="fileName" style="padding-top:5px; padding-left:10px;">Pridanie záznamu chôdze</h2>
		</div>
	</div>
	<div class="standard col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php
			if ($file_upload->file_content === false) {
				?>
					<p>Vyberte súbor vo formáte .bvh</p>
					<p class="error"><?php echo @$file_upload->error;?></p>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form class="form-horizontal" method="post" enctype="multipart/form-data">
							<div class="form-group">					
								<input type="file" class="filestyle" id="user_gait_key_frames" name="user_gait_key_frames" data-buttonName="btn-primary" required>							
							</div>

							<div class="form-group"> 
								<button type="submit" class="btn btn-primary" name="submit_step_1">Pokračovať</button>
							</div>
						</form>
					</div>
				<?php
			}
		?>

		<?php
			if ($file_upload->file_content !== false) {
				?>
					<p>Skontrolujte, či sa všetky údaje správne načítali.</p>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-30">
						<form class="form-horizontal" method="post">
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="gait_bvh_file">Záznam chôdze</label>	
								<div class="col-sm-8">
									<input type="text" class="form-control" id="gait_bvh_file" name="gait_bvh_file" value="<?=$file_upload->file_name?>" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="name">Meno:</label>
								<div class="col-sm-8">
									<input type="text" name="name" id="name" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="surname">Priezvisko:</label>
								<div class="col-sm-8">
									<input type="text" name="surname" id="surname" class="form-control" required>
								</div>
							</div>	
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="height">Height</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="height" name="height" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_arm_length">Left arm</label>
								
								<div class="col-sm-8">
									<input type="text" class="form-control" id="L_arm_length" name="L_arm" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_arm_length">Right arm</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="R_arm_length" name="R_arm" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_leg_length">Left Leg</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="L_leg_length" name="L_leg" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_leg_length">Right leg</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="R_leg_length" name="R_leg" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="walk_speed">Walk speed</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="walk_speed" name="walk_speed" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_stride_length">Left stride</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="L_stride_length" name="L_stride" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_stride_length">Right stride</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="R_stride_length" name="R_stride" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="step_width">Step width</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="step_width" name="step_width" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="head_variance_x">Head variance X</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="head_variance_x" name="head_variance_x" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="head_variance_y">Head variance Y</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="head_variance_y" name="head_variance_y" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_knee_variance_y">Left knee var. Y</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="L_knee_variance_y" name="L_knee_variance_y" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_knee_variance_y">Right knee var. Y</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="R_knee_variance_y" name="R_knee_variance_y" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_shoulder_variance_x">Left shoulder var. X</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="L_shoulder_variance_x" name="L_shoulder_variance_x" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_shoulder_variance_x">Right shoulder var. X</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="R_shoulder_variance_x" name="R_shoulder_variance_x" readonly>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="head_leaning">Head leaning</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="head_leaning" name="head_leaning" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="body_leaning">Body leaning</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="body_leaning" name="body_leaning" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_arm_bend">Left arm bend</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="L_arm_bend" name="L_arm_bend" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_arm_bend">Right arm bend</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="R_arm_bend" name="R_arm_bend" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="L_leg_bend">Left leg bend</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="L_leg_bend" name="L_leg_bend" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="R_leg_bend">Right leg bend</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="R_leg_bend" name="R_leg_bend" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="hands_distances">Hands distance</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="hands_distances" name="hands_distances" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="elbow_distances">Elbows distance</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="elbow_distances" name="elbow_distances" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="knee_distances">Knees distance</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="knee_distances" name="knee_distances" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="foot_distances">Foots distance</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="foot_distances" name="foot_distances" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="toe_distances">Toe caps distance</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="toe_distances" name="toe_distances" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="shouldersHorizontalRot">Shoulders horizontal rotation</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="shouldersHorizontalRot" name="shouldersHorizontalRot" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="shouldersVericalRot">Shoulders vertical rotation</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="shouldersVericalRot" name="shouldersVericalRot" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="head_acceleration">Head acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="head_acceleration" name="head_acceleration" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="hips_acceleration">Hips acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="hips_acceleration" name="hips_acceleration" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="left_fore_arm_acc">L Elbow acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="left_fore_arm_acc" name="left_fore_arm_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="right_fore_arm_acc">R Elbow acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="right_fore_arm_acc" name="right_fore_arm_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="left_hand_acc">L Hand acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="left_hand_acc" name="left_hand_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="right_hand_acc">R Hand acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="right_hand_acc" name="right_hand_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="left_leg_acc">L Knee acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="left_leg_acc" name="left_leg_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="right_leg_acc">R Knee acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="right_leg_acc" name="right_leg_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="left_foot_acc">L Foot acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="left_foot_acc" name="left_foot_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="right_foot_acc">R Foot acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="right_foot_acc" name="right_foot_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="left_toe_acc">L toe acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="left_toe_acc" name="left_toe_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group">
								<label style="text-align:left;" class="control-label col-sm-2" for="right_toe_acc">R toe acceleration</label>
								<div class="col-sm-8">
									<textarea class="form-control textarea-no-resize" id="right_toe_acc" name="right_toe_acc" readonly></textarea>
								</div>
							</div>
							<div class="form-group"> 
								<label style="text-align:left;" class="control-label col-sm-2"></label>		
								<div class="col-sm-8">
									<button type="submit" class="btn btn-primary" name="submit_step_2">Potvrdiť a uložiť</button>
								</div>
							</div>
						</form>
					</div>

					<script>
						var array_of_content = <?php echo json_encode($file_upload->file_content);?>;
						var file_name 		 = <?php echo json_encode($file_upload->file_name);?>;
						var player = false;

						if (array_of_content !== false) {
							player 			 = new Bvh_play(array_of_content, file_name);
							player.arrPoints = player.walkFilter.filterWalkGetArrays();
							if (player.arrPoints['Hips'].length > 0) {
								features = new Features(player.arrPoints);
								features.fillTableWithValuesArrayConversion();
							} else {
								player = false;
							}
						}
					</script>
				<?php
			}
		?>
	</div>
</div>
<script>
	var eig_v = [
	[	0.0217  , -0.0263  , -0.0120  ,  0.0539  ,  0.3065  ,  0.5694 ,  -0.3252  ,  0.6454  , -0.2337  , -0.0273  ],
	[	0.0073  ,  0.0054  ,  0.0119  , -0.1265  , -0.1554  ,  0.1347 ,  -0.4525  , -0.3941  , -0.4371  ,  0.6248  ],
	[	0.0105  , -0.1595  ,  0.0005  ,  0.1645  ,  0.0504  ,  0.7523 ,   0.3079  , -0.4995  ,  0.1593  , -0.0958  ],
	[	0.0533  , -0.2172  , -0.0043  , -0.4717  , -0.5674  ,  0.2153 ,  -0.0358  ,  0.2759  ,  0.4884  ,  0.2080  ],
	[   -0.0248 ,   0.1802 ,   0.0222 ,  -0.0065 ,   0.0020 ,   0.0249,   -0.7389 ,  -0.2735 ,   0.3646 ,  -0.4604 ],
	[   -0.1524 ,   0.2119 ,  -0.0944 ,   0.7828 ,  -0.4865 ,   0.0383,   -0.0614 ,   0.1619 ,   0.1156 ,   0.1695 ],
	[   -0.2980 ,   0.7660 ,   0.0817 ,  -0.3260 ,  -0.2292 ,   0.2045,    0.2015 ,   0.0138 ,  -0.2180 ,  -0.1697 ],
	[	0.4151  ,  0.2256  , -0.8771  , -0.0681  ,  0.0238  ,  0.0238 ,   0.0280  , -0.0285  ,  0.0132  ,  0.0085  ],
	[   -0.4156 ,   0.2437 ,  -0.1060 ,  -0.0273 ,   0.5052 ,  -0.0145,    0.0107 ,   0.0067 ,   0.5062 ,   0.4941 ],
	[	0.7341  ,  0.3902  ,  0.4507  ,  0.0845  ,  0.1115  ,  0.0255 ,   0.0466  ,  0.0219  ,  0.2004  ,  0.2065  ]
	]; 
	
	var orig_axis = [
		[1,0,0,0,0,0,0,0,0,0],
		[0,1,0,0,0,0,0,0,0,0],
		[0,0,1,0,0,0,0,0,0,0],
		[0,0,0,1,0,0,0,0,0,0],
		[0,0,0,0,1,0,0,0,0,0],
		[0,0,0,0,0,1,0,0,0,0],
		[0,0,0,0,0,0,1,0,0,0],
		[0,0,0,0,0,0,0,1,0,0],
		[0,0,0,0,0,0,0,0,1,0],
		[0,0,0,0,0,0,0,0,0,1]
	];
	
	// for(let i in orig_axis) {
		// console.log(scalarProduct(eig_v[3], orig_axis[i]));
	// }
</script>








