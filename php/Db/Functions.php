<?php	
	function get_all_users(){
		
		$sql = "
			SELECT users_main.*, mocap_files_main.bvh_file  
			FROM users_main
			INNER JOIN mocap_files_main ON (users_main.id = mocap_files_main.user_id)
			ORDER BY users_main.firstname, users_main.lastname 
		";
		
		return $GLOBALS['DB']->fetch_multi($sql);
	}
	
	function insertUserReturnId($firstname, $lastname) {
		
		$sql = "
			SELECT id 
			FROM users_main 
			WHERE firstname = '$firstname' AND lastname = '$lastname'
		";

		$selected_id = $GLOBALS['DB']->fetch_single($sql);
		
		if ($selected_id) {
			
			return $selected_id['id'];
		} else {
			$sql = "
				INSERT INTO users_main (firstname, lastname) 
				VALUES ('$firstname', '$lastname')
			"; 
			
			$GLOBALS['DB']->query($sql);
			
			return $GLOBALS['DB']->id();
		}
	}
	
	function insertBvhFile($user_id, $bvh) {
		
		$sql = "
			SELECT id 
			FROM mocap_files_main 
			WHERE user_id = '$user_id'
		";

		$selected_id = $GLOBALS['DB']->fetch_single($sql);
		
		if (!$selected_id) {
			$sql = "
				INSERT INTO mocap_files_main (user_id, bvh_file) 
				VALUES ('$user_id', '$bvh')
			";
			
			$GLOBALS['DB']->query($sql);
		} else {
			$sql = "
				UPDATE mocap_files_main 
				SET bvh_file = '$bvh' 
				WHERE user_id = '$user_id'
			";
			
			$GLOBALS['DB']->query($sql);
		}
	}
	
	function getAllSignals() {
		$sql = "
			SELECT * 
			FROM signals_main
			GROUP BY signals_main.user_id
		";
		
		return $GLOBALS['DB']->fetch_multi($sql);
	}
	
	function addSignalsTest(&$usersId) {
		foreach ($usersId as $id => $key) {
			$userId = $usersId[$id]['id'];
			$sql = "
				SELECT signals_main.body_part, signals_main.signals
				FROM signals_main
				WHERE signals_main.user_id = $userId
				ORDER BY signals_main.body_part;
			";
			
			$usersId[$id]['signal'] = $GLOBALS['DB']->fetch_multi($sql);
		}		
	}
	
	function insertSignal($userId, $bodyPart, $signal) {
		$sql = "
			SELECT id 
			FROM signals_main 
			WHERE user_id = '$userId' AND body_part = '$bodyPart'
		";

		$selected_id = $GLOBALS['DB']->fetch_single($sql);
		
		if (!$selected_id) {
			$sql = "
				INSERT INTO signals_main (
					user_id, body_part, signals
				)
				VALUES (
					'$userId', '$bodyPart', '$signal'
				)
				"; 
				
			$GLOBALS['DB']->query($sql);
			
		} else {
			
			$sql = "
				UPDATE signals_main 
				SET signals = '$signal'
					
				WHERE user_id = '$userId' AND body_part = '$bodyPart'"; 
				
			$GLOBALS['DB']->query($sql);
		}
	}
	
	
	
	
	function insertFeatures($user_id, $height, $L_arm, $R_arm, $L_leg, $R_leg, $walk_speed, $L_stride, $R_stride, $step_width, $head_variance_x, $head_variance_y, $L_knee_variance_y, $R_knee_variance_y, $L_shoulder_variance_x, $R_shoulder_variance_x) {
		
		$sql = "
			SELECT id 
			FROM features_main 
			WHERE user_id = '$user_id'
		";

		$selected_id = $GLOBALS['DB']->fetch_single($sql);
		
		if (!$selected_id) {
			$sql = "
				INSERT INTO features_main (
					user_id,
					height, 
					L_arm, 
					R_arm, 
					L_leg, 
					R_leg, 
					walk_speed, 
					L_stride, 
					R_stride, 
					step_width, 
					head_variance_x,
					head_variance_y, 
					L_knee_variance_y,
					R_knee_variance_y, 
					L_shoulder_variance_x, 
					R_shoulder_variance_x
				) 
				VALUES (
					'$user_id',
					'$height', 
					'$L_arm', 
					'$R_arm', 
					'$L_leg', 
					'$R_leg',
					'$walk_speed', 
					'$L_stride', 
					'$R_stride', 
					'$step_width', 
					'$head_variance_x', 
					'$head_variance_y', 
					'$L_knee_variance_y',
					'$R_knee_variance_y', 
					'$L_shoulder_variance_x', 
					'$R_shoulder_variance_x'
				)
				"; 
				
			$GLOBALS['DB']->query($sql);
			
		} else {
			
			$sql = "
				UPDATE features_main 
				SET height 				  = '$height', 
					L_arm				  = '$L_arm', 
					R_arm			 	  = '$R_arm', 
					L_leg			 	  = '$L_leg', 
					R_leg			 	  = '$R_leg', 
					walk_speed 			  = '$walk_speed', 
					L_stride 		 	  = '$L_stride', 
					R_stride 		 	  = '$R_stride', 
					step_width		 	  = '$step_width',
					head_variance_x       = '$head_variance_x', 
					head_variance_y       = '$head_variance_y', 
					L_knee_variance_y 	  = '$L_knee_variance_y', 
					R_knee_variance_y 	  = '$R_knee_variance_y', 
					L_shoulder_variance_x = '$L_shoulder_variance_x', 
					R_shoulder_variance_x = '$R_shoulder_variance_x'
				WHERE user_id = '$user_id'"; 
				
			$GLOBALS['DB']->query($sql);
		}
	}
	
	function insertAcceleration($userId, $seqNumber, $bodyPart) {
		
		$sql = "
			SELECT id 
			FROM signals_main
			WHERE user_id = '$userId' AND sequence_number = '$seqNumber' AND body_part = '$bodyPart'
		";

		$selected_id = $GLOBALS['DB']->fetch_single($sql);
		
		if (!$selected_id) {
			
			$sql = "
				INSERT INTO signals_main (user_id, sequence_number, body_part) 
				VALUES ('$userId', '$seqNumber', '$bodyPart')
			";
			
			$GLOBALS['DB']->query($sql);	
			
			return $GLOBALS['DB']->id();
			
		} else {
			
			$sql = "
				UPDATE signals_main
				SET sequence_number = '$seqNumber',
					body_part		= '$bodyPart'
				WHERE user_id = '$userId' AND sequence_number = '$seqNumber' AND body_part = '$bodyPart'
			";
			
			$GLOBALS['DB']->query($sql);
			
			return $selected_id['id'];
		}
	}
	
	function insertAccelerationValues($acc_id, $values) {
		
		$sql  = "DELETE FROM acceleration_value WHERE acc_id = '$acc_id'";
		
		$GLOBALS['DB']->query($sql);

		for ($j = 0; $j < count($values); $j++) {
			$pom_value = $values[$j]; 
			$sql2 	   = "INSERT INTO acceleration_value (acc_id, value, order_val) VALUES('$acc_id', '$pom_value', '$j')"; 
			
			$GLOBALS['DB']->query($sql2);
		}
	}
	
	function loadFeatures($user_id) {
		$sql = "
			SELECT 
				height, 				 
				L_arm,		
				R_arm,
				L_leg,			 	 
				R_leg,				
				walk_speed,
				L_stride,
				R_stride,
				step_width,	 	     
				head_variance_x,      
				head_variance_y,      	 
				L_knee_variance_y, 	 
				R_knee_variance_y, 	  
				L_shoulder_variance_x,
				R_shoulder_variance_x
			FROM features_main
			WHERE user_id = '$user_id'
		";
		
		return $GLOBALS['DB']->fetch_single($sql);
	}
	
	function loadFeaturesOfAllUsers() {
		$sql = "
			SELECT 
				height,
				L_arm,
				R_arm,
				L_leg,
				R_leg,
				walk_speed,
				L_stride,
				R_stride,
				step_width,
				head_variance_x,
				head_variance_y,
				L_knee_variance_y,
				R_knee_variance_y,
				L_shoulder_variance_x,
				R_shoulder_variance_x
			FROM features_main
		";
		
		return $GLOBALS['DB']->fetch_multi($sql);
	}
	
	function loadSignals($user_id) {
		$sql = "
			SELECT *
			FROM signals_main
			WHERE user_id = $user_id
			ORDER BY body_part
		";
		
		return $GLOBALS['DB']->fetch_multi($sql);
	}

?>