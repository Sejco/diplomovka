\select@language {slovak}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}
\contentsline {chapter}{\numberline {2}Anal\IeC {\'y}za \IeC {\v l}udskej ch\IeC {\^o}dze}{3}
\contentsline {section}{\numberline {2.1}Sn\IeC {\'\i }manie pohybu}{3}
\contentsline {section}{\numberline {2.2}\IeC {\v S}trukt\IeC {\'u}ra MoCap d\IeC {\'a}t}{4}
\contentsline {section}{\numberline {2.3}Model kostry}{7}
\contentsline {section}{\numberline {2.4}Reprezent\IeC {\'a}cia ch\IeC {\^o}dze}{9}
\contentsline {section}{\numberline {2.5}Parametre ch\IeC {\^o}dze}{11}
\contentsline {subsection}{\numberline {2.5.1}Fyziologick\IeC {\'e} parametre}{11}
\contentsline {subsection}{\numberline {2.5.2}Pohybov\IeC {\'e} parametre}{13}
\contentsline {subsubsection}{R\IeC {\'y}chlos\IeC {\v t} ch\IeC {\^o}dze}{14}
\contentsline {subsubsection}{D\IeC {\'l}\IeC {\v z}ka kroku}{14}
\contentsline {subsubsection}{\IeC {\v S}\IeC {\'\i }rka krokov}{16}
\contentsline {subsubsection}{Variancia}{17}
\contentsline {subsection}{\numberline {2.5.3}\IeC {\v C}asovo z\IeC {\'a}visl\IeC {\'e} parametre}{19}
\contentsline {subsubsection}{Zr\IeC {\'y}chlenie}{20}
\contentsline {subsubsection}{Rot\IeC {\'a}cie ramien}{22}
\contentsline {subsubsection}{Vzdialenosti}{24}
\contentsline {subsubsection}{N\IeC {\'a}klon a ohyb}{25}
\contentsline {section}{\numberline {2.6}Porovn\IeC {\'a}vanie hodn\IeC {\^o}t}{26}
\contentsline {subsection}{\numberline {2.6.1}Porovn\IeC {\'a}vanie fyziologick\IeC {\'y}ch a pohybov\IeC {\'y}ch parametrov}{27}
\contentsline {subsection}{\numberline {2.6.2}Porovn\IeC {\'a}vanie \IeC {\v c}asovo z\IeC {\'a}visl\IeC {\'y}ch parametrov}{28}
\contentsline {section}{\numberline {2.7}Anal\IeC {\'y}za parametrov}{31}
\contentsline {subsection}{\numberline {2.7.1}Anal\IeC {\'y}za fyziologick\IeC {\'y}ch a pohybov\IeC {\'y}ch parametrov}{31}
\contentsline {subsection}{\numberline {2.7.2}Anal\IeC {\'y}za \IeC {\v c}asovo z\IeC {\'a}visl\IeC {\'y}ch parametrov}{36}
\contentsline {chapter}{\numberline {3}Implement\IeC {\'a}cia}{41}
\contentsline {section}{\numberline {3.1}N\IeC {\'a}vrh syst\IeC {\'e}mu}{41}
\contentsline {subsection}{\numberline {3.1.1}Syntaktick\IeC {\'y} analyz\IeC {\'a}tor}{42}
\contentsline {subsection}{\numberline {3.1.2}Filter ch\IeC {\^o}dze}{42}
\contentsline {subsection}{\numberline {3.1.3}Prehr\IeC {\'a}va\IeC {\v c}}{44}
\contentsline {subsection}{\numberline {3.1.4}Deskriptor}{45}
\contentsline {subsection}{\numberline {3.1.5}Analyz\IeC {\'a}tor ch\IeC {\^o}dze}{46}
\contentsline {subsection}{\numberline {3.1.6}Porovnanie}{47}
\contentsline {section}{\numberline {3.2}N\IeC {\'a}vrh datab\IeC {\'a}zy}{48}
\contentsline {chapter}{\numberline {4}Valid\IeC {\'a}cia}{50}
\contentsline {chapter}{\numberline {5}Z\IeC {\'a}ver}{53}
