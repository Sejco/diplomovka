CD obsahuje zdrojov� k�dy k aplik�ci�.


IN�TAL�CIA

In�tal�cie je rovnak� ako pre �tandarn� webov� aplik�ciu. S�bory sta�i nakop�rova� do www adres�ra.

Zlo�ka 'sql' obsahuje s�bor db.sql. Toto je datab�za, ktor� je potrebn� naimportova�.

V s�bore php\Db\Connection.php je potrebn� v 8 a 10 riadku nastavi� prihlasovacie �daje k datab�ze. Jeden riadok je pre localhost a druh� pre �ubo�n� in� adresu.


Ak by sa vykytli probl�my, be�iaca verzia je do�asne umiestnen� aj na adrese http://testpage.g6.cz/
Kv�li pr�snym obmedzeniam tohto servera je v�ak mo�n�, �e niektor� po�iadavky skon�ia na time out. Pr�padne ve�k� BVH s�bory nep�jdu uploadnu�.




NA��TANIE KOSTRY

Ka�d� typ BVh s�borov obsahuje kostru s rozdielnymi n�zvami k�bov. 

Syst�m nedok�e rozpozna� jednotliv� k�by na kostre. Je preto potrebn� ich definova�.
V s�bore js\bvh_parser.js sa na riadku 300 nach�dza funkcia, ktor� zjednocuje n�zvy k�bov.
V pr�pade, �e Vami na��tan� kostra bude obsahova� k�by, ktor� maj� in� n�zvy, ako uveden� v tejto funkcii, je potrebn�
bu� prida� nov� case alebo premenova� k�by priamo v BVH s�boroch.
K�by ktor�ch n�zov nebude mo�n� priradi� nebude syst�m bra� do �vahy.

S�bory ktor� s� obsiahnut� na tomto CD bud� fungova� bez probl�mov.





VYH�AD�VANIE

Zlo�ka mocap obsahuje BVH s�bory s ch�dzou. S�bory, ktor� maj� v n�zve "A" s� ulo�en� v prilo�enej datab�ze.
Pre vyh�ad�vanie v datab�ze na z�kalde ch�dze sta�� klikn�� v hornom menu na z�lo�ku "vyh�ad�vanie", vybra� niektor� 
z prilo�en�ch BVH s�borov (pr�padne in�ch), nastavi� v�hy jednotliv�ch parametrov a potvrdi�. 
